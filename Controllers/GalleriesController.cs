﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Authorization;

namespace ClinicWebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Galleries")]
    public class GalleriesController : Controller
    {
        private readonly ClinicDBContext _context;

        public GalleriesController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: api/Galleries
        //[HttpGet]
        //public IEnumerable<WebsiteGallery> GetWebsiteGallery()
        //{
        //    return _context.WebsiteGallery;
        //}

        // GET: api/Galleries/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetWebsiteGallery([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var websiteGallery = _context.WebsiteGallery.Where(m => m.WebsiteId == id).Where(m=>m.Show==true).ToList();

            if (websiteGallery == null)
            {
                return NotFound();
            }

            return Ok(websiteGallery);
        }

        // PUT: api/Galleries/5
        // [HttpPut("{id}")]
        // public async Task<IActionResult> PutWebsiteGallery([FromRoute] long id, [FromBody] WebsiteGallery websiteGallery)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     if (id != websiteGallery.Id)
        //     {
        //         return BadRequest();
        //     }

        //     _context.Entry(websiteGallery).State = EntityState.Modified;

        //     try
        //     {
        //         await _context.SaveChangesAsync();
        //     }
        //     catch (DbUpdateConcurrencyException)
        //     {
        //         if (!WebsiteGalleryExists(id))
        //         {
        //             return NotFound();
        //         }
        //         else
        //         {
        //             throw;
        //         }
        //     }

        //     return NoContent();
        // }

        // POST: api/Galleries
        // [HttpPost]
        // public async Task<IActionResult> PostWebsiteGallery([FromBody] WebsiteGallery websiteGallery)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     _context.WebsiteGallery.Add(websiteGallery);
        //     await _context.SaveChangesAsync();

        //     return CreatedAtAction("GetWebsiteGallery", new { id = websiteGallery.Id }, websiteGallery);
        // }

        // DELETE: api/Galleries/5
        // [HttpDelete("{id}")]
        // public async Task<IActionResult> DeleteWebsiteGallery([FromRoute] long id)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     var websiteGallery = await _context.WebsiteGallery.SingleOrDefaultAsync(m => m.Id == id);
        //     if (websiteGallery == null)
        //     {
        //         return NotFound();
        //     }

        //     _context.WebsiteGallery.Remove(websiteGallery);
        //     await _context.SaveChangesAsync();

        //     return Ok(websiteGallery);
        // }

        // private bool WebsiteGalleryExists(long id)
        // {
        //     return _context.WebsiteGallery.Any(e => e.Id == id);
        // }
    }
}