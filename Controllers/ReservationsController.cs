﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using System.Dynamic;
using System.Security.Claims;


namespace ClinicWebApp.Controllers
{
    public class ReservationsController : Controller
    {
        private readonly ClinicDBContext _context;
      

        public ReservationsController(ClinicDBContext context)
        {
            _context = context;
        }


      
        // GET: Reservations
         [Authorize]
        public async Task<IActionResult> Index(DateTime? sdate, DateTime? edate, string currentFilter,int? page)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }


             ViewData["cid"] = UserInfo.ClinicId;
            ViewData["uid"] = UserInfo.UserId;


            var reserves = from s in _context.Reservation.Where(m => m.ClinicId == UserInfo.ClinicId)
                           select s;

 
            if ( !String.IsNullOrEmpty(sdate.ToString()) || !String.IsNullOrEmpty(edate.ToString()))
            {
                ViewData["sdate"] = Convert.ToDateTime(sdate);
                ViewData["edate"] = Convert.ToDateTime(edate);
                reserves = reserves.Where(l => l.Date >= sdate).Where(l => l.Date <= edate);
            }
            int pageSize = 10;



            return View(await PaginatedList<Reservation>.CreateAsync(reserves.Where(a => a.ClinicId == UserInfo.ClinicId).Include(a=>a.Patient).OrderByDescending(a=>a.Date).ThenBy(a=>a.Time).AsNoTracking(), page ?? 1, pageSize));
           
        }


        public async Task<IActionResult> AddFinancialData(string pid){
            ViewData["extra"]=_context.Extra;
            ViewData["pid"]=pid;
            return View();
        }

        // GET: Reservations
         [Authorize]
        public async Task<IActionResult> IndexVisit(DateTime? sdate, DateTime? edate, string currentFilter, int? page)
        {

            if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic)
            .FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }



            ViewData["cid"] = UserInfo.ClinicId;
            ViewData["uid"] = UserInfo.UserId;



            /* var reserves = from s in _context
            .Reservation.Where(m => m.ClinicId == UserInfo.ClinicId)
            .Include(m=>m.PatientRecord)
            .Where(r=>r.Visited==true)
            .Where(r=>r.Date.Day==DateTime.Now.Day)
                           select s;*/



            


            var visitedContext = _context.Reservation
            .Where(r => r.Date.ToShortDateString() == DateTime.Now.ToShortDateString())
            .Where(r=>r.ClinicId== UserInfo.ClinicId).Include(a => a.Patient).OrderBy(a=>a.Time);
            

            //var visitedContextshow= visitedContext.Where(r => r.Visited == true);
            var visitedContextshow= visitedContext.Where(r => r.DoctorVisited == true);
            @ViewData["visited"] = visitedContextshow.Count();

            @ViewData["acceptance"] = visitedContext.Where(r => r.Acceptance == true).Count();

            @ViewData["total"]= visitedContext.ToList().Count();



            return View(visitedContext);

        }






        // GET: Reservations/Details/5

        public async Task<IActionResult> Details(long? id)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            if (id == null)
            {
                return NotFound();
            }

            var reservation = await _context.Reservation
                .Include(r => r.Clinic)
                .Include(r => r.Patient)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (reservation == null)
            {
                return NotFound();
            }

            return View(reservation);
        }

        // GET: Reservations/Create
         [Authorize]
        public IActionResult Create()
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "Address");
            ViewData["PatientId"] = new SelectList(_context.Patient, "Id", "FirstName");
            return View();
        }

        // POST: Reservations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
         [Authorize]
        public async Task<IActionResult> Create([Bind("Id,PatientId,ClinicId,Payment,PaymentType,Visited,Date,Time")] Reservation reservation)
        {
            if (ModelState.IsValid)
            {
                _context.Add(reservation);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "Address", reservation.ClinicId);
            ViewData["PatientId"] = new SelectList(_context.Patient, "Id", "FirstName", reservation.PatientId);
            return View(reservation);
        }




        [HttpPost]
        [ValidateAntiForgeryToken]
         [Authorize]
        public async Task<IActionResult> SelectPatient([Bind("Id,PatientId,ClinicId,Payment,PaymentType,Visited,Date,Time")] Reservation reservation,long? det)
        {
            if (ModelState.IsValid)
            {
                _context.Add(reservation);
                await _context.SaveChangesAsync();
                 return RedirectToAction("Details","ReservationFreeDates",new{Id=det});
            }
            return View(reservation);
        }

        //this methode is the same as previous methode but it convert jalali date from input

        [HttpPost]
        [ValidateAntiForgeryToken]
         [Authorize]
        public async Task<IActionResult> AddToReservationList([Bind("Id,PatientId,ClinicId,Payment,PaymentType,Visited,Date,Time")] Reservation reservation)
        {
            if (ModelState.IsValid)
            {
               
                _context.Add(reservation);
                await _context.SaveChangesAsync();
                // return RedirectToAction(nameof(Index));
                return RedirectToAction("Index","Patients");
            }
            // return View(reservation);
            return RedirectToAction("Index","Patients");
        }


        // GET: Reservations/Edit/5
         [Authorize]
        public async Task<IActionResult> Edit(long? id)
        {


            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            
            

            //freeDate
            var dateselectlist = _context.ReservationFreeDate.Where(a => a.Date >= DateTime.Now).Where(a => a.ClinicId == cid.ClinicId).OrderBy(a => a.Date).Select(s => new
            {
                date = s.Date,
                convertedDate = s.Date.ToString("yyyy-MM-dd")
        })
    .ToList();


           ViewData["rfd"]=new SelectList(dateselectlist, "date", "convertedDate",new{@class="eDate"});




            if (id == null)
            {
                return NotFound();
            }

            //var clinicprice= _context.Clinic.FirstOrDefault(m=>m.Id==UserInfo.ClinicId).VisitPrice;

            var reservation = await _context.Reservation.Include(m=>m.Patient).SingleOrDefaultAsync(m => m.Id == id);
            if (reservation == null)
            {
                return NotFound();
            }

            // if(reservation.Payment=="" || reservation.Payment==null){
            //     ViewData["clinicprice"]=clinicprice;
            // }
            // else{
            //     ViewData["clinicprice"]=reservation.Payment;
            // }
            ViewData["PatientId"] = reservation.Patient.FullName;


            return View(reservation);
        }

        // POST: Reservations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
         [Authorize]
        public async Task<IActionResult> Edit(long id, [Bind("Id,PatientId,ClinicId,Payment,PaymentType,Visited,Date,Time")] Reservation reservation)
        {
            if (id != reservation.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(reservation);
                    await _context.SaveChangesAsync();
                    // ViewData["status"]="1";
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ReservationExists(reservation.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                
                return RedirectToAction("Edit","Reservations",new{Id=id});
            }
            ViewData["PatientId"] = reservation.Patient.FullName; return View(reservation);
        }






         [HttpPost]
        public async Task<IActionResult> EditVisited(long id)
        {
             var reservation = _context.Reservation.FirstOrDefault(m => m.Id == id);
            if (reservation != null)
            {
                reservation.DoctorVisited = true;
            
            _context.Update(reservation);
            await _context.SaveChangesAsync();


            //save a patientrecord for ajax editing
            PatientRecord patientRecord=new PatientRecord();
            patientRecord.ReservationId=reservation.Id;
            patientRecord.Description="";
            patientRecord.Medication="";
            patientRecord.DescriptionText="";
            _context.Add(patientRecord);
            await _context.SaveChangesAsync();

            return Json(new { status = "success", message = reservation,pr=patientRecord });
            }
           return Json(new { status = "failed" });
        }





 [HttpPost]
        public async Task<IActionResult> EditAcceptance(long id)
        {
             var reservation = _context.Reservation.FirstOrDefault(m => m.Id == id);
            if (reservation != null)
            {
                reservation.Acceptance = true;
                reservation.AcceptanceTime =TimeSpan.Parse(DateTime.Now.ToString("hh:mm"));
            
            _context.Update(reservation);
            await _context.SaveChangesAsync();

            return Json(new { status = "success", message = reservation });
            }
           return Json(new { status = "failed" });
        }



        [HttpPost]
        public async Task<IActionResult> UpdateVisit(long id)
        {
            //Update visit
            var reservation = _context.Reservation.FirstOrDefault(m => m.Id == id);
            if (reservation != null)
            {
                reservation.Visited = true;
            
            _context.Update(reservation);
            await _context.SaveChangesAsync();

            return Json(new { status = "success", message = reservation });
            }
           return Json(new { status = "failed" });
   
        }


[HttpPost]
        public async Task<IActionResult> UpdatePayment([FromBody] Reservation r)
        {
            //Update visit
             var v = await _context.Reservation.Include(m=>m.Patient).SingleOrDefaultAsync(m => m.Id == r.Id);
            if (v != null)
            {
                v.Payment = r.Payment.Replace(",", "");

                v.PaymentType = r.PaymentType;
                 _context.Update(v);
            await _context.SaveChangesAsync();

            return new JsonResult(new { status = "success", message = v , sent=r });
            }
            return new JsonResult(new { status = "failed",message=r });
        }


        [HttpGet]
        public async Task<IActionResult> ExtraOnline([FromRoute] long id)
        {
         //update extra

             var res = _context.Reservation.FirstOrDefault(m => m.Id == id);
            if (res != null && res.Payment!=null)
            {
            int visit_id=_context.Extra.Where(r=>r.ClinicId==UserInfo.ClinicId).FirstOrDefault(r=>r.Title=="ویزیت").Id;
           
            var x=new ExtraPayment();
            x.ReservationId=id;
            x.Amount=res.Payment;
            x.ExtraId=visit_id;

            _context.Add(x);
            await _context.SaveChangesAsync ();
              return new JsonResult(new { extra=x,res=id });
            }
           
            return new JsonResult(new { msg="null",res=id });
        }






        [HttpPost]
        public async Task<IActionResult> UpdateExtra([FromBody] ExtraPayment x)
        {
            //Update visit
            var v = await _context.ExtraPayment.Include(m=>m.Extra).Include(m=>m.Reservation).SingleOrDefaultAsync(m => m.Id == x.Id);
            if (v != null)
            {
                v.Amount=x.Amount;
            _context.Update(v);
            await _context.SaveChangesAsync();

            return new JsonResult(new { status = "success", message = v , sent=x });
            }

            else{
                  _context.Add(x);
            await _context.SaveChangesAsync();
            return new JsonResult(new { status = "success" , sent=x });
            }
            return new JsonResult(new { status = "failed",message=v });
        }



  public JsonResult GetExtraPayment ([FromQuery] long rid) {
            var extra = _context.ExtraPayment
            .Where(m=>m.ReservationId==rid)
            .Include(m=>m.Extra)
            .ToList (); 
            if(extra!=null){
                 return Json (new{data=extra});
            }
           else{
                  return Json (new{data="nodata"});
           }
        }



        public JsonResult GetWebsiteInfo () {
            var clinic = _context.Clinic
            .FirstOrDefault(m=>m.Id==UserInfo.ClinicId);
            if(clinic!=null){
                 return Json (new{data=clinic});
            }
           else{
                  return Json (new{data="nodata"});
           }
        }



 [Authorize]
        public IActionResult QueueListViewComponent()
        {
            return ViewComponent("QueueList");
        }




        // GET: Reservations/Delete/5
         [Authorize]
        public async Task<IActionResult> Delete(long? id,long? resid)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            if (id == null)
            {
                return NotFound();
            }

            var reservation = await _context.Reservation
                .Include(r => r.Clinic)
                .Include(r => r.Patient)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (reservation == null)
            {
                return NotFound();
            }
            ViewData["resid"]=resid;
            return View(reservation);
        }

        // POST: Reservations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
         [Authorize]
        public async Task<IActionResult> DeleteConfirmed(long id,long resid)
        {
            var reservation = await _context.Reservation.SingleOrDefaultAsync(m => m.Id == id);
           _context.Reservation.Remove(reservation);
           await _context.SaveChangesAsync();
            // return RedirectToAction(nameof(Index));
             return RedirectToAction("Details","ReservationFreeDates",new{Id=resid});
        }

        private bool ReservationExists(long id)
        {
            return _context.Reservation.Any(e => e.Id == id);
        }



        // GET: Financial Check
         [Authorize]
        public async Task<IActionResult> FinancialCheck(DateTime? sdate, DateTime? edate)
        {
            ViewData["financialCheck"]=_context.Reservation
            .Include(r=>r.Patient)
            .Where(l => l.DoctorVisited == true)
            .Where(l => l.Payment == null || l.PaymentType == null)
            .OrderBy(p=>p.Date);

            ViewData["extra"]=_context.Extra;
           
            return View();
        }

        // GET: Reservations
         [Authorize]
        public async Task<IActionResult> Financial(DateTime? sdate, DateTime? edate)
        {

            if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

        
            //verified online
            ViewData["onlineFinancial"]=_context.OnlineReserve
            .Where(l => l.Date >= sdate)
            .Where(l => l.Date <= edate)
            .Where(l=>l.PaymentStatus==true)
            .Where(l=>l.verify==true)
            .Where(l=>l.ClinicId==UserInfo.ClinicId);




            var clinicDBContext = _context.Reservation.Include(l=>l.Patient)
            .Where(l=>l.Payment!=null)
            .Where(l=>l.ClinicId==UserInfo.ClinicId);
        
          

            var clinicdbbydate= clinicDBContext.Where(l => l.Date >= sdate).Where(l => l.Date <= edate);


            ViewData["extraFinancial"]=_context.ExtraPayment
            .Include(r=>r.Extra)
            .Include(r=>r.Reservation)
            .Where(l => l.Reservation.Date >= sdate)
            .Where(l => l.Reservation.Date <= edate)
            .OrderBy(p=>p.Reservation.Date).ThenBy(p => p.Reservation.PatientId);
           
            //گزارش تفصیلی
            ViewData["totalfinancial"]=clinicdbbydate.ToList().OrderBy(p => p.Date);
           
            var financialList =clinicdbbydate.GroupBy(l => l.Date)
                         .Select(lg =>
                               new
                               {
                                   date = lg.Key,
                                   count = lg.Count(),
                                   Total = lg.Sum(w => Convert.ToDouble(w.Payment))
                               }).OrderBy(p => p.date);
          


                ViewData["sdate"] = sdate;
                ViewData["edate"] = edate;
           

            dynamic total = new ExpandoObject();
            total.title = "گزارش مالی ";
            total.msg = "";

            if (financialList.Any())
            {
                var result = "";
                await financialList.ForEachAsync(i => result += "<tr><td class='eDate'>"+ i.date +" </td><td> "+"<span class='total'>"+ i.Total + "</span>  تومان" + "</td></tr>");
                total.dyn = result;


                var prices="";
                await financialList.ForEachAsync(i => prices +="<li>"+ i.Total+"</li>");
                ViewBag.chartp = prices;



                var dates = "";
                await financialList.ForEachAsync(i => dates += "<li class='eDate'>" + i.date.ToString("yyyy-MM-dd") + "</li>");
                ViewBag.chartd = dates;

               
            }
            else
            {
                var noresult = "";
                total.dyn = noresult;
                total.msg = "<h3 class='alert alert-warning'>در این بازه زمانی گزارش مالی ثبت نشده است</h3>";
            }



            var cinfo=_context.Clinic.FirstOrDefault(a=>a.Id==UserInfo.ClinicId);
            ViewData["clinicimg"]=cinfo.LogoFile;
             ViewData["clinicname"]=cinfo.FullName;

            return View(total);
        }

    }
}
