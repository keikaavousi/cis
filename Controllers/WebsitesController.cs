﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace ClinicWebApp.Controllers
{
    //[Route("api/[controller]")]
    public class WebsitesController : Controller
    {
        private readonly ClinicDBContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;
        public static string logo_img;
        public static string about_img;




        public WebsitesController(ClinicDBContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: Websites
         [Authorize(Roles="admin")]
        public async Task<IActionResult> Index()
        {
            var clinicDBContext = _context.Website.Include(w => w.Clinic);
            return View(await clinicDBContext.ToListAsync());
        }

        // GET: Websites/Details/5
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var website = await _context.Website
                .Include(w => w.Clinic)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (website == null)
            {
                return NotFound();
            }

            return View(website);
        }

        // GET: Websites/Create
        [Authorize]
        public IActionResult Create()
        {
           

                var user = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == user);
                var web = _context.Website.LastOrDefault(a => a.ClinicId == cid.ClinicId);


            
                
                if (web != null)

                {
                    return RedirectToAction("Edit", "Websites", new { Id = web.Id });
                }
                else
                {
                ViewData["cid"] = cid.ClinicId;
                ViewData["uid"] = user;
                return View();
                }

            }



        // POST: Websites/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.



        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Create([Bind("Id,ClinicId,LogoFileUrl,Title,SubTitle,AboutUs,AboutUsPicture,Address,Tel1,Tel2,Tel3,Tel4,Tel5,Theme,ColorTheme,VisitTime,Map")] Website website, IFormFile LogoFileUrl, IFormFile AboutUsPicture)
        {

            string webRootPath = _hostingEnvironment.WebRootPath;
            string abspath = "\\images\\website_content\\" + website.ClinicId;
            System.IO.Directory.CreateDirectory(webRootPath + abspath);


            string contentpath = "images/website_content/" + website.ClinicId;


            if (ModelState.IsValid)
            {

                if (LogoFileUrl == null || LogoFileUrl.Length == 0)
                {
                    website.LogoFileUrl = null;
                }
                else
                {
                    var fileName = ContentDispositionHeaderValue.Parse(LogoFileUrl.ContentDisposition).FileName.Trim('"');
                    var FileExtension = Path.GetExtension(fileName);


                    var logopath = contentpath + "/" + Path.GetRandomFileName() + FileExtension;

                    using (var stream = new FileStream("wwwroot/" + logopath, FileMode.Create))
                    {
                        await LogoFileUrl.CopyToAsync(stream);
                        website.LogoFileUrl = logopath.ToString();
                    }
                }



                if (AboutUsPicture == null || AboutUsPicture.Length == 0)
                {
                    website.AboutUsPicture = null;
                }
                else
                {
                    var fileName = ContentDispositionHeaderValue.Parse(AboutUsPicture.ContentDisposition).FileName.Trim('"');
                    var FileExtension = Path.GetExtension(fileName);

                    var imgpath = contentpath + "/" + Path.GetRandomFileName() + FileExtension;

                    using (var stream = new FileStream("wwwroot/" + imgpath, FileMode.Create))
                    {
                        await AboutUsPicture.CopyToAsync(stream);
                        website.AboutUsPicture = imgpath.ToString();
                    }
                }



                _context.Add(website);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "Address", website.ClinicId);
            return View(website);
        }

        // GET: Websites/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {


            var user = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == user);
                var web = _context.Website.LastOrDefault(a => a.ClinicId == cid.ClinicId);
                
                

            if (id == null || id!=web.Id)
            {
                return NotFound();
            }

            var website = await _context.Website.SingleOrDefaultAsync(m => m.Id == id);
            if (website == null)
            {
                return NotFound();
            }
            logo_img = website.LogoFileUrl;
            about_img = website.AboutUsPicture;
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "Address", website.ClinicId);
            return View(website);
        }

        // POST: Websites/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ClinicId,LogoFileUrl,Title,SubTitle,AboutUs,AboutUsPicture,Address,Tel1,Tel2,Tel3,Tel4,Tel5,Theme,ColorTheme,VisitTime,Map")] Website website, IFormFile LogoFileUrl, IFormFile AboutUsPicture)
        {
            if (id != website.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    string contentpath = "images/website_content/" + website.ClinicId;


                    if (ModelState.IsValid)
                    {

                        if (LogoFileUrl == null || LogoFileUrl.Length == 0)
                        {

                            website.LogoFileUrl = logo_img;

                        }
                        else if (LogoFileUrl.FileName != null && LogoFileUrl.Length > 0)
                        {
                            var fileName = ContentDispositionHeaderValue.Parse(LogoFileUrl.ContentDisposition).FileName.Trim('"');
                            var FileExtension = Path.GetExtension(fileName);


                            var logopath = contentpath + "/" + Path.GetRandomFileName() + FileExtension;

                            using (var stream = new FileStream("wwwroot/" + logopath, FileMode.Create))
                            {
                                await LogoFileUrl.CopyToAsync(stream);
                                website.LogoFileUrl = logopath.ToString();
                            }
                        }



                        if (AboutUsPicture == null || AboutUsPicture.Length == 0)
                        {

                            website.AboutUsPicture = about_img;

                        }
                        else if (AboutUsPicture.FileName != null && AboutUsPicture.Length > 0)
                        {
                            var fileName = ContentDispositionHeaderValue.Parse(AboutUsPicture.ContentDisposition).FileName.Trim('"');
                            var FileExtension = Path.GetExtension(fileName);

                            var imgpath = contentpath + "/" + Path.GetRandomFileName() + FileExtension;

                            using (var stream = new FileStream("wwwroot/" + imgpath, FileMode.Create))
                            {
                                await AboutUsPicture.CopyToAsync(stream);
                                website.AboutUsPicture = imgpath.ToString();
                            }
                        }




                        _context.Update(website);
                        await _context.SaveChangesAsync();
                        TempData["msg"] = "<span class='alert alert-success page-notif'>ویرایش با موفقیت انجام شد</span>";

                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WebsiteExists(website.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Edit", "Websites", new { Id=id });
            }
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "Address", website.ClinicId);
            return RedirectToAction("Edit", "Websites", new { Id=id });
        }

        // GET: Websites/Delete/5
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var website = await _context.Website
                .Include(w => w.Clinic)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (website == null)
            {
                return NotFound();
            }

            return View(website);
        }

        // POST: Websites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var website = await _context.Website.SingleOrDefaultAsync(m => m.Id == id);
            _context.Website.Remove(website);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WebsiteExists(int id)
        {
            return _context.Website.Any(e => e.Id == id);
        }



      

        

    }

}
