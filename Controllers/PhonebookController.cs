﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using System.Security.Claims;

namespace ClinicWebApp.Controllers
{
    public class PhonebookController : Controller
    {
        private readonly ClinicDBContext _context;

        public PhonebookController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: Messages
        [Authorize]
        public async Task<IActionResult> Index()
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            var clinicDBContext = _context.Phonebook.Where(r=>r.ClinicId==UserInfo.ClinicId);
            return View(await clinicDBContext.ToListAsync());
        }

       
       

        // GET: Messages/Create
        [Authorize]
        public IActionResult Create()
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            ViewData["cid"] = UserInfo.ClinicId;
            return View();
        }

        // POST: Messages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("Id,FName,LName,Tel,Description,ClinicId")] Phonebook phonebook)
        {
            if (ModelState.IsValid)
            {
                _context.Add(phonebook);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["cid"] = UserInfo.ClinicId;
            return View(phonebook);
        }

        // GET: Messages/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            
            if (id == null)
            {
                return NotFound();
            }

            var phonebook = await _context.Phonebook.SingleOrDefaultAsync(m => m.Id == id);

            if (phonebook == null)
            {
                return NotFound();
            }
            ViewData["uid"] = UserInfo.UserId;
            return View(phonebook);
        }

        // POST: Messages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FName,LName,Tel,Description,ClinicId")] Phonebook phonebook)
        {
            if (id != phonebook.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(phonebook);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PhonebookExists(phonebook.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["cid"] = UserInfo.ClinicId;
            return View(phonebook);
        }

        // GET: Messages/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }
            
            if (id == null)
            {
                return NotFound();
            }

            var phonebook = await _context.Phonebook
                .SingleOrDefaultAsync(m => m.Id == id);
            if (phonebook == null)
            {
                return NotFound();
            }

            return View(phonebook);
        }

        // POST: Messages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var phonebook = await _context.Phonebook.SingleOrDefaultAsync(m => m.Id == id);
            _context.Phonebook.Remove(phonebook);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PhonebookExists(int id)
        {
            return _context.Phonebook.Any(e => e.Id == id);
        }

        }
    }

