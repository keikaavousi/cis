﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Authorization;


namespace ClinicWebApp.Controllers
{
    public class WebsitePostsController : Controller
    {
        private readonly ClinicDBContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;
        public static string main_img;



        public WebsitePostsController(ClinicDBContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: WebsitePosts
         [Authorize]
        public async Task<IActionResult> Index()
        {  
            ViewData["cid"] = UserInfo.ClinicId;
            ViewData["uid"] = UserInfo.UserId;
            var wid= _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;
            ViewData["wid"]=wid;
            /**/

            var clinicDBContext = _context.WebsitePost.Include(w => w.Category).Where(c => c.WebsiteId == wid);
           
           
            return View(await clinicDBContext.ToListAsync());
        }

        // GET: WebsitePosts/Details/5
         [Authorize]
        public async Task<IActionResult> Details(long? id)
        {
                        var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            if (id == null)
            {
                return NotFound();
            }

            var websitePost = await _context.WebsitePost
                .Include(w => w.Category)
                .Include(w => w.Website)
                 .Where(w => w.Website.Id == wid)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (websitePost == null)
            {
                return NotFound();
            }

            return View(websitePost);
        }

        // GET: WebsitePosts/Create
         [Authorize]
        public IActionResult Create()
        {
            
            ViewData["CategoryId"] = new SelectList(_context.Category, "Id", "Title");

            ViewData["WebsiteId"]= _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;
             var user = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == user);

            ViewData["websiteUrl"] = cid.Clinic.Url;

            return View();
        }

        // POST: WebsitePosts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
         [Authorize]
        public async Task<IActionResult> Create([Bind("Id,WebsiteId,Title,Slug,Date,PostContent,MainPicture,CategoryId,Tag,Comment,Keywords,Description,Visible")] WebsitePost websitePost,IFormFile MainPicture)
        {
            string webRootPath = _hostingEnvironment.WebRootPath;
            string abspath = "\\images\\website_content\\" + HttpContext.Session.GetString("cid");
            System.IO.Directory.CreateDirectory(webRootPath + abspath);


            string contentpath = "images/website_content/" + HttpContext.Session.GetString("cid");

            if (ModelState.IsValid)
            {
                if (MainPicture == null || MainPicture.Length == 0)
                {
                    websitePost.MainPicture = null;
                }
                else
                {
                    var fileName = ContentDispositionHeaderValue.Parse(MainPicture.ContentDisposition).FileName.Trim('"');
                    var FileExtension = Path.GetExtension(fileName);


                    var imgpath = contentpath + "/" + Path.GetRandomFileName() + FileExtension;

                    using (var stream = new FileStream("wwwroot/" + imgpath, FileMode.Create))
                    {
                        await MainPicture.CopyToAsync(stream);
                        websitePost.MainPicture = imgpath.ToString();
                    }
                }


               //websitePost.Date= Utility.ToMiladi(Convert.ToDateTime(websitePost.Date));
                _context.Add(websitePost);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewData["CategoryId"] = new SelectList(_context.Category, "Id", "Title", websitePost.CategoryId);
            ViewData["WebsiteId"]= _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            return View(websitePost);
        }

        // GET: WebsitePosts/Edit/5
         [Authorize]
        public async Task<IActionResult> Edit(long? id)
        {
                        var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            if (id == null)
            {
                return NotFound();
            }

            var websitePost = await _context.WebsitePost
             .Where(w => w.Website.Id == wid)
             .SingleOrDefaultAsync(m => m.Id == id);
            if (websitePost == null)
            {
                return NotFound();
            }
            
            main_img = websitePost.MainPicture;
            ViewData["CategoryId"] = new SelectList(_context.Category, "Id", "Title", websitePost.CategoryId);
             var user = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == user);
            ViewData["WebsiteId"]= _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            ViewData["websiteUrl"] = cid.Clinic.Url;

            return View(websitePost);
        }

        // POST: WebsitePosts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
         [Authorize]
        public async Task<IActionResult> Edit(long id, [Bind("Id,WebsiteId,Title,Slug,Date,PostContent,MainPicture,CategoryId,Tag,Comment,Keywords,Description,Visible")] WebsitePost websitePost,IFormFile MainPicture)
        {
            if (id != websitePost.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                string contentpath = "images/website_content/" + HttpContext.Session.GetString("cid");

                try
                {
                    if (MainPicture == null || MainPicture.Length == 0)
                    {

                        websitePost.MainPicture = main_img;

                    }
                    else if (MainPicture.FileName != null && MainPicture.Length > 0)
                    {
                        var fileName = ContentDispositionHeaderValue.Parse(MainPicture.ContentDisposition).FileName.Trim('"');
                        var FileExtension = Path.GetExtension(fileName);


                        var logopath = contentpath + "/" + Path.GetRandomFileName() + FileExtension;

                        using (var stream = new FileStream("wwwroot/" + logopath, FileMode.Create))
                        {
                            await MainPicture.CopyToAsync(stream);
                            websitePost.MainPicture = logopath.ToString();
                        }
                    }


                    //websitePost.Date = Utility.ToMiladi(Convert.ToDateTime(websitePost.Date));

                    _context.Update(websitePost);
                    await _context.SaveChangesAsync();
                    TempData["msg"] = "<span class='alert alert-success page-notif'>ویرایش با موفقیت انجام شد</span>";

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WebsitePostExists(websitePost.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(_context.Category, "Id", "Title", websitePost.CategoryId);
            ViewData["WebsiteId"] = new SelectList(_context.Website, "Id", "Id", websitePost.WebsiteId);
            return View(websitePost);
        }

        // GET: WebsitePosts/Delete/5
         [Authorize]
        public async Task<IActionResult> Delete(long? id)
        {
                        var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            if (id == null)
            {
                return NotFound();
            }

            var websitePost = await _context.WebsitePost
                .Include(w => w.Category)
                .Include(w => w.Website)
                 .Where(w => w.Website.Id == wid)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (websitePost == null)
            {
                return NotFound();
            }

            return View(websitePost);
        }

        // POST: WebsitePosts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
         [Authorize]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
                        var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            var websitePost = await _context.WebsitePost
             .Where(w => w.Website.Id == wid)
             .SingleOrDefaultAsync(m => m.Id == id);
            _context.WebsitePost.Remove(websitePost);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WebsitePostExists(long id)
        {
            return _context.WebsitePost.Any(e => e.Id == id);
        }


 [Authorize]
        public IActionResult CheckExistingSlug(string Slug, int Id, int WebsiteId)
        {
            var ed = _context.WebsitePost.Where(a => a.Slug == Slug).Where(a => a.WebsiteId == WebsiteId).Where(a => a.Id != Id);

            if (ed.Any())
            {
                return Json(data: "این عنوان برای نامک تکراری است");
            }
            else
            {
                return Json(data: true);
            }


        }


     
        }
}
