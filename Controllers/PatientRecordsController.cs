﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using System.Security.Claims;
namespace ClinicWebApp.Controllers
{
    public class PatientRecordsController : Controller
    {
        private readonly ClinicDBContext _context;


        public PatientRecordsController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: PatientRecords
        [Authorize(Roles="doctor,admin,secretary")]
        public async Task<IActionResult> Index(long? patientId,int? d)
        {
            
            if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

                @ViewData["flag"]=d;

                var patient = _context.Patient.FirstOrDefault(r => r.Id == patientId);
                
                ViewData["patientName"] = patient.FullName;

                var clinicDBContext = _context.Reservation.Include(p => p.PatientRecord)
                .Where(p => p.ClinicId == UserInfo.ClinicId)
                .Where(p=>p.PatientId==patientId)
                //.Where(p=>p.Visited==true)
                .OrderByDescending(r=>r.Id);
                
                
               
                
                return View(await clinicDBContext.ToListAsync());

           /*  if (patientId != null)
            {
                var patient = _context.Patient.FirstOrDefault(r => r.Id == patientId);
                
                ViewData["patientName"] = patient.FullName;

                var clinicDBContext = _context.PatientRecord.Include(p => p.Reservation)
                .Where(p => p.Reservation.ClinicId == UserInfo.ClinicId)
                .Where(p=>p.Reservation.PatientId==patientId)
                .OrderByDescending(r=>r.Id);
                
                
                
                return View(await clinicDBContext.ToListAsync());
            }
            else
            {
                var clinicDBContext = _context.PatientRecord.Include(p => p.Reservation);
                return View(await clinicDBContext.ToListAsync());
            }*/
        }



      

        // GET: PatientRecords/Details/5
        [Authorize(Roles="doctor,admin,secretary")]
        public async Task<IActionResult> Details(long? id,int? d)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            @ViewData["flag"]=d;
            if (id == null)
            {
                ViewData["notfound"]="true";
            }

//it changed due to error for singleordefault,because it added empty row
            var patientRecord=await _context.PatientRecord
                .Include(p=>p.Reservation.Patient)
                .Where(m => m.DescriptionText != "")
                .Where(m=> m.DescriptionText != null)
                .LastOrDefaultAsync(m => m.ReservationId == id);

                

                 ViewData["extrapayment"]= await _context.ExtraPayment
                 .Include(m=>m.Extra)
                .Where(m => m.ReservationId == id).ToListAsync();


               


            if (patientRecord == null)
            {
                ViewData["notfound"]="true";
            }
            

            return View(patientRecord);
        }


      
        // GET: PatientRecords/Create
        [Authorize(Roles="doctor,admin,secretary")]
        public IActionResult Create(long? rnum)
        {
            if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }



            var clinicDBContext = _context.Reservation.Include(p=>p.Patient).Where(p => p.ClinicId == UserInfo.ClinicId).Where(p => p.Id == rnum).FirstOrDefault();
            ViewData["patientName"] = clinicDBContext.Patient.FullName;
            ViewData["Date"] =clinicDBContext.Date;
            ViewData["DocumentId"] = clinicDBContext.Patient.DocNumber;
            ViewData["ReservationId"] = rnum;

            long pid=clinicDBContext.Patient.Id;


            ViewData["pid"]=pid;

            var attachment=_context.RecordAttachment
                .Where(c=>c.PatientId==pid).OrderByDescending(c=>c.Id).ToList();

                if(attachment.Any()){
                    ViewData["Attachment"]=attachment;
                }
                else{
                    ViewData["Attachment"]=null;
                }
           

            //var records=_context.PatientRecord.Include(p=>p.Reservation).Where(p => p.Reservation.ClinicId == UserInfo.ClinicId).Where(p=>p.Reservation.PatientId==pid).ToList();
            var records=_context.Reservation.Where(p => p.ClinicId == UserInfo.ClinicId)
            .Where(p=>p.PatientId==pid).ToList();

            if(records.Any()){
                ViewData["PatientRecord"]=records;
            }
            else{
               ViewData["PatientRecord"]=null; 
            }
            
           
           var patientDoc=_context.Patient.Where(p => p.ClinicId == UserInfo.ClinicId)
           .Where(p=>p.Id==pid)
           .FirstOrDefault();

            ViewData["Job"]=patientDoc.Job;
            ViewData["Reagent"]=patientDoc.Reagent;
            ViewData["MainDisease"]=patientDoc.MainDisease;
            ViewData["UsedMedication"]=patientDoc.UsedMedication;
            

            //var pid = clinicDBContext.Patient.Id;
            //ViewData["Records"] = _context.PatientRecord.Where(p => p.Reservation.PatientId == pid).ToList();
            return View();
        }

        // POST: PatientRecords/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="doctor,admin,secretary")]
        public async Task<IActionResult> Create([Bind("Id,ReservationId,Description,Medication,DescriptionText")] PatientRecord patientRecord)
        {


            if (ModelState.IsValid)
            {
                _context.Add(patientRecord);
                await _context.SaveChangesAsync();
                return RedirectToAction("IndexVisit","Reservations");
            }
            //ViewData["ReservationId"] = new SelectList(_context.Reservation, "Id", "Id", patientRecord.ReservationId);
            return View(patientRecord);
        }




        [HttpPost]
        public async Task<IActionResult> saveRecord ([FromBody] PatientRecord patientRecord) {

            var status = false;
           
            if (!ModelState.IsValid) 
            {
                return Json (status);
            } 
            else {
                _context.Update(patientRecord);
                await _context.SaveChangesAsync ();            
            status = true;
            return Json(new{st=status,pr=patientRecord.Id});
            }
        }


        // GET: PatientRecords/Edit/5
        [Authorize(Roles="doctor,admin,secretary")]
        public async Task<IActionResult> Edit(long? id,long? resid)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

 var patientRecord=new PatientRecord();

if(resid!=null){
      patientRecord = await _context.PatientRecord.Include(p=>p.Reservation)
            .Where(p => p.Reservation.ClinicId == UserInfo.ClinicId)
            .Include(p=>p.Reservation.Patient)
            .FirstOrDefaultAsync(m => m.Reservation.Id == resid);
}

else if(id!=null){


            patientRecord = await _context.PatientRecord.Include(p=>p.Reservation)
            .Where(p => p.Reservation.ClinicId == UserInfo.ClinicId)
            .Include(p=>p.Reservation.Patient)
            .FirstOrDefaultAsync(m => m.Id == id);
 }

            /* if (patientRecord == null)
            {
                return RedirectToAction("Create","PatientRecords",new{rnum=resid});
            }
            else{
                */

            ViewData["patientName"] = patientRecord.Reservation.Patient.FullName;
            ViewData["Date"] = patientRecord.Reservation.Date;
            ViewData["DocumentId"] = patientRecord.Reservation.Patient.DocNumber;
            long pid=patientRecord.Reservation.Patient.Id;
            ViewData["pid"]=pid;

                 var attachment=_context.RecordAttachment
                .Where(c=>c.PatientId==pid).ToList();

                if(attachment.Any()){
                    ViewData["Attachment"]=attachment;
                }
                else{
                    ViewData["Attachment"]=null;
                }
           

            //var records=_context.PatientRecord.Include(p=>p.Reservation).Where(p => p.Reservation.ClinicId == UserInfo.ClinicId).Where(p=>p.Reservation.PatientId==pid).ToList();
            var records=_context.Reservation.Where(p => p.ClinicId == UserInfo.ClinicId)
            .Where(p=>p.PatientId==pid).ToList();

            if(records.Any()){
                ViewData["PatientRecord"]=records;
            }
            else{
               ViewData["PatientRecord"]=null; 
            }
            
           
           var patientDoc=_context.Patient.Where(p => p.ClinicId == UserInfo.ClinicId)
           .Where(p=>p.Id==pid)
           .FirstOrDefault();

            ViewData["Job"]=patientDoc.Job;
            ViewData["Reagent"]=patientDoc.Reagent;
            ViewData["MainDisease"]=patientDoc.MainDisease;
            ViewData["UsedMedication"]=patientDoc.UsedMedication;


    

            


             ViewData["id"]=patientRecord.Id;

            return View(patientRecord);
        }

        // POST: PatientRecords/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="doctor,admin,secretary")]
        public async Task<IActionResult> Edit(long id, [Bind("Id,ReservationId,Description,DescriptionText,Medication")] PatientRecord patientRecord)
        {
           

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(patientRecord);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PatientRecordExists(patientRecord.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

           /* ViewData["patientName"] = patientRecord.Reservation.Patient.FullName;
            ViewData["Date"] = patientRecord.Reservation.Date;
            ViewData["DocumentId"] = patientRecord.Reservation.Patient.DocNumber;*/

                return RedirectToAction("IndexVisit","Reservations");
            }
           return View(patientRecord);
        }

        // GET: PatientRecords/Delete/5
        [Authorize(Roles="doctor,admin")]
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patientRecord = await _context.PatientRecord
                .Include(p => p.Reservation)
                .Where(p => p.Reservation.ClinicId == UserInfo.ClinicId)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (patientRecord == null)
            {
                return NotFound();
            }

            return View(patientRecord);
        }

        // POST: PatientRecords/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="doctor,admin")]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var patientRecord = await _context.PatientRecord.SingleOrDefaultAsync(m => m.Id == id);
            _context.PatientRecord.Remove(patientRecord);
            await _context.SaveChangesAsync();
            
            return RedirectToAction(nameof(Index));
        }

        private bool PatientRecordExists(long id)
        {
            return _context.PatientRecord.Any(e => e.Id == id);
        }
    }
}
