﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using System.Globalization;

namespace ClinicWebApp.Controllers
{
    public class PatientsController : Controller
    {
        private readonly ClinicDBContext _context;


        public PatientsController(ClinicDBContext context)
        {
            _context = context;
        }


        [Authorize]
        public IActionResult Recordfile(string id)
        {
            var file = Path.Combine(Directory.GetCurrentDirectory(),
                                    "Records", "records_images", id);

            return PhysicalFile(file, "image/jpeg");
        }

        [Authorize]
        public IActionResult QueueIndex(DateTime? dt)
        {
            if (dt != null)
            {
                ViewData["date"] = dt;
            }
            else
            {
                ViewData["date"] = DateTime.Now;
            }
            return ViewComponent("QueueList",dt);
        }
            // GET: Patients
            [Authorize]
            public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter,int? page,DateTime? dt)
        {
            if(UserInfo.ClinicId==0){
            var uiid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var ciid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uiid);
            UserInfo.UserId = uiid;
            UserInfo.ClinicId = ciid.ClinicId;
            }


            if (dt != null)
            {
                    ViewData["date"]=dt;
            }
            else
            {
                ViewData["date"] = DateTime.Now;
            }


            ViewData["CurrentSort"] = sortOrder;

            
            ViewData["idSortParm"] = String.IsNullOrEmpty(sortOrder) ? "id" : "";
            ViewData["NameSortParm"] = sortOrder == "name_desc" ? "name_desc" : "";

            ViewData["CurrentFilter"] = searchString;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var patients = from s in _context.Patient.Where(a => a.ClinicId == UserInfo.ClinicId)
                           select s;

       

            if (!String.IsNullOrEmpty(searchString))
            {
                patients = patients.Where(s => s.LastName.Contains(searchString)
                                       || s.FirstName.Contains(searchString)
                                       || s.DocNumber.Equals(searchString)
                                       );
            }


            switch (sortOrder)
            {
                case "name_desc":
                    patients = patients.OrderByDescending(s => s.FirstName);
                    break;
                case "id":
                    patients = patients.OrderByDescending(s => s.DocNumber);
                    break;
                default:
                    patients = patients.OrderBy(s => s.DocNumber);
                    break;
            }

              
            ViewData["cid"] = UserInfo.ClinicId;
            ViewData["uid"] = UserInfo.UserId;

            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a=>a.UserId== UserInfo.UserId);
            ViewData["VisitPrice"]=cid.Clinic.VisitPrice;

            // var clinicDBContext = _context.Patient.Where(a=>a.ClinicId==cid.ClinicId).OrderByDescending(a=>a.Id);
            // return View(await clinicDBContext.AsNoTracking().ToListAsync());

            int pageSize = 10;
            return View(await PaginatedList<Patient>.CreateAsync(patients.Where(a => a.ClinicId == cid.ClinicId).AsNoTracking(), page ?? 1, pageSize));

            //return patients;
        }





        // GET: Patients/Details/5
        [Authorize]
        public async Task<IActionResult> Details(long? id)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }


            if (id == null)
            {
                return NotFound();
            }

            var patient = await _context.Patient
                .Include(p => p.Clinic)
                .Where(p => p.ClinicId == UserInfo.ClinicId)
                .SingleOrDefaultAsync(m => m.Id == id);

                /* ViewData["attachment"]=_context.RecordAttachment
                .Where(c=>c.PatientId==id)
                .OrderByDescending(c=>c.Id);*/

            if (patient == null)
            {
                return NotFound();
            }

            return View(patient);
        }

        // GET: Patients/Create
        [Authorize]
        public IActionResult Create()
        {
             ViewData["cid"] = UserInfo.ClinicId;
            ViewData["uid"] = UserInfo.UserId;


            /*var dn = _context.Patient.LastOrDefault(a=>a.ClinicId==UserInfo.ClinicId);
            long docnumber=0;
            if (dn != null)
            {
               docnumber= long.Parse(dn.DocNumber);
                docnumber += 1;
            }
            else
            {
                docnumber = 1;
            }
            ViewData["docnumber"]=docnumber;*/
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "FullName");
            return View();
        }


        //Check Existing DocNumber
        // [HttpPost]
[Authorize]
        public IActionResult CheckExistingDocNumber(string DocNumber,int Id,int ClinicId)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var ciid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = ciid.ClinicId;
            }

            int cid = Convert.ToInt32(ViewData["cid"]);
            var ed = _context.Patient.Include(a=>a.DocNumber).Where(a => a.DocNumber == DocNumber).Where(a=>a.ClinicId==ClinicId);
            //ViewData["result"] = ed;
            if (Id == 0)
            {
                if (ed.Any())
                {
                    return Json(data: "این شماره پرونده قبلا ثبت شده است");
                }
                else
                {
                    return Json(data: true);
                }
            }
            else
            {
                return Json(data: true);
            }
               
        }

        //private bool IsDocNumber(string documentNo)
        //{
        //    if (_context.Patient.Include(a => a.ClinicId == (Convert.ToInt32(ViewData["cid"]))).FirstOrDefault(a => a.DocNumber == documentNo) == null)
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        return true;
        //    }
        //}


       
        // POST: Patients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("Id,DocNumber,FirstName,LastName,Tel,Mobile,Email,Address,MainDisease,FatherName,ClinicId,UsedMedication,Reagent,NationalId,Job,NoCharge,Insurance,Supplementary")] Patient patient)
        {
            if (ModelState.IsValid)
            {
                _context.Add(patient);
                await _context.SaveChangesAsync();
            return RedirectToAction("Details","Patients", new{Id=patient.Id});
            }
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "Address", patient.ClinicId);
            return View(patient);
        }




        /*******************create new doc for non exisiting patients*******************/




        // GET: Patients/Create
        [Authorize]
        public IActionResult CreateNew(long? id)
        {
             ViewData["cid"] = UserInfo.ClinicId;
            ViewData["uid"] = UserInfo.UserId;
            
            var dn = _context.Patient.LastOrDefault(a => a.ClinicId == UserInfo.ClinicId);
            long docnumber = 0;
            if (dn != null)
            {
                docnumber = long.Parse(dn.DocNumber);
                docnumber += 1;
            }
            else
            {
                docnumber = 1;
            }


            var patient = _context.OnlineReserve.SingleOrDefault(a => a.Id == id);
            ViewData["fname"] = patient.FirstName;
            ViewData["lname"] = patient.LastName;
            ViewData["mobile"] = patient.Mobile;
            ViewData["email"] = patient.Email;
            ViewData["date"] = patient.Date;
            ViewData["time"] = patient.Time;


            ViewData["docnumber"] = docnumber;
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "FullName");
            return View();
        }


[Authorize]
        public async Task<IActionResult> CreateNewp(string date,string time, [Bind("Id,DocNumber,FirstName,LastName,Tel,Mobile,Email,Address,MainDisease,FatherName,ClinicId,UsedMedication,Reagent,NationalId,Job,NoCharge,Insurance,Supplementary")] Patient patient,Reservation res)
        {
            if (ModelState.IsValid)
            {


                res.Date =Convert.ToDateTime(date);
                res.Time = TimeSpan.Parse(time);
                res.Patient = patient;

                _context.Add(patient);
                _context.Add(res);
                await _context.SaveChangesAsync();


                return RedirectToAction("Index","OnlineReserves");
            }
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "Address", patient.ClinicId);
            return View();
        }

        // GET: Patients/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(long? id)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            if (id == null)
            {
                return NotFound();
            }

            var patient = await _context.Patient.Where(p => p.ClinicId == UserInfo.ClinicId).SingleOrDefaultAsync(m => m.Id == id);
            if (patient == null)
            {
                return NotFound();
            }
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "Address", patient.ClinicId);
            return View(patient);
        }

        // POST: Patients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(long id, [Bind("Id,DocNumber,FirstName,LastName,Tel,Mobile,Email,Address,MainDisease,FatherName,ClinicId,UsedMedication,Reagent,NationalId,Job,NoCharge,Insurance,Supplementary")] Patient patient)
        {
            if (id != patient.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(patient);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PatientExists(patient.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details","Patients",new{Id=patient.Id});
            }
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "Address", patient.ClinicId);
            return View(patient);
        }

        // GET: Patients/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(long? id)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            if (id == null)
            {
                return NotFound();
            }

            var patient = await _context.Patient
            .Where(p => p.ClinicId == UserInfo.ClinicId)
                .Include(p => p.Clinic)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (patient == null)
            {
                return NotFound();
            }

            return View(patient);
        }

        // POST: Patients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var patient = await _context.Patient.Where(p => p.ClinicId == UserInfo.ClinicId).SingleOrDefaultAsync(m => m.Id == id);
            _context.Patient.Remove(patient);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PatientExists(long id)
        {
            return _context.Patient.Any(e => e.Id == id);
        }



           
           public JsonResult GetPatients (string fname,String lname) {
            var events = _context.Patient
                .Where (a => a.ClinicId == UserInfo.ClinicId)
                .Select(a=>new{
                    a.Id,
                    a.DocNumber,
                    a.FullName,
                    a.FirstName,
                    a.LastName,
                    a.Tel,
                    a.Mobile
                })
                .Where(a=>a.FirstName.Contains(fname) && a.LastName.Contains(lname))
                .ToList ();
            return Json (new { data = events });
        }



         public JsonResult GetSearchedPatients (string pname) {
            var events = _context.Patient
                .Where (a => a.ClinicId == UserInfo.ClinicId)
                .Select(a=>new{
                    a.Id,
                    a.DocNumber,
                    a.FullName,
                    a.FirstName,
                    a.LastName,
                    a.Tel,
                    a.Mobile
                })
                .Where(a=>a.FirstName.Contains(pname) || a.LastName.Contains(pname))
                .ToList ();
            return Json (new { data = events });
        }


        


        [HttpPost]
        public async Task<IActionResult> SavePatient ([FromBody] Patient patient) {

            var status = false;

            if (!ModelState.IsValid) {
                
                return Json (status);
            } else {

            var dn = _context.Patient.LastOrDefault(a=>a.ClinicId==UserInfo.ClinicId);
            long docnumber=0;
            if (dn != null)
            {
               docnumber= long.Parse(dn.DocNumber);
                docnumber += 1;
            }
             else
            {
                docnumber = 1;
            }

                 patient.DocNumber=docnumber.ToString();

                _context.Add (patient);
                await _context.SaveChangesAsync ();
                status = true;
               return Json(new {status,pid=patient.Id});

            }
        }



    }
}
