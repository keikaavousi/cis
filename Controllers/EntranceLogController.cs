﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using System.Security.Claims;

namespace ClinicWebApp.Controllers
{
    public class EntranceLogController : Controller
    {
        private readonly ClinicDBContext _context;

        public EntranceLogController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: Messages
        [Authorize]
        public async Task<IActionResult> Index()
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            ViewData["uid"] = UserInfo.UserId;


           

            var clinicDBContext = _context.EntranceLog.Where(r=>r.UserId==UserInfo.UserId);
            return View(await clinicDBContext.ToListAsync());

        }


         [Authorize]
        public async Task<IActionResult> report(string usid)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            ViewData["uid"] = UserInfo.UserId;


            ViewData["users"]=_context.UserClinic.Include(r=>r.User)
            .Where(r=>r.ClinicId==UserInfo.ClinicId);


            var clinicDBContext = _context.EntranceLog.Where(r=>r.UserId==usid);
            return View(await clinicDBContext.ToListAsync());
            
        }

       

       

        // POST: Messages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("Id,Checkin,Checkout,UserId")] EntranceLog EntranceLog)
        {
            if (ModelState.IsValid)
            {
                _context.Add(EntranceLog);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["uid"] = UserInfo.UserId;

            return View(EntranceLog);
        }

        // GET: Messages/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            ViewData["uid"] = UserInfo.UserId;
            
            if (id == null)
            {
                return NotFound();
            }

            var EntranceLog = await _context.EntranceLog.SingleOrDefaultAsync(m => m.Id == id);

            if (EntranceLog == null)
            {
                return NotFound();
            }

            return View(EntranceLog);
        }

        // POST: Messages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Checkin,Checkout,UserId")] EntranceLog EntranceLog)
        {
            if (id != EntranceLog.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(EntranceLog);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EntranceLogExists(EntranceLog.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["uid"] = UserInfo.UserId;
            return View(EntranceLog);
        }

        // GET: Messages/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            ViewData["uid"] = UserInfo.UserId;

            if (id == null)
            {
                return NotFound();
            }

            var EntranceLog = await _context.EntranceLog
                .SingleOrDefaultAsync(m => m.Id == id);
            if (EntranceLog == null)
            {
                return NotFound();
            }

            return View(EntranceLog);
        }

        // POST: Messages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var EntranceLog = await _context.EntranceLog.SingleOrDefaultAsync(m => m.Id == id);
            _context.EntranceLog.Remove(EntranceLog);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EntranceLogExists(int id)
        {
            return _context.EntranceLog.Any(e => e.Id == id);
        }

        }
    }

