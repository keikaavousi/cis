﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using System.Net.NetworkInformation;
using System.Text;
using System.Net;
using System.Threading;
namespace ClinicWebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ClinicDBContext _context;
        public HomeController(ClinicDBContext context){
            _context=context;
        }
         private static Timer timer;
        class TimerState
            {
                public int Counter;
            }
        [Authorize]
        public IActionResult Index(DateTime? dt)
        {
                    var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                    var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
                    UserInfo.UserId = uid;
                    UserInfo.ClinicId = cid.ClinicId;


            if (dt != null)
            {
                 ViewData["date"]= Utility.ToMiladi(Convert.ToDateTime(dt));
            }
            else
            {
               ViewData["date"]= DateTime.Now;
            }

            return View();
            }



        public IActionResult Rdrct()
        {
            if (User.IsInRole("admin"))
                    {
                        return Redirect("/Clinics");
                    }
                    else if (User.IsInRole("secretary"))
                    {
                        return Redirect("/Home");
                    }
                    else if (User.IsInRole("doctor"))
                    {
                        return RedirectToAction("IndexVisit","Reservations");
                    }
                    else if (User.IsInRole("webadmin"))
                    {
                        return Redirect("/WebsiteInfo");
                    }
                    return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "تماس با ما";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
