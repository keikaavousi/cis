﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using System.Security.Claims;

namespace ClinicWebApp.Controllers
{
    public class NoteController : Controller
    {
        private readonly ClinicDBContext _context;

        public NoteController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: Messages
        [Authorize]
        public async Task<IActionResult> Index()
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            var clinicDBContext = _context.Note.Where(r=>r.ClinicId==UserInfo.ClinicId);
            return View(await clinicDBContext.ToListAsync());
        }

       
       

        // GET: Messages/Create
        [Authorize]
        public IActionResult Create()
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            ViewData["cid"] = UserInfo.ClinicId;
            return View();
        }

        // POST: Messages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("Id,Title,NoteText,Date,ClinicId")] Note Note)
        {
            if (ModelState.IsValid)
            {
                Note.Date=DateTime.Now;
                _context.Add(Note);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["cid"] = UserInfo.ClinicId;
            return View(Note);
        }

        // GET: Messages/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            
            if (id == null)
            {
                return NotFound();
            }

            var Note = await _context.Note.SingleOrDefaultAsync(m => m.Id == id);

            if (Note == null)
            {
                return NotFound();
            }
            ViewData["uid"] = UserInfo.UserId;
            return View(Note);
        }

        // POST: Messages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,NoteText,Date,ClinicId")] Note Note)
        {
            if (id != Note.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    Note.Date=DateTime.Now;
                    _context.Update(Note);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NoteExists(Note.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["cid"] = UserInfo.ClinicId;
            return View(Note);
        }

        // GET: Messages/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }
            
            if (id == null)
            {
                return NotFound();
            }

            var Note = await _context.Note
                .SingleOrDefaultAsync(m => m.Id == id);
            if (Note == null)
            {
                return NotFound();
            }

            return View(Note);
        }

        // POST: Messages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var Note = await _context.Note.SingleOrDefaultAsync(m => m.Id == id);
            _context.Note.Remove(Note);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NoteExists(int id)
        {
            return _context.Note.Any(e => e.Id == id);
        }

        }
    }

