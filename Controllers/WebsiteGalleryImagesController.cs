﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Authorization;


namespace ClinicWebApp.Controllers
{
    public class WebsiteGalleryImagesController : Controller
    {
        private readonly ClinicDBContext _context;
        private IHostingEnvironment _hostingEnvironment;

        public WebsiteGalleryImagesController(ClinicDBContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }
        public void OnGet()
        {
        }

        // GET: WebsiteGalleryImages
         [Authorize]
        public async Task<IActionResult> Index()
        {
             var wid=_context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            var clinicDBContext = _context.WebsiteGalleryImage.Include(w => w.Gallery).Where(w=>w.Gallery.WebsiteId==wid);
            return View(await clinicDBContext.ToListAsync());
        }

        // GET: WebsiteGalleryImages/Details/5
         [Authorize]
        public async Task<IActionResult> Details(long? id)
        {
           var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            if (id == null)
            {
                return NotFound();
            }

            var websiteGalleryImage = await _context.WebsiteGalleryImage
                .Include(w => w.Gallery)
                .Where(w => w.Gallery.Website.Id == wid)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (websiteGalleryImage == null)
            {
                return NotFound();
            }

            return View(websiteGalleryImage);
        }

        // GET: WebsiteGalleryImages/Create
         [Authorize]
        public IActionResult Create(long? id)

        {
            var wid=_context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;
            var gallery = _context.WebsiteGallery.Where(p => p.WebsiteId == wid).FirstOrDefault(p => p.Id == id);
            ViewData["GalleryId"] = gallery.Id;
            ViewData["GalleryTitle"] = gallery.Title;
            ViewData["GalleryImages"] = _context.WebsiteGalleryImage.Include(w => w.Gallery).Where(w => w.Gallery.WebsiteId ==wid ).Where(w=>w.GalleryId== gallery.Id).ToList().OrderByDescending(w=>w.Id);

            return View();
        }

        // POST: WebsiteGalleryImages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
         [Authorize]
        public async Task<IActionResult> Create([Bind("Id,FileUrl,GalleryId,Title")] WebsiteGalleryImage websiteGalleryImage,List<IFormFile> FileUrl)
        {
            string webRootPath = _hostingEnvironment.WebRootPath;
            string abspath = "\\images\\website_content\\gallery_content" + HttpContext.Session.GetString("cid");
            System.IO.Directory.CreateDirectory(webRootPath + abspath);
            string contentpath = "images/website_content/gallery_content" + HttpContext.Session.GetString("cid");
var wid=_context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;
            if (ModelState.IsValid)
            {

                if(FileUrl==null || FileUrl.Count == 0)
                {
                    return RedirectToAction(nameof(Create));
                }
                else
                {
                    foreach (IFormFile item in FileUrl)
                    {
                        var fileName = ContentDispositionHeaderValue.Parse(item.ContentDisposition).FileName.Trim('"');
                        var FileExtension = Path.GetExtension(fileName);


                        var imgpath = contentpath + "/" + Path.GetRandomFileName() + FileExtension;



                        using (var stream = new FileStream("wwwroot/" + imgpath, FileMode.Create))
                        {
                            await item.CopyToAsync(stream);
                            websiteGalleryImage.FileUrl = imgpath.ToString();
                        
                            var dn = _context.WebsiteGalleryImage.LastOrDefault();
                            if (dn == null)
                            {
                                websiteGalleryImage.Id = 1;
                            }
                            else
                            {
                                websiteGalleryImage.Id = dn.Id + 1;
                            }

                            //websiteGalleryImage.Title = websiteGalleryImage.Title;

                            _context.Add(websiteGalleryImage);
                            _context.Database.OpenConnection();
                            try
                            {
                                
                                _context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.websiteGalleryImage ON");
                                await _context.SaveChangesAsync();
                                _context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.websiteGalleryImage OFF");
                            }
                            finally
                            {
                                _context.Database.CloseConnection();
                            }


                        }

                    }
                }

                //_context.Add(websiteGalleryImage);
                //await _context.SaveChangesAsync();
                return RedirectToAction("Create", "WebsiteGalleryImages",websiteGalleryImage.GalleryId);
            }
            var gallery = _context.WebsiteGallery.Where(p => p.WebsiteId == wid).FirstOrDefault(p => p.Id == websiteGalleryImage.GalleryId);
            ViewData["GalleryId"] = gallery.Id;
            ViewData["GalleryTitle"] = gallery.Title;
            return View(websiteGalleryImage);
        }

        // GET: WebsiteGalleryImages/Edit/5
         [Authorize]
        public async Task<IActionResult> Edit(long? id)
        {
                       var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            if (id == null)
            {
                return NotFound();
            }

            var websiteGalleryImage = await _context.WebsiteGalleryImage
            .Where(w => w.Gallery.Website.Id == wid)
            .SingleOrDefaultAsync(m => m.Id == id);
            if (websiteGalleryImage == null)
            {
                return NotFound();
            }
            ViewData["GalleryId"] = websiteGalleryImage.Id;
            ViewData["GalleryTitle"] = websiteGalleryImage.Title;

            return View(websiteGalleryImage);
        }

        // POST: WebsiteGalleryImages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
         [Authorize]
        public async Task<IActionResult> Edit(long id, [Bind("Id,FileUrl,GalleryId,Title")] WebsiteGalleryImage websiteGalleryImage)
        {

            ViewData["GalleryId"] = websiteGalleryImage.Id;
            ViewData["GalleryTitle"] = websiteGalleryImage.Title;

            if (id != websiteGalleryImage.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
               

                try
                {
                    _context.Update(websiteGalleryImage);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WebsiteGalleryImageExists(websiteGalleryImage.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return Redirect("/WebsiteGalleryImages/Create/"+websiteGalleryImage.GalleryId);
            }

            return Redirect("/WebsiteGalleryImages/Create/" + websiteGalleryImage.GalleryId);
        }

        // GET: WebsiteGalleryImages/Delete/5
         [Authorize]
        public async Task<IActionResult> Delete(long? id)
        {
                                   var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            if (id == null)
            {
                return NotFound();
            }

            var websiteGalleryImage = await _context.WebsiteGalleryImage
                .Include(w => w.Gallery)
                .Where(w => w.Gallery.Website.Id == wid)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (websiteGalleryImage == null)
            {
                return NotFound();
            }

            return View(websiteGalleryImage);
        }

        // POST: WebsiteGalleryImages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
         [Authorize]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            var websiteGalleryImage = await _context.WebsiteGalleryImage
            .Where(w => w.Gallery.Website.Id == wid)
            .SingleOrDefaultAsync(m => m.Id == id);
            _context.WebsiteGalleryImage.Remove(websiteGalleryImage);
            await _context.SaveChangesAsync();
            return Redirect("/WebsiteGalleryImages/Create/"+websiteGalleryImage.GalleryId);

        }

        private bool WebsiteGalleryImageExists(long id)
        {
            return _context.WebsiteGalleryImage.Any(e => e.Id == id);
        }
    }
}
