﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using System.IO;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace ClinicWebApp.Controllers
{
    public class WebsitePagesController : Controller
    {
        private readonly ClinicDBContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;
        public static string main_img;

     

        public WebsitePagesController(ClinicDBContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }


       
        // GET: WebsitePages
         [Authorize]
        public async Task<IActionResult> Index()
        {
            /*get current website id*/
             var cid = UserInfo.ClinicId;
            var uid = UserInfo.UserId;
            var wid = _context.Website.FirstOrDefault(a => a.ClinicId == cid).Id;
            var c=_context.Clinic.FirstOrDefault(a=>a.Id==cid);

            HttpContext.Session.SetInt32("wid", wid);
            HttpContext.Session.SetString("cid", cid.ToString());
            HttpContext.Session.SetString("weburl", c.Url);

            

            
            /**/

            var clinicDBContext = _context.WebsitePage.Include(w => w.Website);


            return View(await clinicDBContext.ToListAsync());
        }

        // GET: WebsitePages/Details/5
         [Authorize]
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var websitePage = await _context.WebsitePage
                .Include(w => w.Website)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (websitePage == null)
            {
                return NotFound();
            }

            return View(websitePage);
        }

        // GET: WebsitePages/Create
         [Authorize]
        public IActionResult Create(long? id)
        {
            ViewData["pages"] = new SelectList(_context.WebsitePage.Where(m => m.Id != id), "Id", "Title");
           ViewData["WebsiteId"]= _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;
            ViewData["websiteUrl"] = HttpContext.Session.GetString("weburl");

            return View();
        }

        // POST: WebsitePages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
         [Authorize]
        public async Task<IActionResult> Create([Bind("Id,WebsiteId,Title,Slug,PageContent,PagePicture,ParentPage,ShowInMenu,ShowInFrontPage,Theme")] WebsitePage websitePage,IFormFile PagePicture,long? id)
        {
            string webRootPath = _hostingEnvironment.WebRootPath;
            string abspath = "\\images\\website_content\\" + HttpContext.Session.GetString("cid");
            System.IO.Directory.CreateDirectory(webRootPath + abspath);
            string contentpath = "images/website_content/" + HttpContext.Session.GetString("cid");

            if (ModelState.IsValid)
            {
                if (PagePicture == null || PagePicture.Length == 0)
                {
                    websitePage.PagePicture = null;
                }
                else
                {
                    var fileName = ContentDispositionHeaderValue.Parse(PagePicture.ContentDisposition).FileName.Trim('"');
                    var FileExtension = Path.GetExtension(fileName);


                    var imgpath = contentpath + "/" + Path.GetRandomFileName() + FileExtension;

                    using (var stream = new FileStream("wwwroot/" + imgpath, FileMode.Create))
                    {
                        await PagePicture.CopyToAsync(stream);
                        websitePage.PagePicture = imgpath.ToString();
                    }
                }
                _context.Add(websitePage);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["pages"] = new SelectList(_context.WebsitePage.Where(m => m.Id != id), "Id", "Title");
           ViewData["WebsiteId"]= _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;
            ViewData["websiteUrl"] = HttpContext.Session.GetString("weburl");
            return View(websitePage);
        }

        // GET: WebsitePages/Edit/5
         [Authorize]
        public async Task<IActionResult> Edit(long? id)
        {
           var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            if (id == null)
            {
                return NotFound();
            }

            var websitePage = await _context.WebsitePage
            .Where(w => w.Website.Id == wid)
            .SingleOrDefaultAsync(m => m.Id == id);
            if (websitePage == null)
            {
                return NotFound();
            }
            main_img = websitePage.PagePicture;
            ViewData["pages"] = new SelectList(_context.WebsitePage.Where(m=>m.Id!=id), "Id", "Title");
           ViewData["WebsiteId"]= _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            return View(websitePage);
        }

        // POST: WebsitePages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
         [Authorize]
        public async Task<IActionResult> Edit(long id, [Bind("Id,WebsiteId,Title,Slug,PageContent,PagePicture,ParentPage,ShowInMenu,ShowInFrontPage,Theme")] WebsitePage websitePage,IFormFile PagePicture)
        {
            if (id != websitePage.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                string contentpath = "images/website_content/" + HttpContext.Session.GetString("cid");

                try
                {

                    if (PagePicture == null || PagePicture.Length == 0)
                    {

                        websitePage.PagePicture = main_img;

                    }
                    else if (PagePicture.FileName != null && PagePicture.Length > 0)
                    {
                        var fileName = ContentDispositionHeaderValue.Parse(PagePicture.ContentDisposition).FileName.Trim('"');
                        var FileExtension = Path.GetExtension(fileName);


                        var logopath = contentpath + "/" + Path.GetRandomFileName() + FileExtension;

                        using (var stream = new FileStream("wwwroot/" + logopath, FileMode.Create))
                        {
                            await PagePicture.CopyToAsync(stream);
                            websitePage.PagePicture = logopath.ToString();
                        }
                    }
                    _context.Update(websitePage);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WebsitePageExists(websitePage.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["pages"] = new SelectList(_context.WebsitePage.Where(m => m.Id != id), "Id", "Title");
           ViewData["WebsiteId"]= _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;
            return View(websitePage);
        }

        // GET: WebsitePages/Delete/5
         [Authorize]
        public async Task<IActionResult> Delete(long? id)
        {
           var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            if (id == null)
            {
                return NotFound();
            }

            var websitePage = await _context.WebsitePage
             .Where(w => w.Website.Id == wid)
                .Include(w => w.Website)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (websitePage == null)
            {
                return NotFound();
            }

            return View(websitePage);
        }

        // POST: WebsitePages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
         [Authorize]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            var websitePage = await _context.WebsitePage
             .Where(w => w.Website.Id == wid)
            .SingleOrDefaultAsync(m => m.Id == id);
            _context.WebsitePage.Remove(websitePage);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WebsitePageExists(long id)
        {
            return _context.WebsitePage.Any(e => e.Id == id);
        }

        [Authorize]
        public IActionResult CheckExistingSlug(string Slug, int Id, int WebsiteId)
        {
            var ed = _context.WebsitePage.Where(a => a.Slug == Slug).Where(a => a.WebsiteId == WebsiteId).Where(a => a.Id != Id);

            if (ed.Any())
            {
                return Json(data: "این عنوان برای نامک تکراری است");
            }
            else
            {
                return Json(data: true);
            }


        }
    }
}
