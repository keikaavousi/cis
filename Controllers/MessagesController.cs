﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using System.Security.Claims;

namespace ClinicWebApp.Controllers
{
    public class MessagesController : Controller
    {
        private readonly ClinicDBContext _context;

        public MessagesController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: Messages
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Index()
        {
            var clinicDBContext = _context.Message.Include(m => m.Sender);
            return View(await clinicDBContext.ToListAsync());
        }

        [Authorize]
        public async Task<IActionResult> IndexMessage(int? page)
        {
            if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            // var user = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            // var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == user);
            ViewData["cid"] = UserInfo.ClinicId;
            ViewData["uid"] = UserInfo.UserId;
            int pageSize = 10;
            return View(await PaginatedList<Message>.CreateAsync(_context.Message.Where(m=>m.SenderId==UserInfo.ClinicId).AsNoTracking(), page ?? 1, pageSize));
        }

        // GET: Messages/Details/5
         [Authorize(Roles="admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var message = await _context.Message
                .Include(m => m.Sender)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (message == null)
            {
                return NotFound();
            }

            return View(message);
        }



        // GET: Messages/Details/5
       [Authorize]
        public async Task<IActionResult> DetailsMessage(int? id)
        {
            if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }
            
            if (id == null)
            {
                return NotFound();
            }

            var message = await _context.Message
                .Include(m => m.Sender)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (message == null)
            {
                return NotFound();
            }

            return View(message);
        }

        // GET: Messages/Create
        [Authorize]
        public IActionResult Create()
        {
            ViewData["cid"] = UserInfo.ClinicId;
            ViewData["uid"] = UserInfo.UserId;
            ViewData["SenderId"] = UserInfo.ClinicId;


            //var clinicDBContext = _context.Message.Include(m => m.Sender).Where(m=>m.SenderId==UserInfo.ClinicId).ToList();
           
            return View();
        }

        // POST: Messages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("Id,SenderId,Date,MessageText,Reply")] Message message)
        {
              ViewData["cid"] = UserInfo.ClinicId;
            ViewData["uid"] = UserInfo.UserId;
            //ViewData["wid"]= _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;
            
            if (ModelState.IsValid)
            {
                message.Date = DateTime.Now;
                _context.Add(message);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(IndexMessage));
            }
            ViewData["SenderId"] = UserInfo.ClinicId;
            return View(message);
        }

        // GET: Messages/Edit/5
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var message = await _context.Message.SingleOrDefaultAsync(m => m.Id == id);
            var dbview = _context.Message.Include(a => a.Sender).SingleOrDefault(a => a.Id == id);
            ViewData["FullName"] = dbview.Sender.FullName;
        
            if (message == null)
            {
                return NotFound();
            }
            return View(message);
        }

        // POST: Messages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,SenderId,Date,MessageText,Reply")] Message message)
        {
            if (id != message.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(message);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MessageExists(message.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(message);
        }

        // GET: Messages/Delete/5
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var message = await _context.Message
                .Include(m => m.Sender)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (message == null)
            {
                return NotFound();
            }

            return View(message);
        }

        // POST: Messages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var message = await _context.Message.SingleOrDefaultAsync(m => m.Id == id);
            _context.Message.Remove(message);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MessageExists(int id)
        {
            return _context.Message.Any(e => e.Id == id);
        }


        [HttpPost]
        public async Task<IActionResult> SendMsg ([FromBody] Message Message) {

            var status = false;

            if (!ModelState.IsValid) {
                
                return Json (status);
            } else {
                _context.Add (Message);
                await _context.SaveChangesAsync ();
                status = true;
               return Json(status);

            }
        }
    }
}
