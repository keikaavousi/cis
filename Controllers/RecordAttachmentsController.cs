﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;
using System.Security.Claims;
using System.Net.NetworkInformation;
using System.Text;
using System.Net;
using System.Threading;

namespace ClinicWebApp.Controllers
{
    public class RecordAttachmentsController : Controller
    {
        private readonly ClinicDBContext _context;
        private IHostingEnvironment _hostingEnvironment;



       

        public RecordAttachmentsController(ClinicDBContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }
        public void OnGet()
        {
        }
        // GET: RecordAttachments
        [Authorize]
        public async Task<IActionResult> Index(long? patientId)
        {
             if(UserInfo.ClinicId==0){
            var uiid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var ciid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uiid);
            UserInfo.UserId = uiid;
            UserInfo.ClinicId = ciid.ClinicId;
            }

            

            if (!string.IsNullOrEmpty(patientId.ToString()))
            {
                var clinicDBContext = _context.RecordAttachment.Include(r => r.Patient).Where(r => r.PatientId == patientId)
                 .Where(r => r.Patient.ClinicId == UserInfo.ClinicId)
                .OrderByDescending(r => r.Id).ToList();
                if (!clinicDBContext.Any())
                {
                    ViewData["msg"] = "فایلی در پرونده این بیمار وجود ندارد";

                }
                var patient = _context.Patient.Where(r => r.Id == patientId).FirstOrDefault();
                ViewData["patientName"] = patient.FullName;
                ViewData["PatientId"]=patientId;
                return View(clinicDBContext);

            }
            else
            {
                var clinicDBContext = _context.RecordAttachment.Include(r => r.Patient).OrderByDescending(r => r.Id);

                return View(await clinicDBContext.ToListAsync());

            }

        }

        // GET: RecordAttachments/Details/5
        [Authorize]
        public async Task<IActionResult> Details(long? id)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }
            if (id == null)
            {
                return NotFound();
            }

            var recordAttachment = await _context.RecordAttachment
                .Include(r => r.Patient)
                .Where(r => r.Patient.ClinicId == UserInfo.ClinicId)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (recordAttachment == null)
            {
                return NotFound();
            }

          
                
            return View(recordAttachment);
        }

        // GET: RecordAttachments/Create
        [Authorize]
        public IActionResult Create()
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            ViewData["PatientId"] = new SelectList(_context.Patient, "Id", "FirstName");
            return View();
        }

        // POST: RecordAttachments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.


 [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Createb([Bind("Id,PatientId,FileUrl,UploadDateTime,Title")] RecordAttachment recordAttachment)
        {
             var pid = recordAttachment.PatientId;
            if (ModelState.IsValid)
            {
                DateTime now = DateTime.Now;
                recordAttachment.UploadDateTime = now;
                 _context.Add(recordAttachment);
                 await _context.SaveChangesAsync();
                 
            }

            TempData["status"] = 1;
            return Redirect("/RecordAttachments/Index?patientId="+ pid);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("Id,PatientId,FileUrl,UploadDateTime,Title")] RecordAttachment recordAttachment, List<IFormFile> FileUrl)
        {
            var pid = recordAttachment.PatientId;

            
            if (ModelState.IsValid)
            {
                DateTime now = DateTime.Now;
                recordAttachment.UploadDateTime = now;
                if (FileUrl == null || FileUrl.Count == 0)
                {
                    recordAttachment.FileUrl = null;
                }
                else
                {

                    foreach (IFormFile item in FileUrl)
                    {

                        //get extention
                        var ext = Path.GetExtension(item.FileName);

                        //file name
                        var fileName = Path.GetRandomFileName()+ext;
                        var fullPath = "Records/records_images/";

                        using (var stream = new FileStream(fullPath + fileName, FileMode.Create))
                        {
                            item.CopyTo(stream);
                            recordAttachment.FileUrl = fileName;

                            var dn = _context.RecordAttachment.LastOrDefault();
                            if (dn == null)
                            {
                                recordAttachment.Id = 1;
                            }
                            else
                            {
                                recordAttachment.Id = dn.Id + 1;
                            }


                            _context.Add(recordAttachment);
                            _context.Database.OpenConnection();
                            try
                            {

                                _context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.RecordAttachment ON");
                                await _context.SaveChangesAsync();
                                _context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.RecordAttachment OFF");
                            }
                            finally
                            {
                                _context.Database.CloseConnection();
                            }


                        }

                    }


                }

            }
            TempData["status"] = 1;
            return Redirect("/RecordAttachments/Index?patientId="+ pid);
        }


        


        // GET: RecordAttachments/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(long? id)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            if (id == null)
            {
                return NotFound();
            }

            var recordAttachment = await _context.RecordAttachment
            .Where(r => r.Patient.ClinicId == UserInfo.ClinicId)
            .SingleOrDefaultAsync(m => m.Id == id);
            if (recordAttachment == null)
            {
                return NotFound();
            }
            ViewData["PatientId"] = new SelectList(_context.Patient, "Id", "FirstName", recordAttachment.PatientId);
            return View(recordAttachment);
        }

        // POST: RecordAttachments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(long id, [Bind("Id,PatientId,FileUrl,UploadDateTime,Title")] RecordAttachment recordAttachment)
        {
            long pid=recordAttachment.PatientId;
            if (id != recordAttachment.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(recordAttachment);
                    await _context.SaveChangesAsync();
                   TempData["status"]=1;
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RecordAttachmentExists(recordAttachment.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                
               return Redirect("/RecordAttachments/Index?patientId="+pid);
            }
            ViewData["PatientId"] = new SelectList(_context.Patient, "Id", "FirstName", recordAttachment.PatientId);
            return Redirect("/RecordAttachments/Index?patientId="+pid);
        }


        public static long? pid=0;
        
        // GET: RecordAttachments/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(long? id)
        {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            if (id == null)
            {
                return NotFound();
            }

            
            var recordAttachment = await _context.RecordAttachment
                .Include(r => r.Patient)
                .Where(r => r.Patient.ClinicId == UserInfo.ClinicId)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (recordAttachment == null)
            {
                return NotFound();
            }
            pid=recordAttachment.Patient.Id;
            return View(recordAttachment);
        }

        // POST: RecordAttachments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var recordAttachment = await _context.RecordAttachment.SingleOrDefaultAsync(m => m.Id == id);
            _context.RecordAttachment.Remove(recordAttachment);
            await _context.SaveChangesAsync();
            deletepic(recordAttachment.FileUrl);




            return Redirect("/RecordAttachments/Index?patientId="+pid);
        }

        [Authorize]
        public void deletepic(string fileurl)
        {
            var dirPath = "Records/records_images/";
            System.IO.File.Delete(Path.Combine(dirPath,fileurl));


            return;
        }

        [Authorize]
        private bool RecordAttachmentExists(long id)
        {
            return _context.RecordAttachment.Any(e => e.Id == id);
        }
    }
}
