﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using ClinicWebApp.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace ClinicWebApp.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly ClinicDBContext _context;

        public CategoriesController(ClinicDBContext context)
        {
            _context = context;
        }

       

        // GET: Categories
        [Authorize]
        public async Task<IActionResult> Index()
        {

            ViewData["cid"] = UserInfo.ClinicId;
            ViewData["uid"] = UserInfo.UserId;
            long wid= _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;
            ViewData["wid"]=wid;
          


            var clinicDBContext = _context.Category.Include(c => c.Website).Where(c=>c.WebsiteId == wid);
            return View(await clinicDBContext.ToListAsync());
        }

        // GET: Categories/Details/5
        [Authorize]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category = await _context.Category
                .Include(c => c.Website)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        // GET: Categories/Create
        [Authorize]
        public IActionResult Create()
        {
         ViewData["WebsiteId"] = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;;

            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("Id,Title,Slug,WebsiteId,ImageUrl,ShowInFrontPage")] Category category)
        {
            if (ModelState.IsValid)
            {
                _context.Add(category);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["WebsiteId"] = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;;
            return View(category);
        }

        // GET: Categories/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category = await _context.Category.SingleOrDefaultAsync(m => m.Id == id);
            if (category == null)
            {
                return NotFound();
            }
             ViewData["WebsiteId"] = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;;
            return View(category);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Slug,WebsiteId,ImageUrl,ShowInFrontPage")] Category category)
        {
            if (id != category.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(category);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CategoryExists(category.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
             ViewData["WebsiteId"] = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;;
            return View(category);
        }

        // GET: Categories/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category = await _context.Category
                .Include(c => c.Website)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var category = await _context.Category.SingleOrDefaultAsync(m => m.Id == id);
            _context.Category.Remove(category);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CategoryExists(int id)
        {
            return _context.Category.Any(e => e.Id == id);
        }


     
        public IActionResult CheckExistingSlug(string Slug, int Id, int WebsiteId)
        {
            var ed = _context.Category.Where(a => a.Slug == Slug).Where(a => a.WebsiteId == WebsiteId).Where(a => a.Id != Id);
           
                if (ed.Any())
                {
                    return Json(data: "این عنوان برای نامک تکراری است");
                }
                else
                {
                    return Json(data: true);
                }
           

        }
    }
}
