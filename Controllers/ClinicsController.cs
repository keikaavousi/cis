﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using System.Globalization;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Threading;

namespace ClinicWebApp.Controllers
{
    public class ClinicsController : Controller
    {
        private readonly ClinicDBContext _context;
        public static string lgf;
        public ClinicsController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: Clinics
        // [Authorize(Roles="admin")]
        public async Task<IActionResult> Index()
        {
            var clinicDBContext = _context.Clinic.Include(c => c.Speciality);
            return View(await clinicDBContext.ToListAsync());
        }

        // GET: Clinics/Details/5
        //  [Authorize(Roles="admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var clinic = await _context.Clinic
                .Include(c => c.Speciality)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (clinic == null)
            {
                return NotFound();
            }

           
            return View(clinic);
        }

        // GET: Clinics/Create
        //  [Authorize(Roles="admin")]
        public IActionResult Create()
        {
            
            ViewData["SpecialityId"] = new SelectList(_context.Speciality, "Id", "Title");
            return View();
        }

        // POST: Clinics/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //  [Authorize(Roles="admin")]
        public async Task<IActionResult>  Create([Bind("Id,FirstName,LastName,Address,Tel,VisitPrice,Mobile,Email,Url,Website,ClinicPanel,ReservationPanel,StartDate,Price,SupportPercent,Description,Enabled,SpecialityId,LogoFile")] Clinic clinic, IFormFile LogoFile)
        {

            if (ModelState.IsValid)
            {
                if(clinic.Price!=null){
                    clinic.Price = clinic.Price.Replace(",", "");
                    }

                    if(clinic.VisitPrice!=null){
                    clinic.VisitPrice=clinic.VisitPrice.Replace(",", "");
                   }


                //return Content("file not selected");

                if (LogoFile == null || LogoFile.Length == 0)
                {
                    clinic.LogoFile = null;
                }
                else{
                    var path = "images/logo_files/" + Path.GetRandomFileName() + ".jpg";

                    using (var stream = new FileStream("wwwroot/" + path, FileMode.Create))
                    {
                        await LogoFile.CopyToAsync(stream);
                        clinic.LogoFile = path.ToString();
                    }
                }
                // return RedirectToAction("Files");
               



                
                _context.Add(clinic);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["SpecialityId"] = new SelectList(_context.Speciality, "Id", "Title", clinic.SpecialityId);
            return View(clinic);
        }

        // GET: Clinics/Edit/5
        //  [Authorize(Roles="admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var clinic = await _context.Clinic.SingleOrDefaultAsync(m => m.Id == id);
            if (clinic == null)
            {
                return NotFound();
            }
            ViewData["StartDate"] = clinic.StartDate.ToString("yyyy-MM-dd");
            ViewData["SpecialityId"] = new SelectList(_context.Speciality, "Id", "Title", clinic.SpecialityId);
            //ViewData["LogoFile"] = clinic.LogoFile;
            lgf = clinic.LogoFile;
            return View(clinic);
        }

        // POST: Clinics/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //  [Authorize(Roles="admin")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FirstName,LastName,Address,Tel,VisitPrice,Mobile,Email,Url,Website,ClinicPanel,ReservationPanel,StartDate,Price,SupportPercent,Description,Enabled,SpecialityId,LogoFile")] Clinic clinic,IFormFile LogoFileUploader)
        {
            if (id != clinic.Id)
            {
                return NotFound();
            }
            
            if (ModelState.IsValid)
            {
                try
                {
                    if(clinic.Price!=null){
                    clinic.Price = clinic.Price.Replace(",", "");
                    }

                    if(clinic.VisitPrice!=null){
                    clinic.VisitPrice=clinic.VisitPrice.Replace(",", "");
                   }

                     if (LogoFileUploader == null)
                    {
                        
                            clinic.LogoFile = lgf;
                            //_context.Update(clinic);
                            //await _context.SaveChangesAsync();
                        
                    }
                    else if (LogoFileUploader.FileName !=null && LogoFileUploader.Length>0)
                    {

                        var path = "images/logo_files/" + Path.GetRandomFileName() + ".jpg";

                        using (var stream = new FileStream("wwwroot/" + path, FileMode.Create))
                        {
                            await LogoFileUploader.CopyToAsync(stream);
                            clinic.LogoFile = path.ToString();
                        }
                       
                    }
                   

                    _context.Update(clinic);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClinicExists(clinic.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["SpecialityId"] = new SelectList(_context.Speciality, "Id", "Title", clinic.SpecialityId);
            return View(clinic);
        }

        // GET: Clinics/Delete/5
        //  [Authorize(Roles="admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var clinic = await _context.Clinic
                .Include(c => c.Speciality)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (clinic == null)
            {
                return NotFound();
            }

            return View(clinic);
        }

        // POST: Clinics/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
         [Authorize(Roles="admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var clinic = await _context.Clinic.SingleOrDefaultAsync(m => m.Id == id);
            _context.Clinic.Remove(clinic);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ClinicExists(int id)
        {
            return _context.Clinic.Any(e => e.Id == id);
        }
    }
}
