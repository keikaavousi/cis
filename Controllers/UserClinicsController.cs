﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Authorization;

namespace ClinicWebApp.Controllers
{
    public class UserClinicsController : Controller
    {
        private readonly ClinicDBContext _context;

        public UserClinicsController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: UserClinics
        [Authorize]
        public async Task<IActionResult> Index(int? id)
        {
            var clinicDBContext = _context.UserClinic.Include(u => u.Clinic).Include(u => u.User).Where(u=>u.ClinicId==id);
            
            var clinicname = _context.UserClinic.Include(u => u.Clinic).Include(u => u.User).FirstOrDefault(u => u.ClinicId == id);

if(clinicname!=null){
            string cname = clinicname.Clinic.FullName;

            ViewData["ClinicName"] = cname;
}           

ViewData["id"] = id;
            return View(await clinicDBContext.ToListAsync());
        }

        // GET: UserClinics/Details/5
        [Authorize]
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userClinic = await _context.UserClinic
                .Include(u => u.Clinic)
                .Include(u => u.User)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (userClinic == null)
            {
                return NotFound();
            }

            return View(userClinic);
        }

        // GET: UserClinics/Create
        [Authorize]
        public IActionResult Create()
        {
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "Address");
            ViewData["UserId"] = new SelectList(_context.AspNetUsers, "Id", "Id");
            return View();
        }

        // POST: UserClinics/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("Id,UserId,ClinicId")] UserClinic userClinic)
        {
            if (ModelState.IsValid)
            {
                _context.Add(userClinic);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "Address", userClinic.ClinicId);
            ViewData["UserId"] = new SelectList(_context.AspNetUsers, "Id", "Id", userClinic.UserId);
            return View(userClinic);
        }

        // GET: UserClinics/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userClinic = await _context.UserClinic.SingleOrDefaultAsync(m => m.Id == id);
            if (userClinic == null)
            {
                return NotFound();
            }
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "Address", userClinic.ClinicId);
            ViewData["UserId"] = new SelectList(_context.AspNetUsers, "Id", "Id", userClinic.UserId);
            return View(userClinic);
        }

        // POST: UserClinics/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(long id, [Bind("Id,UserId,ClinicId")] UserClinic userClinic)
        {
            if (id != userClinic.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(userClinic);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserClinicExists(userClinic.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "Address", userClinic.ClinicId);
            ViewData["UserId"] = new SelectList(_context.AspNetUsers, "Id", "Id", userClinic.UserId);
            return View(userClinic);
        }

        // GET: UserClinics/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userClinic = await _context.UserClinic
                .Include(u => u.Clinic)
                .Include(u => u.User)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (userClinic == null)
            {
                return NotFound();
            }

            return View(userClinic);
        }

        // POST: UserClinics/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var userClinic = await _context.UserClinic.SingleOrDefaultAsync(m => m.Id == id);
            _context.UserClinic.Remove(userClinic);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserClinicExists(long id)
        {
            return _context.UserClinic.Any(e => e.Id == id);
        }
    }
}
