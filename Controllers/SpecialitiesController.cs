﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace ClinicWebApp.Controllers
{
    public class SpecialitiesController : Controller
    {
        private readonly ClinicDBContext _context;
        public static string imgf;
        public SpecialitiesController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: Specialities
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Speciality.ToListAsync());
        }

        // GET: Specialities/Details/5
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var speciality = await _context.Speciality
                .SingleOrDefaultAsync(m => m.Id == id);
            if (speciality == null)
            {
                return NotFound();
            }

            return View(speciality);
        }

        // GET: Specialities/Create
        [Authorize(Roles="admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Specialities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Create([Bind("Id,Title,ImageFile")] Speciality speciality, IFormFile ImageFile)
        {
            if (ModelState.IsValid)
            {
                //return Content("file not selected");

                if (ImageFile == null || ImageFile.Length == 0)
                {
                    speciality.ImageFile = null;
                }
                else
                {
                    var path = "images/speciality_files/" + ImageFile.FileName;

                    using (var stream = new FileStream("wwwroot/" + path, FileMode.Create))
                    {
                        await ImageFile.CopyToAsync(stream);
                        speciality.ImageFile = path.ToString();
                    }
                }

                _context.Add(speciality);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(speciality);
        }

        // GET: Specialities/Edit/5
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var speciality = await _context.Speciality.SingleOrDefaultAsync(m => m.Id == id);
            if (speciality == null)
            {
                return NotFound();
            }
            imgf = speciality.ImageFile;
            return View(speciality);
        }

        // POST: Specialities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,ImageFile")] Speciality speciality,IFormFile ImageFile)
        {
            if (id != speciality.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (ImageFile == null)
                    {
                        if (speciality.ImageFile == null)
                        {
                            speciality.ImageFile = imgf;
                        }
                    }
                    else if (ImageFile.FileName != null && ImageFile.Length > 0)
                    {

                        var path = "images/speciality_files/" + ImageFile.FileName;

                        using (var stream = new FileStream("wwwroot/" + path, FileMode.Create))
                        {
                            await ImageFile.CopyToAsync(stream);
                            speciality.ImageFile = path.ToString();
                        }

                    }
                    _context.Update(speciality);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SpecialityExists(speciality.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(speciality);
        }

        // GET: Specialities/Delete/5
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var speciality = await _context.Speciality
                .SingleOrDefaultAsync(m => m.Id == id);
            if (speciality == null)
            {
                return NotFound();
            }

            return View(speciality);
        }

        // POST: Specialities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var speciality = await _context.Speciality.SingleOrDefaultAsync(m => m.Id == id);
            _context.Speciality.Remove(speciality);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SpecialityExists(int id)
        {
            return _context.Speciality.Any(e => e.Id == id);
        }
    }
}
