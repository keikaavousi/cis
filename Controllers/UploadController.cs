   using System.Collections.Generic;
   using System.IO;
   using System.Linq;
   using System.Net.Http.Headers;
   using System.Security.Claims;
   using System.Threading.Tasks;
   using System;
   using ClinicWebApp.Models;
   using Microsoft.AspNetCore.Authorization;
   using Microsoft.AspNetCore.Hosting;
   using Microsoft.AspNetCore.Http;
   using Microsoft.AspNetCore.Mvc.Rendering;
   using Microsoft.AspNetCore.Mvc;
   using Microsoft.EntityFrameworkCore;

   namespace ClinicWebApp.Controllers {
       public class Item {
           public bool success { get; set; }
           public string url { get; set; }
       }

       [Authorize]
       public class UploadController : Controller {
           private readonly ClinicDBContext _context;
           private IHostingEnvironment _hostingEnvironment;

           public UploadController (ClinicDBContext context, IHostingEnvironment hostingEnvironment) {
               _context = context;
               _hostingEnvironment = hostingEnvironment;
           }
           public void OnGet () { }

           [HttpPost]
           [ValidateAntiForgeryToken]
           [Authorize]
           public IActionResult UploadFilesAjax () {
               long size = 0;
               var files = Request.Form.Files;
               var filename = "";
               foreach (var file in files) {
                   filename = ContentDispositionHeaderValue
                       .Parse (file.ContentDisposition)
                       .FileName
                       .Trim ('"');
                   filename = _hostingEnvironment.WebRootPath + $@"\{filename}";
                   size += file.Length;
                   using (FileStream fs = System.IO.File.Create (filename)) {
                       file.CopyTo (fs);
                       fs.Flush ();
                   }
               }
               return Json (new Item { success = true, url = filename });
           }
       }
   }