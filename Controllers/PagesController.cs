﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;

namespace ClinicWebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Pages")]
    public class PagesController : Controller
    {
        private readonly ClinicDBContext _context;

        public PagesController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: api/Pages
        [HttpGet]
        public IEnumerable<WebsitePage> GetWebsitePage()
        {
            return _context.WebsitePage;
        }

        // GET: api/Pages/5
        [HttpGet("{id}")]
        public IEnumerable<WebsitePage> GetWebsitePage([FromRoute] long id)
        {
               var websitePage = _context.WebsitePage.Where(m => m.WebsiteId == id).ToList();
                return websitePage;
            
        }


         [HttpGet("pagedet")]
        public IEnumerable<WebsitePage> GetWebsitePageDetails([FromQuery(Name ="pid")] long pid)
        {
           var websitePage = _context.WebsitePage.Where(m=>m.Id == pid);
                return websitePage;

            
        }




//  [HttpGet("{id}")]
//         public IEnumerable<WebsitePage> GetWebsitePage([FromRoute] long id, [FromQuery(Name ="pid")] long pid)
//         {
//             if (pid!=null)
//             {
//                var websitePage = _context.WebsitePage.Where(m => m.WebsiteId == id && m.Id == pid);
//                 return websitePage;
//             }
//             else
//             {
//                var websitePage = _context.WebsitePage.Where(m => m.WebsiteId == id).ToList();
//                 return websitePage;
//             }

            
//         }



       
        //[HttpGet]
        //public WebsitePage GetWebsitePageByName([FromQuery] long cid, [FromQuery] string slug)
        //{
        //    var websitePage = _context.WebsitePage.SingleOrDefault(m => m.WebsiteId == cid && m.Slug == slug);
        //    return websitePage;
        //}


        // PUT: api/Pages/5
        // [HttpPut("{id}")]
        // public async Task<IActionResult> PutWebsitePage([FromRoute] long id, [FromBody] WebsitePage websitePage)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     if (id != websitePage.Id)
        //     {
        //         return BadRequest();
        //     }

        //     _context.Entry(websitePage).State = EntityState.Modified;

        //     try
        //     {
        //         await _context.SaveChangesAsync();
        //     }
        //     catch (DbUpdateConcurrencyException)
        //     {
        //         if (!WebsitePageExists(id))
        //         {
        //             return NotFound();
        //         }
        //         else
        //         {
        //             throw;
        //         }
        //     }

        //     return NoContent();
        // }

        // POST: api/Pages
        // [HttpPost]
        // public async Task<IActionResult> PostWebsitePage([FromBody] WebsitePage websitePage)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     _context.WebsitePage.Add(websitePage);
        //     await _context.SaveChangesAsync();

        //     return CreatedAtAction("GetWebsitePage", new { id = websitePage.Id }, websitePage);
        // }

        // // DELETE: api/Pages/5
        // [HttpDelete("{id}")]
        // public async Task<IActionResult> DeleteWebsitePage([FromRoute] long id)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     var websitePage = await _context.WebsitePage.SingleOrDefaultAsync(m => m.Id == id);
        //     if (websitePage == null)
        //     {
        //         return NotFound();
        //     }

        //     _context.WebsitePage.Remove(websitePage);
        //     await _context.SaveChangesAsync();

        //     return Ok(websitePage);
        // }

        // private bool WebsitePageExists(long id)
        // {
        //     return _context.WebsitePage.Any(e => e.Id == id);
        // }
    }
}