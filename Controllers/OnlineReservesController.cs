﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using System.Security.Claims;

namespace ClinicWebApp.Controllers
{
    public class OnlineReservesController : Controller
    {
        private readonly ClinicDBContext _context;

        public OnlineReservesController(ClinicDBContext context)
        {
            _context = context;
        }


       
            // GET: OnlineReserves
            [Authorize]
            public async Task<IActionResult> Index() {


           var notconfirmed= from p in _context.OnlineReserve.Where(a => a.ClinicId == UserInfo.ClinicId)
            where !_context.Reservation.Any(es => (es.Date == p.Date) && (es.Time == p.Time))
            select p;
            return View(notconfirmed);
           // return View(await _context.OnlineReserve.ToListAsync());

        }

        // GET: OnlineReserves/Details/5
        [Authorize]
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var onlineReserve = await _context.OnlineReserve
                .SingleOrDefaultAsync(m => m.Id == id);
            if (onlineReserve == null)
            {
                return NotFound();
            }

            return View(onlineReserve);
        }

        // GET: OnlineReserves/Create
       
        public IActionResult Create()
        {

             var user = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a=>a.UserId== user);
            ViewData["cid"] = cid.ClinicId;
            ViewData["uid"] = user;
            
          
            //freeDate
            var dateselectlist = _context.ReservationFreeDate.Where(a => a.Date >= DateTime.Now).Where(a => a.ClinicId == cid.ClinicId).OrderBy(a => a.Date).Select(s => new
            {
                date = s.Date,
                convertedDate = s.Date.ToString("yyyy-MM-dd")
        })
    .ToList();


           ViewData["rfd"]=new SelectList(dateselectlist, "date", "convertedDate",new{@class="eDate"});

            return View();
        }

        
        public JsonResult GetTimes(DateTime DateId)
        {
            var times = _context.ReservationFreeDate.Where(a => a.Date == Convert.ToDateTime(DateId)).FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId);
            var stime = times.StartTime;
            var etime = times.EndTime;
            IList<TimeSpan> timeArray = new List<TimeSpan>();
            var timepervisit = times.TimePerVisit;
            //TimeSpan min = new TimeSpan(0, 0, 15, 0, 0);
            TimeSpan min = TimeSpan.FromMinutes(timepervisit);

            timeArray.Add(stime);
            // for (int i = 0; stime.Add(min) < etime && i <= times.PatientPerDay; i++)
            for (int i = 0; stime.Add(min) < etime; i++)
            {
                stime = stime.Add(min);
                timeArray.Add(stime);
            }


            IList<TimeSpan> freeTimeArray = new List<TimeSpan>();
            foreach (var t in timeArray)
            {
                
                var onreserved = _context.OnlineReserve.Where(a => a.Time == t)
                .Where(a => a.Date == Convert.ToDateTime(DateId))
                .Where(a=>a.PaymentStatus==true)
                .FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId);

                 var tempreserved = _context.OnlineReserve.Where(a => a.Time == t)
                .Where(a => a.Date == Convert.ToDateTime(DateId))
                .Where(a=> (DateTime.Now-a.NowDate).TotalSeconds<5*60)
                .FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId);


                 var offreserved = _context.Reservation.Where(a => a.Time == t)
                .Where(a => a.Date == Convert.ToDateTime(DateId))
                .FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId);
                if (onreserved == null && offreserved==null && tempreserved==null)
                {
                    freeTimeArray.Add(t);
                }
            }
            return Json(new{freeTimeArray=freeTimeArray,times=timeArray.Count()});

        }




        // POST: OnlineReserves/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FirstName,LastName,Mobile,Email,Date,Time,ClinicId")] OnlineReserve onlineReserve)
        {
             var user = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a=>a.UserId== user);
            ViewData["cid"] = cid.ClinicId;
            ViewData["uid"] = user;


            if (ModelState.IsValid)
            {
                _context.Add(onlineReserve);
                await _context.SaveChangesAsync();

            return RedirectToAction("Index");
            }
            return View(onlineReserve);
        }


        // GET: OnlineReserves/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var dateselectlist = _context.ReservationFreeDate.Where(a => a.Date >= DateTime.Now).Where(a => a.ClinicId == UserInfo.ClinicId).OrderBy(a => a.Date).Select(s => new
            {
                date = s.Date,
                convertedDate = s.Date.ToString("yyyy-MM-dd")
            })
      .ToList();
            ViewData["rfd"] = new SelectList(dateselectlist, "date", "convertedDate",new{@class="eDate"});



            var onlineReserve = await _context.OnlineReserve.SingleOrDefaultAsync(m => m.Id == id);

            if (onlineReserve == null)
            {
                return NotFound();
            }
            return View(onlineReserve);
        }

        // POST: OnlineReserves/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(long id, [Bind("Id,FirstName,LastName,Mobile,Email,Date,Time")] OnlineReserve onlineReserve)
        {
            if (id != onlineReserve.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(onlineReserve);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OnlineReserveExists(onlineReserve.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(onlineReserve);
        }

        // GET: OnlineReserves/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var onlineReserve = await _context.OnlineReserve
                .SingleOrDefaultAsync(m => m.Id == id);
            if (onlineReserve == null)
            {
                return NotFound();
            }

            return View(onlineReserve);
        }

        // POST: OnlineReserves/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var onlineReserve = await _context.OnlineReserve.SingleOrDefaultAsync(m => m.Id == id);
            _context.OnlineReserve.Remove(onlineReserve);
            await _context.SaveChangesAsync();
            return RedirectToAction("Create","ReservationFreeDates");
        }

        private bool OnlineReserveExists(long id)
        {
            return _context.OnlineReserve.Any(e => e.Id == id);
        }
    }
}
