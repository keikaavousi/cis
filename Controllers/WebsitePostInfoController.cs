﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;

namespace ClinicWebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/WebsitePostInfo")]

    public class WebsitePostInfoController : Controller
    {
        private readonly ClinicDBContext _context;

        public WebsitePostInfoController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: api/WebsitePostInfo
        [HttpGet]
        public IEnumerable<WebsitePost> GetWebsitePost()
        {
            return _context.WebsitePost;
        }

        // GET: api/WebsitePostInfo/5

        [HttpGet("{cid}")]
        public IEnumerable<WebsitePost> GetWebsitePost([FromRoute] long cid, [FromQuery] long? category)
        {
            if (category == null)
            {
                    return _context.WebsitePost.Where(m => m.WebsiteId == cid).Where(m => m.Visible == true).OrderByDescending(m=>m.Id).Take(6);

            }
            else
            {
                return _context.WebsitePost.Include(m=>m.Category).Where(m => m.CategoryId == category).Where(m => m.Visible == true).Where(m => m.WebsiteId == cid);

            }
        }


        //[HttpGet("{id}")]
        //public IEnumerable<WebsitePost> GetWebsitePost([FromRoute] long id)
        //{
        //    return _context.WebsitePost.Where(m => m.WebsiteId == id).Where(m => m.Visible == true);
        //}




        // PUT: api/WebsitePostInfo/5
        // [HttpPut("{id}")]
        // public async Task<IActionResult> PutWebsitePost([FromRoute] long id, [FromBody] WebsitePost websitePost)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     if (id != websitePost.Id)
        //     {
        //         return BadRequest();
        //     }

        //     _context.Entry(websitePost).State = EntityState.Modified;

        //     try
        //     {
        //         await _context.SaveChangesAsync();
        //     }
        //     catch (DbUpdateConcurrencyException)
        //     {
        //         if (!WebsitePostExists(id))
        //         {
        //             return NotFound();
        //         }
        //         else
        //         {
        //             throw;
        //         }
        //     }

        //     return NoContent();
        // }

        // POST: api/WebsitePostInfo
        // [HttpPost]
        // public async Task<IActionResult> PostWebsitePost([FromBody] WebsitePost websitePost)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     _context.WebsitePost.Add(websitePost);
        //     await _context.SaveChangesAsync();

        //     return CreatedAtAction("GetWebsitePost", new { id = websitePost.Id }, websitePost);
        // }

        // // DELETE: api/WebsitePostInfo/5
        // [HttpDelete("{id}")]
        // public async Task<IActionResult> DeleteWebsitePost([FromRoute] long id)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     var websitePost = await _context.WebsitePost.SingleOrDefaultAsync(m => m.Id == id);
        //     if (websitePost == null)
        //     {
        //         return NotFound();
        //     }

        //     _context.WebsitePost.Remove(websitePost);
        //     await _context.SaveChangesAsync();

        //     return Ok(websitePost);
        // }

        // private bool WebsitePostExists(long id)
        // {
        //     return _context.WebsitePost.Any(e => e.Id == id);
        // }
    }




    [Route("api/WebsitePostDetails/Post")]
    public class WebsitePostDetailsController : Controller
    {
        private readonly ClinicDBContext _context;

        public WebsitePostDetailsController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: api/WebsitePostInfo/5
        [HttpGet("{id}")]
        public IEnumerable<WebsitePost> GetWebsitePostDetails([FromRoute] long id)
        {
            return _context.WebsitePost.Where(m => m.Id == id).Include(m => m.Category);

        }
    }

}