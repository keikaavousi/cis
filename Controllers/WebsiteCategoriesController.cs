﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;

namespace ClinicWebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/WebsiteCategories")]
    public class WebsiteCategoriesController : Controller
    {
        private readonly ClinicDBContext _context;

        public WebsiteCategoriesController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: api/WebsiteCategories
        [HttpGet]
        public IEnumerable<Category> GetCategory()
        {
            return _context.Category;
        }

        // GET: api/WebsiteCategories/5
        [HttpGet("{id}")]
        public IActionResult GetCategory([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var category = await _context.Category.SingleOrDefaultAsync(m => m.Id == id);
            var category = _context.Category.Where(m => m.WebsiteId == id);

            if (category == null)
            {
                return NotFound();
            }

            return Ok(category);
        }

        // PUT: api/WebsiteCategories/5
        // [HttpPut("{id}")]
        // public async Task<IActionResult> PutCategory([FromRoute] int id, [FromBody] Category category)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     if (id != category.Id)
        //     {
        //         return BadRequest();
        //     }

        //     _context.Entry(category).State = EntityState.Modified;

        //     try
        //     {
        //         await _context.SaveChangesAsync();
        //     }
        //     catch (DbUpdateConcurrencyException)
        //     {
        //         if (!CategoryExists(id))
        //         {
        //             return NotFound();
        //         }
        //         else
        //         {
        //             throw;
        //         }
        //     }

        //     return NoContent();
        // }

        // POST: api/WebsiteCategories
        // [HttpPost]
        // public async Task<IActionResult> PostCategory([FromBody] Category category)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     _context.Category.Add(category);
        //     await _context.SaveChangesAsync();

        //     return CreatedAtAction("GetCategory", new { id = category.Id }, category);
        // }

        // DELETE: api/WebsiteCategories/5
        // [HttpDelete("{id}")]
        // public async Task<IActionResult> DeleteCategory([FromRoute] int id)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     var category = await _context.Category.SingleOrDefaultAsync(m => m.Id == id);
        //     if (category == null)
        //     {
        //         return NotFound();
        //     }

        //     _context.Category.Remove(category);
        //     await _context.SaveChangesAsync();

        //     return Ok(category);
        // }

        // private bool CategoryExists(int id)
        // {
        //     return _context.Category.Any(e => e.Id == id);
        // }
    }
}