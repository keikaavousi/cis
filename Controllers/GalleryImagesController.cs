﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Authorization;

namespace ClinicWebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/GalleryImages")]
    public class GalleryImagesController : Controller
    {
        private readonly ClinicDBContext _context;

        public GalleryImagesController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: api/GalleryImages
        //[HttpGet("{id}")]
        //public IEnumerable<WebsiteGalleryImage> GetWebsiteGalleryImage([FromRoute] long id)
        //{
        //    return _context.WebsiteGalleryImage.Where(m=>m.GalleryId==id);
        //}

        // GET: api/GalleryImages/5
        [HttpGet("{id}")]
        public IActionResult GetWebsiteGalleryImage([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var websiteGalleryImage = _context.WebsiteGalleryImage.Where(m => m.GalleryId == id);

            if (websiteGalleryImage == null)
            {
                return NotFound();
            }

            return Ok(websiteGalleryImage);
        }

        // PUT: api/GalleryImages/5
        // [HttpPut("{id}")]
        // public async Task<IActionResult> PutWebsiteGalleryImage([FromRoute] long id, [FromBody] WebsiteGalleryImage websiteGalleryImage)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     if (id != websiteGalleryImage.Id)
        //     {
        //         return BadRequest();
        //     }

        //     _context.Entry(websiteGalleryImage).State = EntityState.Modified;

        //     try
        //     {
        //         await _context.SaveChangesAsync();
        //     }
        //     catch (DbUpdateConcurrencyException)
        //     {
        //         if (!WebsiteGalleryImageExists(id))
        //         {
        //             return NotFound();
        //         }
        //         else
        //         {
        //             throw;
        //         }
        //     }

        //     return NoContent();
        // }

        // POST: api/GalleryImages
        // [HttpPost]
        // public async Task<IActionResult> PostWebsiteGalleryImage([FromBody] WebsiteGalleryImage websiteGalleryImage)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     _context.WebsiteGalleryImage.Add(websiteGalleryImage);
        //     await _context.SaveChangesAsync();

        //     return CreatedAtAction("GetWebsiteGalleryImage", new { id = websiteGalleryImage.Id }, websiteGalleryImage);
        // }

        // // DELETE: api/GalleryImages/5
        // [HttpDelete("{id}")]
        // public async Task<IActionResult> DeleteWebsiteGalleryImage([FromRoute] long id)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     var websiteGalleryImage = await _context.WebsiteGalleryImage.SingleOrDefaultAsync(m => m.Id == id);
        //     if (websiteGalleryImage == null)
        //     {
        //         return NotFound();
        //     }

        //     _context.WebsiteGalleryImage.Remove(websiteGalleryImage);
        //     await _context.SaveChangesAsync();

        //     return Ok(websiteGalleryImage);
        // }

        // private bool WebsiteGalleryImageExists(long id)
        // {
        //     return _context.WebsiteGalleryImage.Any(e => e.Id == id);
        // }
    }
}