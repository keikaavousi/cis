﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;

namespace ClinicWebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Online")]
    public class OnlineController : Controller
    {
        private readonly ClinicDBContext _context;

        public OnlineController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: api/Pages
        [HttpGet]
        public IEnumerable<ReservationFreeDate> GetOnlineDates()
        {
        //freeDate
            return _context.ReservationFreeDate;
        }



        [HttpGet("res_id")]
        public OnlineReserve GetOnlineReserves([FromQuery(Name="id")] long id)
        {
            return _context.OnlineReserve.FirstOrDefault(r=>r.Id==id);
        }

        // GET: api/Pages/5
        [HttpGet("{id}")]
        public IEnumerable<ReservationFreeDate> GetReservationFreeDate([FromRoute] long id)
        {
               var dateselectlist = _context.ReservationFreeDate.Where(a => a.Date >= DateTime.Now)
               .Where(a=>a.OnlineEnabled==true).Where(a => a.ClinicId == id).OrderBy(a => a.Date);
            return dateselectlist;
        }



        [HttpGet("TimeLapsedTime")]
        public JsonResult GetTimeLapsed([FromQuery(Name ="DateId")] DateTime DateId,[FromQuery(Name ="TimeId")] TimeSpan TimeId,[FromQuery(Name ="cid")] int cid){
             var tempreserved = _context.OnlineReserve.Where(a => a.Time == TimeId)
                .Where(a => a.Date == Convert.ToDateTime(DateId))
                .Where(a=> (DateTime.Now-a.NowDate).TotalSeconds<5*60 || a.PaymentStatus==true)
                .FirstOrDefault(a => a.ClinicId == cid);



            return Json(new{temp=tempreserved});
        }






       [HttpGet("times")]
         public JsonResult GetTimes([FromQuery(Name ="DateId")] DateTime DateId,[FromQuery(Name ="cid")] int cid)
        {
             var times = _context.ReservationFreeDate.Where(a => a.Date == Convert.ToDateTime(DateId)).FirstOrDefault(a => a.ClinicId == cid);
            var stime = times.StartTime;
            var etime = times.EndTime;
            IList<TimeSpan> timeArray = new List<TimeSpan>();
            var timepervisit = times.TimePerVisit;
            //TimeSpan min = new TimeSpan(0, 0, 15, 0, 0);
            TimeSpan min = TimeSpan.FromMinutes(timepervisit);

            timeArray.Add(stime);
            // for (int i = 0; stime.Add(min) < etime && i <= times.PatientPerDay; i++)
            for (int i = 0; stime.Add(min) < etime; i++)
            {
                stime = stime.Add(min);
                timeArray.Add(stime);
            }


            IList<TimeSpan> freeTimeArray = new List<TimeSpan>();
            foreach (var t in timeArray)
            {
                
                var onreserved = _context.OnlineReserve.Where(a => a.Time == t)
                .Where(a => a.Date == Convert.ToDateTime(DateId))
                .Where(a=>a.PaymentStatus==true)
                .FirstOrDefault(a => a.ClinicId == cid);



                

                var tempreserved = _context.OnlineReserve.Where(a => a.Time == t)
                .Where(a => a.Date == Convert.ToDateTime(DateId))
                .Where(a=> (DateTime.Now-a.NowDate).TotalSeconds<5*60)
                .FirstOrDefault(a => a.ClinicId == cid);

           /* bool tempr=false;
            TimeSpan elapsedTime = DateTime.Now - tempreserved.NowDate;
           if(elapsedTime.TotalSeconds<5*60){
            tempr=true;
           }  */  



                 var offreserved = _context.Reservation.Where(a => a.Time == t)
                .Where(a => a.Date == Convert.ToDateTime(DateId))
                .FirstOrDefault(a => a.ClinicId == cid);
                if (onreserved == null && offreserved==null && tempreserved==null)
                {
                    freeTimeArray.Add(t);
                }
            }
            
            return Json(new{freeTimeArray=freeTimeArray,times=timeArray.Count()});


        }




     
     [HttpPost("saveReserve")]
        public async Task<IActionResult> saveReserve ([FromBody] OnlineReserve onlineReserve) {

            var status = false;

            if (!ModelState.IsValid) {
                
                return Json (status);
            } 
            else {
                _context.Add(onlineReserve);
                await _context.SaveChangesAsync ();
                status = true;
               return Json(new{status=status,saved=onlineReserve.Id});
            }
        }




        [HttpPost("updateReserve")]
        public async Task<IActionResult> savePayment([FromBody] OnlineReserve r)
        {
             var reservation = _context.OnlineReserve.FirstOrDefault(m => m.Id == r.Id);
            if (reservation != null)
            {
                reservation.Payment = r.Payment;
                reservation.PaymentStatus=r.PaymentStatus;
                reservation.RefID=r.RefID;
            
            _context.Update(reservation);
            await _context.SaveChangesAsync();

            return Json("success");
            }
           return Json("failed");
        }





//  [HttpGet("{id}")]
//         public IEnumerable<ReservationFreeDate> GetReservationFreeDate([FromRoute] long id, [FromQuery(Name ="pid")] long pid)
//         {
//             if (pid!=null)
//             {
//                var ReservationFreeDate = _context.ReservationFreeDate.Where(m => m.WebsiteId == id && m.Id == pid);
//                 return ReservationFreeDate;
//             }
//             else
//             {
//                var ReservationFreeDate = _context.ReservationFreeDate.Where(m => m.WebsiteId == id).ToList();
//                 return ReservationFreeDate;
//             }

            
//         }



       
        //[HttpGet]
        //public ReservationFreeDate GetReservationFreeDateByName([FromQuery] long cid, [FromQuery] string slug)
        //{
        //    var ReservationFreeDate = _context.ReservationFreeDate.SingleOrDefault(m => m.WebsiteId == cid && m.Slug == slug);
        //    return ReservationFreeDate;
        //}


        // PUT: api/Pages/5
        // [HttpPut("{id}")]
        // public async Task<IActionResult> PutReservationFreeDate([FromRoute] long id, [FromBody] ReservationFreeDate ReservationFreeDate)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     if (id != ReservationFreeDate.Id)
        //     {
        //         return BadRequest();
        //     }

        //     _context.Entry(ReservationFreeDate).State = EntityState.Modified;

        //     try
        //     {
        //         await _context.SaveChangesAsync();
        //     }
        //     catch (DbUpdateConcurrencyException)
        //     {
        //         if (!ReservationFreeDateExists(id))
        //         {
        //             return NotFound();
        //         }
        //         else
        //         {
        //             throw;
        //         }
        //     }

        //     return NoContent();
        // }

        // POST: api/Pages
        // [HttpPost]
        // public async Task<IActionResult> PostReservationFreeDate([FromBody] ReservationFreeDate ReservationFreeDate)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     _context.ReservationFreeDate.Add(ReservationFreeDate);
        //     await _context.SaveChangesAsync();

        //     return CreatedAtAction("GetReservationFreeDate", new { id = ReservationFreeDate.Id }, ReservationFreeDate);
        // }

        // // DELETE: api/Pages/5
        // [HttpDelete("{id}")]
        // public async Task<IActionResult> DeleteReservationFreeDate([FromRoute] long id)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     var ReservationFreeDate = await _context.ReservationFreeDate.SingleOrDefaultAsync(m => m.Id == id);
        //     if (ReservationFreeDate == null)
        //     {
        //         return NotFound();
        //     }

        //     _context.ReservationFreeDate.Remove(ReservationFreeDate);
        //     await _context.SaveChangesAsync();

        //     return Ok(ReservationFreeDate);
        // }

        // private bool ReservationFreeDateExists(long id)
        // {
        //     return _context.ReservationFreeDate.Any(e => e.Id == id);
        // }
    }
}