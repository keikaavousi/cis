﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using ClinicWebApp.Models;

namespace ClinicWebApp.Controllers
{
    public class NotificationsController : Controller
    {
        private readonly ClinicDBContext _context;

        public NotificationsController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: Notifications
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Index()
        {
            var clinicDBContext = _context.Notification.Include(n => n.Clinic);
            return View(await clinicDBContext.ToListAsync());
        }

        // GET: Notifications/Details/5
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notification = await _context.Notification
                .Include(n => n.Clinic)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (notification == null)
            {
                return NotFound();
            }

            return View(notification);
        }

        // GET: Notifications/Create
        [Authorize(Roles="admin")]
        public IActionResult Create()
        {
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "FullName");
            return View();
        }

        // POST: Notifications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Create([Bind("Id,Notification1,ClinicId,Enabled")] Notification notification)
        {
            notification.Date = DateTime.Now.Date;
            if (ModelState.IsValid)
            {
                _context.Add(notification);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "FullName", notification.ClinicId);
            return View(notification);
        }

        // GET: Notifications/Edit/5
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notification = await _context.Notification.SingleOrDefaultAsync(m => m.Id == id);
            if (notification == null)
            {
                return NotFound();
            }
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "FullName", notification.ClinicId);
            return View(notification);
        }

        // POST: Notifications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Notification1,ClinicId,Date,Enabled")] Notification notification)
        {
            if (id != notification.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    _context.Update(notification);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NotificationExists(notification.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "Address", notification.ClinicId);
            return View(notification);
        }

        // GET: Notifications/Delete/5
        [Authorize(Roles="admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notification = await _context.Notification
                .Include(n => n.Clinic)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (notification == null)
            {
                return NotFound();
            }

            return View(notification);
        }

        // POST: Notifications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var notification = await _context.Notification.SingleOrDefaultAsync(m => m.Id == id);
            _context.Notification.Remove(notification);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NotificationExists(int id)
        {
            return _context.Notification.Any(e => e.Id == id);
        }
    }
}
