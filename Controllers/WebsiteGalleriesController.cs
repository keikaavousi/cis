﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

namespace ClinicWebApp.Controllers
{
    public class WebsiteGalleriesController : Controller
    {
        private readonly ClinicDBContext _context;

        public WebsiteGalleriesController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: WebsiteGalleries
        [Authorize]
        public async Task<IActionResult> Index()
        {
            /*get current website id*/
           
            
            var cid = UserInfo.ClinicId;
            var uid = UserInfo.UserId;
            var wid = _context.Website.FirstOrDefault(a => a.ClinicId == cid).Id;
            var c=_context.Clinic.FirstOrDefault(a=>a.Id==cid);

            HttpContext.Session.SetInt32("wid", wid);
            HttpContext.Session.SetString("cid", cid.ToString());
            HttpContext.Session.SetString("weburl", c.Url);
            
            /**/

            var clinicDBContext = _context.WebsiteGallery.Include(w => w.Website).Where(w=>w.WebsiteId== _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id);
            return View(await clinicDBContext.ToListAsync());
        }

        // GET: WebsiteGalleries/Details/5
        [Authorize]
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var websiteGallery = await _context.WebsiteGallery
                .Include(w => w.Website)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (websiteGallery == null)
            {
                return NotFound();
            }

            return View(websiteGallery);
        }

        // GET: WebsiteGalleries/Create
        [Authorize]
        public IActionResult Create()
        {
               ViewData["cid"] = UserInfo.ClinicId;
            ViewData["uid"] = UserInfo.UserId;
            ViewData["WebsiteId"]= _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            return View();
        }

        // POST: WebsiteGalleries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("Id,WebsiteId,Title,Date,Show")] WebsiteGallery websiteGallery)
        {
              var cid = UserInfo.ClinicId;
            var uid = UserInfo.UserId;
            var wid = _context.Website.FirstOrDefault(a => a.ClinicId == cid).Id;
            var c=_context.Clinic.FirstOrDefault(a=>a.Id==cid);

            HttpContext.Session.SetInt32("wid", wid);
            HttpContext.Session.SetString("cid", cid.ToString());
            HttpContext.Session.SetString("weburl", c.Url);

            if (ModelState.IsValid)
            {
                
                //websiteGallery.WebsiteId=Convert.ToInt32(HttpContext.Session.GetInt32("wid"));
                _context.Add(websiteGallery);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
             ViewData["WebsiteId"] = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;;
            return View(websiteGallery);
        }

        // GET: WebsiteGalleries/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(long? id)
        {
          var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            if (id == null)
            {
                return NotFound();
            }

            var websiteGallery = await _context.WebsiteGallery
            .Where(w => w.Website.Id == wid)
            .SingleOrDefaultAsync(m => m.Id == id);
            if (websiteGallery == null)
            {
                return NotFound();
            }


           
            return View(websiteGallery);
        }

        // POST: WebsiteGalleries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(long id, [Bind("Id,WebsiteId,Title,Date,Show")] WebsiteGallery websiteGallery)
        {
            ViewData["cid"] = UserInfo.ClinicId;
            ViewData["uid"] = UserInfo.UserId;
            if (id != websiteGallery.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    
                    _context.Update(websiteGallery);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WebsiteGalleryExists(websiteGallery.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(websiteGallery);
        }

        // GET: WebsiteGalleries/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(long? id)
        {
                     var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            if (id == null)
            {
                return NotFound();
            }

            var websiteGallery = await _context.WebsiteGallery
                .Include(w => w.Website)
                .Where(w => w.Website.Id == wid)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (websiteGallery == null)
            {
                return NotFound();
            }

            return View(websiteGallery);
        }

        // POST: WebsiteGalleries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
                                 var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            var websiteGallery = await _context.WebsiteGallery
            .Where(w => w.Website.Id == wid)
            .SingleOrDefaultAsync(m => m.Id == id);
            _context.WebsiteGallery.Remove(websiteGallery);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WebsiteGalleryExists(long id)
        {
            return _context.WebsiteGallery.Any(e => e.Id == id);
        }
    }
}
