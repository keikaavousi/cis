﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace ClinicWebApp.Controllers
{
    public class SearchPatientsController : Controller
    {
        private readonly ClinicDBContext _context;

        public SearchPatientsController(ClinicDBContext context)
        {
            _context = context;
        }

[Authorize]
        public async Task<IActionResult> Index(string sortOrder,
   string currentFilter,
   string searchString,
   long? id,
   long? det,
   int? page)
        {
            
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
          
            var onlinePatient = _context.OnlineReserve.SingleOrDefault(p => p.Id == id);
            ViewData["id"] = onlinePatient.Id;
            ViewData["date"] = onlinePatient.Date;
            ViewData["time"] = onlinePatient.Time;
            ViewData["det"]=det;
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            searchString = onlinePatient.FirstName + " "+ onlinePatient.LastName;

            ViewData["CurrentFilter"] = searchString;
            var patients = from s in _context.Patient.Where(p => p.ClinicId == UserInfo.ClinicId)
                           select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                var searchStr=searchString.ToLower().Split(' ');
                patients = patients.Where(s => s.LastName.Contains(searchStr[0])
                                       || s.FirstName.Contains(searchStr[0]));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    patients = patients.Where(p => p.ClinicId == UserInfo.ClinicId).OrderByDescending(s => s.LastName);
                    break;
                case "doc_number":
                    patients = patients.Where(p => p.ClinicId == UserInfo.ClinicId).OrderByDescending(s => s.DocNumber);
                    break;
                default:
                    patients = patients.Where(p => p.ClinicId == UserInfo.ClinicId).OrderByDescending(s => s.LastName);
                    break;
            }

            int pageSize = 10;
            return View(await PaginatedList<Patient>.CreateAsync(patients.AsNoTracking(), page ?? 1, pageSize));
        }

    }
}