﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;


namespace ClinicWebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/ContactForms")]
    public class ContactFormsController : Controller
    {
        private readonly ClinicDBContext _context;

        public ContactFormsController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: api/ContactForms
        [HttpGet]
        public IEnumerable<WebsiteContactForm> GetWebsiteContactForm()
        {
            return _context.WebsiteContactForm;
        }

        // GET: api/ContactForms/5
        [HttpGet("{id}")]
        public IActionResult GetWebsiteContactForm([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var websiteContactForm = _context.WebsiteContactForm.Where(m => m.WebsiteId== id).OrderByDescending(m=>m.Id).ToList();

            if (websiteContactForm == null)
            {
                return NotFound();
            }

            return Ok(websiteContactForm);
        }

        // PUT: api/ContactForms/5
        // [HttpPut("{id}")]
        // public async Task<IActionResult> PutWebsiteContactForm([FromRoute] long id, [FromBody] WebsiteContactForm websiteContactForm)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     if (id != websiteContactForm.Id)
        //     {
        //         return BadRequest();
        //     }

        //     _context.Entry(websiteContactForm).State = EntityState.Modified;

        //     try
        //     {
        //         await _context.SaveChangesAsync();
        //     }
        //     catch (DbUpdateConcurrencyException)
        //     {
        //         if (!WebsiteContactFormExists(id))
        //         {
        //             return NotFound();
        //         }
        //         else
        //         {
        //             throw;
        //         }
        //     }

        //     return NoContent();
        // }

        // POST: api/ContactForms
        [HttpPost]
        public async Task<IActionResult> PostWebsiteContactForm([FromBody] WebsiteContactForm websiteContactForm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.WebsiteContactForm.Add(websiteContactForm);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetWebsiteContactForm", new { id = websiteContactForm.Id }, websiteContactForm);
        }

        // DELETE: api/ContactForms/5
        //[HttpDelete("{id}")]
        // public async Task<IActionResult> DeleteWebsiteContactForm([FromRoute] long id)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     var websiteContactForm = await _context.WebsiteContactForm.SingleOrDefaultAsync(m => m.Id == id);
        //     if (websiteContactForm == null)
        //     {
        //         return NotFound();
        //     }

        //     _context.WebsiteContactForm.Remove(websiteContactForm);
        //     await _context.SaveChangesAsync();

        //     return Ok(websiteContactForm);
        // }

        // private bool WebsiteContactFormExists(long id)
        // {
        //     return _context.WebsiteContactForm.Any(e => e.Id == id);
        // }
    }
}