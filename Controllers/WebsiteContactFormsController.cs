﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Authorization;


namespace ClinicWebApp.Controllers
{
    public class WebsiteContactFormsController : Controller
    {
        private readonly ClinicDBContext _context;

        public WebsiteContactFormsController(ClinicDBContext context)
        {
            _context = context;
        }

        // GET: WebsiteContactForms
        [Authorize]
        public async Task<IActionResult> Index()
        {
            var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;


            var clinicDBContext = _context.WebsiteContactForm.Include(w => w.Website)
            .Where(w => w.Website.Id == wid).OrderByDescending(w=>w.Id);
            return View(await clinicDBContext.ToListAsync());
        }

        // GET: WebsiteContactForms/Details/5
        [Authorize]
        public async Task<IActionResult> Details(long? id)
        {
             var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;
            if (id == null)
            {
                return NotFound();
            }

            var websiteContactForm = await _context.WebsiteContactForm
                .Include(w => w.Website)
                .Where(w => w.Website.Id == wid)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (websiteContactForm == null)
            {
                return NotFound();
            }

            return View(websiteContactForm);
        }

        // GET: WebsiteContactForms/Create
        [Authorize]
        public IActionResult Create()
        {
            ViewData["WebsiteId"] = new SelectList(_context.Website, "Id", "Id");
            return View();
        }

        // POST: WebsiteContactForms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("Id,WebsiteId,Name,Email,Tel,Date,Message,FileUrl,Verify")] WebsiteContactForm websiteContactForm)
        {
            if (ModelState.IsValid)
            {
                _context.Add(websiteContactForm);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["WebsiteId"] = new SelectList(_context.Website, "Id", "Id", websiteContactForm.WebsiteId);
            return View(websiteContactForm);
        }

        // GET: WebsiteContactForms/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(long? id)
        {
            var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            if (id == null)
            {
                return NotFound();
            }

            var websiteContactForm = await _context.WebsiteContactForm
            .Where(w => w.Website.Id == wid)
            .SingleOrDefaultAsync(m => m.Id == id);
            if (websiteContactForm == null)
            {
                return NotFound();
            }

            ViewData["WebsiteId"] = new SelectList(_context.Website, "Id", "Id", websiteContactForm.WebsiteId);
            return View(websiteContactForm);
        }

        // POST: WebsiteContactForms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(long id, [Bind("Id,WebsiteId,Name,Email,Tel,Date,Message,FileUrl,Verify")] WebsiteContactForm websiteContactForm)
        {
            if (id != websiteContactForm.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    _context.Update(websiteContactForm);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WebsiteContactFormExists(websiteContactForm.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["WebsiteId"] = new SelectList(_context.Website, "Id", "Id", websiteContactForm.WebsiteId);
            return View(websiteContactForm);
        }

        // GET: WebsiteContactForms/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(long? id)
        {
                        var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            if (id == null)
            {
                return NotFound();
            }

            var websiteContactForm = await _context.WebsiteContactForm
                .Include(w => w.Website)
                .Where(w => w.Website.Id == wid)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (websiteContactForm == null)
            {
                return NotFound();
            }

            return View(websiteContactForm);
        }

        // POST: WebsiteContactForms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
         var wid = _context.Website.FirstOrDefault(a => a.ClinicId == UserInfo.ClinicId).Id;

            var websiteContactForm = await _context.WebsiteContactForm
            .Where(w => w.Website.Id == wid)
            .SingleOrDefaultAsync(m => m.Id == id);
            _context.WebsiteContactForm.Remove(websiteContactForm);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WebsiteContactFormExists(long id)
        {
            return _context.WebsiteContactForm.Any(e => e.Id == id);
        }
    }
}
