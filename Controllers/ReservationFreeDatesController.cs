﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ClinicWebApp.Data;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Net;
using System.Threading;

namespace ClinicWebApp.Controllers {
    public class ReservationFreeDatesController : Controller {
        private readonly ClinicDBContext _context;
    

        public ReservationFreeDatesController (ClinicDBContext context) {
            _context = context;
        }

        

        // GET: ReservationFreeDates
        [Authorize]
        public async Task<IActionResult> Index () {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            ViewData["cid"] = UserInfo.ClinicId;
            ViewData["uid"] = UserInfo.UserId;

            
            var clinicDBContext = _context.ReservationFreeDate.Include (r => r.Clinic).Where (a => a.ClinicId == UserInfo.ClinicId).OrderByDescending (a => a.Date);
            
            
            
            
            return View (await clinicDBContext.ToListAsync ());
        }

        // GET: ReservationFreeDates/Details/5
        [Authorize]
        public async Task<IActionResult> Details (long? id,DateTime? dt) {
            
            
            if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var clid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = clid.ClinicId;
            }

            if (dt != null)
            {
                ViewData["date"] = Utility.ToMiladi(Convert.ToDateTime(dt));
            }
            else
            {
                ViewData["date"] = DateTime.Now;
            }

            
           if (id == null) {
            var dnow = _context.ReservationFreeDate
            .Where(r=>r.ClinicId== UserInfo.ClinicId)
            .FirstOrDefault(r => r.Date.ToShortDateString() == DateTime.Now.ToShortDateString());

            if(dnow==null){
                 return RedirectToAction("Create","ReservationFreeDates");
            }
            else{
                 return RedirectToAction("Details","ReservationFreeDates",new{Id=dnow.Id});
            }
            }


             long cid=UserInfo.ClinicId;
            //Details of date
            var reservationFreeDate = await _context.ReservationFreeDate
                .Include (r => r.Clinic)
                .Where (r => r.ClinicId == cid)
                .SingleOrDefaultAsync (m => m.Id == id);
            if (reservationFreeDate == null) {
                return NotFound ();
            }


            // reserves
            var reserves = _context.Reservation.Include (r => r.Clinic)
            .Include (r => r.Patient)
            .Where (a => a.ClinicId == cid)
            .Where(a=>a.Date==reservationFreeDate.Date)
            .OrderBy(a => a.Time);

           // ViewData["Count"]=reserves.Count();

                ViewData["Count"]=reserves.Count();

            ViewData["Reserves"]=reserves;





            //online reserves
            var notconfirmed= from p in _context.OnlineReserve
            .Where(a => a.ClinicId == UserInfo.ClinicId && a.Date==reservationFreeDate.Date)
            .Where(a=>a.PaymentStatus==true)
            .Where(a=>a.verify==false)
             select p;
            //where !_context.Reservation.Any(es => (es.Date == p.Date) && (es.Time == p.Time))
           

            ViewData["OnlineReserves"]=notconfirmed;


            ViewData["resid"]=id;


          

            return View (reservationFreeDate);
        }



        // GET: ReservationFreeDates/Create
        [Authorize]
        public IActionResult Create () {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }


            ViewData["cid"] = UserInfo.ClinicId;
            ViewData["uid"] = UserInfo.UserId;



            //ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "Address");
            return View ();
        }

        // POST: ReservationFreeDates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create ([Bind ("Id,Date,StartTime,EndTime,PatientPerDay,ClinicId,TimePerVisit,OnlineEnabled")] ReservationFreeDate reservationFreeDate) {
            if (ModelState.IsValid) {
                _context.Add (reservationFreeDate);
                await _context.SaveChangesAsync ();
                return RedirectToAction (nameof (Index));
            }
            ViewData["ClinicId"] = new SelectList (_context.Clinic, "Id", "Address", reservationFreeDate.ClinicId);
            return View (reservationFreeDate);
        }

        // GET: ReservationFreeDates/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit (long? id) {

            if(UserInfo.ClinicId==0){
                var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
                UserInfo.UserId = uid;
                UserInfo.ClinicId = cid.ClinicId;
            }

            if (id == null) {
                return NotFound ();
            }                                   
                        
            var reservationFreeDate = await _context.ReservationFreeDate.SingleOrDefaultAsync (m => m.Id == id);
            if (reservationFreeDate == null) {
                return NotFound ();
            }

            var res=await _context.Reservation.Where(r=>r.ClinicId==UserInfo.ClinicId).FirstOrDefaultAsync(r=>r.Date==reservationFreeDate.Date);
            if(res!=null){
                ViewData["editstatus"]="0";
            }
             
            

            return View (reservationFreeDate);
        }

        // POST: ReservationFreeDates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit (long id, [Bind ("Id,Date,StartTime,EndTime,PatientPerDay,ClinicId,TimePerVisit,OnlineEnabled")] ReservationFreeDate reservationFreeDate) {
            if (id != reservationFreeDate.Id) {
                return NotFound ();
            }
            

            if (ModelState.IsValid) {
                try {
                       
                        _context.Update (reservationFreeDate);
                         await _context.SaveChangesAsync ();
                 
                } catch (DbUpdateConcurrencyException) {
                    if (!ReservationFreeDateExists (reservationFreeDate.Id)) {
                        return NotFound ();
                    } else {
                        throw;
                    }
                }
                return RedirectToAction ("Details","ReservationFreeDates",new{Id=id});
            }
            //ViewData["ClinicId"] = new SelectList(_context.Clinic, "Id", "Address", reservationFreeDate.ClinicId);
            return View (reservationFreeDate);
        }

        // GET: ReservationFreeDates/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete (long? id) {
             if(UserInfo.ClinicId==0){
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
            UserInfo.UserId = uid;
            UserInfo.ClinicId = cid.ClinicId;
            }

            
            if (id == null) {
                return NotFound ();
            }

            var reservationFreeDate = await _context.ReservationFreeDate
                .Include (r => r.Clinic)
                .Where (r => r.ClinicId == UserInfo.ClinicId)
                .SingleOrDefaultAsync (m => m.Id == id);
            if (reservationFreeDate == null) {
                return NotFound ();
            }



            return View (reservationFreeDate);
        }

        // POST: ReservationFreeDates/Delete/5
        [HttpPost, ActionName ("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed (long id) {
            var reservationFreeDate = await _context.ReservationFreeDate
                .Where (r => r.ClinicId == UserInfo.ClinicId)
                .SingleOrDefaultAsync (m => m.Id == id);
            _context.ReservationFreeDate.Remove (reservationFreeDate);
            await _context.SaveChangesAsync ();
            return RedirectToAction ("Create","ReservationFreeDates");
        }

        private bool ReservationFreeDateExists (long id) {
            return _context.ReservationFreeDate.Any (e => e.Id == id);
        }

        /***********************************full calendar *******************************/
        [HttpPost]
        public async Task<IActionResult> SaveEvent ([FromBody] ReservationFreeDate reservationFreeDate) {

            var status = false;

            if (!ModelState.IsValid) {
                return Json (status);
            } else {
                _context.Add (reservationFreeDate);
                await _context.SaveChangesAsync ();
                status = true;
                                 return CreatedAtAction("ReservationFreeDate", new { id = reservationFreeDate.Id }, reservationFreeDate);

            }

            // if (reservationFreeDate.Id > 0)
            // {
            //     //Update the event
            //     var v = _context.ReservationFreeDate.Where(a => a.Id == reservationFreeDate.Id).FirstOrDefault();
            //     if (v != null)
            //     {
            //         v.ClinicId = reservationFreeDate.ClinicId;
            //         v.TimePerVisit = reservationFreeDate.TimePerVisit;
            //         v.PatientPerDay = reservationFreeDate.PatientPerDay;
            //         v.Date = reservationFreeDate.Date;
            //         v.StartTime = reservationFreeDate.StartTime;
            //         v.EndTime = reservationFreeDate.EndTime;
            //     }
            // }
            // else
            // {

            //}

            return Json (status);
        }

        public JsonResult GetEvents () {
            var events = _context.ReservationFreeDate
            .Where (a => a.ClinicId == UserInfo.ClinicId)
            .Select(a=> new{
               Id=a.Id,
               Date= a.Date,
               Title= _context.Reservation.Where(r=>r.Date==a.Date).Where(r => r.ClinicId == UserInfo.ClinicId).Count()
                +_context.OnlineReserve.Where(r=>r.Date==a.Date).Where(r => r.ClinicId == UserInfo.ClinicId).Where(r=>r.PaymentStatus==true).Where(r=>r.verify==false).Count(),
               StartTime=a.StartTime,
               EndTime=a.EndTime,
               OnlineEnabled=a.OnlineEnabled,
               //Onlines=_context.Reservation.Where(r=>r.Date==a.Date).Where(r => r.ClinicId == UserInfo.ClinicId).Where(r=>r.PaymentType==3).Count()
                Onlines=_context.OnlineReserve.Where(r=>r.Date==a.Date).Where(r => r.ClinicId == UserInfo.ClinicId).Where(r=>r.PaymentStatus==true).Count()
            })
            .OrderBy (a => a.Date)
            .ToList ();



            return Json (new{data=events});

        }



[HttpPost]
        public async Task<IActionResult> verifyOnline([FromBody] OnlineReserve OnlineReserve)
        {
            //Update visit
            var res = _context.OnlineReserve.FirstOrDefault(m => m.Id == OnlineReserve.Id);
            if (res != null)
            {
                res.verify = true;
            
            _context.Update(res);
            await _context.SaveChangesAsync();

            return Json(new { status = "success", message = res,id=OnlineReserve.Id});
            }
           return Json(new { status = "failed",message=res,id=OnlineReserve.Id });
   
        }

        [HttpPost]
        public async Task<IActionResult> saveReserve ([FromBody] Reservation reservation) {

            var status = false;
           
            if (!ModelState.IsValid) 
            {
                return Json (status);
            } 
            else {
                _context.Add (reservation);
                await _context.SaveChangesAsync ();            
            status = true;
            return Json(new{st=status,res=reservation.Id});
            }
        }
    }
}