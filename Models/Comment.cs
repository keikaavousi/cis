﻿using System;
using System.Collections.Generic;

namespace ClinicWebApp.Models
{
    public partial class Comment
    {
        public long Id { get; set; }
        public long PostId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string CommentText { get; set; }
        public bool Verified { get; set; }
        public string Reply { get; set; }

        public WebsitePost Post { get; set; }
    }
}
