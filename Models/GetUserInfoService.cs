using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;


namespace ClinicWebApp
{
 
 public class GetUserInfoService :Controller
{
    private readonly ClinicDBContext _context;
    private readonly UserManager<ApplicationUser> _userManager;
    
    public GetUserInfoService(ClinicDBContext context, UserManager<ApplicationUser> userManager){
       _context=context;
        _userManager = userManager;
    }

    public void GetUserClinic()
    {            
        var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
        var cid = _context.UserClinic.Include(a => a.Clinic).FirstOrDefault(a => a.UserId == uid);
        UserInfo.UserId = uid;
        UserInfo.ClinicId = cid.ClinicId;
    }
}
}