﻿using System;
using System.Collections.Generic;

namespace ClinicWebApp.Models
{
    public partial class Payment
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Amount { get; set; }
        public DateTime? Date { get; set; }
        public byte? PaymentType { get; set; }

        public AspNetUsers User { get; set; }
    }
}
