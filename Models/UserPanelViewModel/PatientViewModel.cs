﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ClinicWebApp.Models.UserPanelViewModel
{
    public class PatientViewModel
    {
        [Key]
        [Display(Name = "شناسه")]
        public string Id { get; set; }

        [Required(ErrorMessage = "این فیلد ضروری است")]
        [DataType(DataType.Text)]
        [Display(Name = "شماره پرونده")]
        public string DocNumber { get; set; }

        [Required(ErrorMessage = "این فیلد ضروری است")]
        [DataType(DataType.Text)]
        [Display(Name = "نام")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "این فیلد ضروری است")]
        [DataType(DataType.Text)]
        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "شماره تماس")]
        public string Tel { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "موبایل")]
        public string Mobile { get; set; }

        [EmailAddress]
        [Display(Name = "ایمیل")]
        public string Email { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "آدرس")]
        public string Address { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "بیماری اصلی")]
        public string MainDisease { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "نام پدر")]
        public string FatherName { get; set; }

        
    }
}
