﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace ClinicWebApp.Models
{
    public partial class Website
    {
        public Website()
        {
            Category = new HashSet<Category>();
            Notification = new HashSet<Notification>();
            WebsiteContactForm = new HashSet<WebsiteContactForm>();
            WebsiteDiscussion = new HashSet<WebsiteDiscussion>();
            WebsiteGallery = new HashSet<WebsiteGallery>();
            WebsitePage = new HashSet<WebsitePage>();
            WebsitePost = new HashSet<WebsitePost>();
            WebsiteSlider = new HashSet<WebsiteSlider>();
        }

        public int Id { get; set; }

        [Display(Name ="کلینیک")]
        public int ClinicId { get; set; }

        [Display(Name = "فایل لوگو")]
        public string LogoFileUrl { get; set; }

        [Display(Name = "عنوان سایت")]
        public string Title { get; set; }

        [Display(Name = "زیر عنوان سایت")]
        public string SubTitle { get; set; }

        [Display(Name = "متن درباره پزشک")]
        public string AboutUs { get; set; }

        [Display(Name = "تصویر درباره پزشک")]
        public string AboutUsPicture { get; set; }

        [Display(Name = "آدرس")]
        public string Address { get; set; }

        //[Display(Name = "اطلاعات آدرس-موقعیت افقی (lat)")]
        //public decimal? Lat { get; set; }

        //[Display(Name = "اطلاعات آدرس-موقعیت عمودی (long)")]
        //public decimal? Long { get; set; }

        [Display(Name = "کد نقشه")]
        public string Map { get; set; }

        [Display(Name = "تلفن 1")]
        public string Tel1 { get; set; }

        [Display(Name = "تلفن 2")]
        public string Tel2 { get; set; }

        [Display(Name = "تلفن 3")]
        public string Tel3 { get; set; }

        [Display(Name = "تلفن 4")]
        public string Tel4 { get; set; }

        [Display(Name = "تلفن 5")]
        public string Tel5 { get; set; }

        [Display(Name = "ساعات حضور در مطب")]
        public string VisitTime { get; set; }

        [Display(Name = "پوسته سایت")]
        public int? Theme { get; set; }

        [Display(Name = "رنگ اصلی پوسته")]
        public string ColorTheme { get; set; }

        [Display(Name = "کلینیک")]
        public Clinic Clinic { get; set; }
        public ICollection<Category> Category { get; set; }
        public ICollection<Notification> Notification { get; set; }
        public ICollection<WebsiteContactForm> WebsiteContactForm { get; set; }
        public ICollection<WebsiteDiscussion> WebsiteDiscussion { get; set; }
        public ICollection<WebsiteGallery> WebsiteGallery { get; set; }
        public ICollection<WebsitePage> WebsitePage { get; set; }
        public ICollection<WebsitePost> WebsitePost { get; set; }
        public ICollection<WebsiteSlider> WebsiteSlider { get; set; }
    }
}
