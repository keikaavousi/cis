﻿using System;
using System.Collections.Generic;

namespace ClinicWebApp.Models
{
    public partial class WebsiteDiscussion
    {
        public long Id { get; set; }
        public int WebsiteId { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }
        public string Email { get; set; }
        public DateTime Date { get; set; }
        public bool Verified { get; set; }
        public string Reply { get; set; }

        public Website Website { get; set; }
    }
}
