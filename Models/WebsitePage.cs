﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace ClinicWebApp.Models
{
    public partial class WebsitePage
    {
        public long Id { get; set; }
        public int WebsiteId { get; set; }

        [Display(Name ="عنوان صفحه")]
         [Required(ErrorMessage="این فیلد ضروری است")]
        public string Title { get; set; }

        [Remote(action: "CheckExistingSlug", controller: "WebsitePages", AdditionalFields = "Id,WebsiteId")]
        [Display(Name ="نامک")]
        [Required(ErrorMessage="این فیلد ضروری است")]
        public string Slug { get; set; }

        [Display(Name ="متن")]
        public string PageContent { get; set; }

        [Display(Name ="تصویر صفحه")]
        public string PagePicture { get; set; }

        [Display(Name ="سردسته صفحه")]
        public long? ParentPage { get; set; }

        [Display(Name ="نمایش در منو")]
        public bool ShowInMenu { get; set; }

        [Display(Name = "نمایش در صفحه اصلی")]
        public bool ShowInFrontPage { get; set; }

         [Display(Name = "تم")]
        public int Theme { get; set; }

        [Display(Name = "وب سایت")]
        public Website Website { get; set; }
    }
}
