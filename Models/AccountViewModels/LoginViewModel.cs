﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ClinicWebApp.Models.AccountViewModels
{
    public class LoginViewModel
    {
        // [Required]
        // [EmailAddress(ErrorMessage = "آدرس ایمیل معتبر نیست")]
        // public string Email { get; set; }

        [Required(ErrorMessage="نام کاربری ضروری است")]
        public string Username { get; set; }


        [Required(ErrorMessage="رمز عبور ضروری است")]
        [DataType(DataType.Password)]
        public string Password { get; set; }


        [Display(Name = "مرا به خاطرت نگهدار")]
        public bool RememberMe { get; set; }
    }
}
