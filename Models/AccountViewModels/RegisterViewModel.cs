﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ClinicWebApp.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage ="این فیلد ضروری است")]
        [EmailAddress]
        [Display(Name = "ایمیل")]
        public string Email { get; set; }



        [Required(ErrorMessage ="این فیلد ضروری است")]
        [Display(Name = "نام کاربری")]
        public string Username { get; set; }


        [Required(ErrorMessage = "این فیلد ضروری است")]
        [StringLength(100, ErrorMessage = "رمز عبور باید حداقل 4 کاراکتر باشد", MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Display(Name = "رمز عبور")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "تکرار رمز عبور")]
        [Compare("Password", ErrorMessage = "رمز عبور و تکرار آن با هم مطابقت ندارند")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "یک نقش انتخاب کنید")]
        public string RoleSelect { get; set; }

        [Display(Name = "شناسه مطب")]
        public int Cid { get; set; }
    }
}
