﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ClinicWebApp.Models.AdminPanelViewModel
{
    public class UsersViewModel
    {
        [Display(Name ="شناسه کاربر")]
        public string Id { get; set; }

        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }

        [Display(Name = "تعداد دفعات ورود ناموفق")]
        public int AccessFailedCount { get; set; }

        [Display(Name = "ایمیل")]
        public string Email { get; set; }

        [Display(Name = "تائید ایمیل")]
        public bool EmailConfirmed { get; set; }

        [Display(Name = "آیا اکانت قفل شده است؟")]
        public bool LockoutEnabled { get; set; }

        [Display(Name = "تاریخ قفل شدن اکانت")]
        public DateTimeOffset? LockoutEnd { get; set; }

        [Display(Name = "رمز عبور کدگذاری شده")]
        public string PasswordHash { get; set; }

    }
}
