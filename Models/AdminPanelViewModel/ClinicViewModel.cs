﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ClinicWebApp.Models.AdminPanelViewModel
{
    public class ClinicViewModel
    {
        [Display(Name = "شناسه")]
        public int Id { get; set; }

        [Required(ErrorMessage = "این فیلد ضروری است")]
        [DataType(DataType.Text)]
        [Display(Name = "نام پزشک")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "این فیلد ضروری است")]
        [DataType(DataType.Text)]
        [Display(Name = "نام خانوادگی پزشک")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "این فیلد ضروری است")]
        [DataType(DataType.MultilineText)]
        [MaxLength(200, ErrorMessage = ".تعداد کاراکتر بیش از حد مجاز است")]
        [Display(Name = "آدرس")]
        public string Address { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "تلفن")]
        public string Tel { get; set; }

        [Required(ErrorMessage = "این فیلد ضروری است")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "موبایل")]
        public string Mobile { get; set; }

        [EmailAddress(ErrorMessage = "آدرس ایمیل معتبر نیست")]
        [Display(Name = "ایمیل")]
        public string Email { get; set; }

        [Required(ErrorMessage = "این فیلد ضروری است")]
        [Url(ErrorMessage = "آدرس وب سایت معتبر نیست. ابتدای آدرس http وارد نمایید")]
        [Display(Name = "آدرس وب سایت")]
        public string Url { get; set; }

        [Display(Name = "سرویس طراحی وب سایت")]
        public bool Website { get; set; }

        [Display(Name = "سرویس مدیریت مطب")]
        public bool ClinicPanel { get; set; }

        [Display(Name = "سرویس تعیین وقت")]
        public bool ReservationPanel { get; set; }

        [Required(ErrorMessage = "این فیلد ضروری است")]
        [Display(Name = "تاریخ شروع قرارداد")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "این فیلد ضروری است")]
        [DataType(DataType.Currency)]
        [Display(Name = "مبلغ قرارداد")]
        public string Price { get; set; }

        [Required(ErrorMessage = "این فیلد ضروری است")]
        [Display(Name = "درصد پشتیبانی سالیانه")]
        public byte? SupportPercent { get; set; }

        [MaxLength(200, ErrorMessage = ".تعداد کاراکتر بیش از حد مجاز است")]
        [Display(Name = "توضیحات")]
        public string Description { get; set; }

        [Required(ErrorMessage = "این فیلد ضروری است")]
        [Display(Name = "سرویس فعال")]
        public bool Enabled { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "تخصص")]
        public int SpecialityId { get; set; }

        [Display(Name = "فایل لوگوی مطب")]
        //public IFormFile LogoFile { get; set; }
        public string LogoFile { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "تخصص")]
        public Speciality Speciality { get; set; }


    }
}
