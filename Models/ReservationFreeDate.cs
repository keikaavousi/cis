﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace ClinicWebApp.Models
{
    public partial class ReservationFreeDate
    {
        public long Id { get; set; }
        
        [Display(Name ="تاریخ")]
        public DateTime Date { get; set; }

        [DisplayFormat(DataFormatString = "{0:hh\\:mm}", ApplyFormatInEditMode = false)]
        [Display(Name = "شروع ویزیت")]
        [Required(ErrorMessage="این فیلد داده ای ضروری است")]
        public TimeSpan StartTime { get; set; }

        [DisplayFormat(DataFormatString = "{0:hh\\:mm}", ApplyFormatInEditMode = false)]
        [Required(ErrorMessage="این فیلد داده ای ضروری است")]
        [Display(Name = "پایان ویزیت")]
        public TimeSpan EndTime { get; set; }

        [Display(Name = "نوبت های قابل رزرو")]
        [Required(ErrorMessage = "این فیلد ضروری است")]
        public int PatientPerDay { get; set; }

        [Required(ErrorMessage = "این فیلد داده ای  ضروری است")]
        [Display(Name = "زمان هر ویزیت")]
        public int TimePerVisit { get; set; }

        [Display(Name = "رزرو اینترنتی دارد؟")]
        public bool OnlineEnabled { get; set; }

        [Display(Name = "کلینیک")]
        public int ClinicId { get; set; }

        [Display(Name = "کلینیک")]
        public Clinic Clinic { get; set; }
    }
}
