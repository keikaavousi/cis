﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace ClinicWebApp.Models
{
    public partial class Phonebook
    {

        public int Id { get; set; }
        
        [Display(Name="نام")]
        public string FName { get; set; }

        [Display(Name="نام خانوادگی")]
        public string LName { get; set; }

        [Display(Name="تلفن")]
        public string Tel { get; set; }

        [Display(Name="توضیحات")]
        public string Description { get; set; }

        public int ClinicId{get;set;}

        public Clinic Clinic { get; set; }
    }
}
