﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace ClinicWebApp.Models
{
    public partial class Note
    {

        public int Id { get; set; }
        
        [Display(Name="عنوان")]
        public string Title { get; set; }

        [Display(Name="متن")]
        public string NoteText { get; set; }

        [Display(Name="تاریخ")]
        public DateTime Date { get; set; }

        public int ClinicId{get;set;}

        public Clinic Clinic { get; set; }
    }
}
