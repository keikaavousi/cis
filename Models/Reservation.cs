﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClinicWebApp.Models
{
    //public abstract class ViewModelBase
    //{
    //    public long Id { get; set; }

    //    [Display(Name = "شناسه بیمار")]
    //    public long? PatientId { get; set; }

    //    [Display(Name = "شناسه مطب")]
    //    public int ClinicId { get; set; }

    //    [Display(Name = "هزینه پرداختی")]
    //    public string Payment { get; set; }

    //    [Display(Name = "نوع پرداخت")]
    //    public byte? PaymentType { get; set; }

    //    [Display(Name = "ویزیت")]
    //    public bool Visited { get; set; }

    //    [Display(Name = "تاریخ انتخابی")]
    //    public DateTime Date { get; set; }

    //    [Display(Name = "زمان انتخابی")]
    //    public TimeSpan Time { get; set; }

    //    [Display(Name = "کلینیک")]
    //    public Clinic Clinic { get; set; }

    //    [Display(Name = "بیمار")]
    //    public Patient Patient { get; set; }
    //}
    //public class ReservationViewModel : ViewModelBase
    //{

    //}

    public partial class Reservation
    {
        public Reservation()
        {
            PatientRecord = new HashSet<PatientRecord>();
            ExtraPayment=new HashSet<ExtraPayment>();
        }


        //public class OuterViewModel
        //{
        //    public InnerViewModel InnerViewModel { get; set; }
        //}

        //public class InnerViewModel
        //{
        //    public long Id { get; set; }

        //    [Display(Name = "شناسه بیمار")]
        //    public long? PatientId { get; set; }

        //    [Display(Name = "شناسه مطب")]
        //    public int ClinicId { get; set; }

        //    [Display(Name = "هزینه پرداختی")]
        //    public string Payment { get; set; }

        //    [Display(Name = "نوع پرداخت")]
        //    public byte? PaymentType { get; set; }

        //    [Display(Name = "ویزیت")]
        //    public bool Visited { get; set; }

        //    [Display(Name = "تاریخ انتخابی")]
        //    public DateTime Date { get; set; }

        //    [Display(Name = "زمان انتخابی")]
        //    public TimeSpan Time { get; set; }

        //    [Display(Name = "کلینیک")]
        //    public Clinic Clinic { get; set; }

        //    [Display(Name = "بیمار")]
        //    public Patient Patient { get; set; }
        //}

        public long Id { get; set; }

        [Display(Name = "شناسه بیمار")]
        public long? PatientId { get; set; }

        [Display(Name = "شناسه مطب")]
        public int ClinicId { get; set; }

        [Display(Name = "هزینه پرداختی")]
        public string Payment { get; set; }

        [Display(Name = "نوع پرداخت")]
        public byte? PaymentType { get; set; }

        [Display(Name = "ویزیت")]
        public bool Visited { get; set; }

        [Display(Name = "ویزیت پزشک")]
        public bool DoctorVisited { get; set; }

        [Display(Name="پذیرش")]
        public bool Acceptance {get;set;}  

         [Display(Name="زمان پذیرش")]
        public TimeSpan AcceptanceTime {get;set;}        

        [Display(Name = "تاریخ انتخابی")]
        public DateTime Date { get; set; }

        [Display(Name = "زمان انتخابی")]
        public TimeSpan Time { get; set; }

        [Display(Name = "کلینیک")]
        public Clinic Clinic { get; set; }

        [Display(Name = "بیمار")]
        public Patient Patient { get; set; }
        [Display(Name = "پرونده بیمار")]
        public ICollection<PatientRecord> PatientRecord { get; set; }
        public ICollection<ExtraPayment> ExtraPayment { get; set; }

    }
}
