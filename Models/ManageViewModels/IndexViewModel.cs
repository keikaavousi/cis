﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ClinicWebApp.Models.ManageViewModels
{
    public class IndexViewModel
    {
        [Display(Name ="نام کاربری")]
        public string Username { get; set; }

        [Display(Name = "تائید ایمیل")]
        public bool IsEmailConfirmed { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "ایمیل")]
        public string Email { get; set; }

        [Phone]
        [Display(Name = "شماره تماس")]
        public string PhoneNumber { get; set; }

        [Display(Name = "وضعیت")]
        public string StatusMessage { get; set; }
    }
}
