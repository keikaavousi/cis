﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClinicWebApp.Models
{
    public partial class Speciality
    {
        public Speciality()
        {
            Clinic = new HashSet<Clinic>();
        }


        public int Id { get; set; }

        [Display(Name ="عنوان")]
        [Required(ErrorMessage = "این فیلد ضروری است")]
        public string Title { get; set; }

        [Display(Name = "تصویر")]
        public string ImageFile { get; set; }

        public ICollection<Clinic> Clinic { get; set; }
    }
}
