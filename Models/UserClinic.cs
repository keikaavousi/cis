﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace ClinicWebApp.Models
{
    public partial class UserClinic
    {
        public long Id { get; set; }
        public string UserId { get; set; }
        public int ClinicId { get; set; }

        [Display(Name = "کلینیک")]
        public Clinic Clinic { get; set; }

        [Display(Name = "کاربر")]
        public AspNetUsers User { get; set; }
    }
}
