﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClinicWebApp.Models
{
    public partial class OnlineReserve
    {
        public long Id { get; set; }

        [Display(Name ="نام")]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }

        [Display(Name ="نام و نام خانوادگی")]
        public string FullName
        {
            get
            {
               return FirstName + " " + LastName;
            }
        }
        [Display(Name = "موبایل")]
        [DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }


        [Display(Name = "ایمیل")]
        [EmailAddress(ErrorMessage ="فرمت ایمیل صحیح نیست")]
        public string Email { get; set; }

        [Display(Name = "تاریخ انتخابی")]
        public DateTime Date { get; set; }

        [Display(Name = "زمان انتخابی")]
        public TimeSpan Time { get; set; }

        [Display(Name = "زمان ثبت")]
        public DateTime NowDate { get; set; }


        [Display(Name = "وضعیت پرداخت")]
        public bool PaymentStatus { get; set; }


        [Display(Name = "مبلغ پرداختی")]
        public string Payment { get; set; }

         [Display(Name = "شناسه ارجاع")]
        public string RefID { get; set; }


        [Display(Name = "تائید شده")]
        public bool verify { get; set; }

        [Display(Name = "کلینیک")]
        public int ClinicId { get; set; }

        [Display(Name = "کلینیک")]
        public Clinic Clinic { get; set; }
    }
}
