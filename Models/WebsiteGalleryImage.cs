﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClinicWebApp.Models
{
    public partial class WebsiteGalleryImage
    {
        public long Id { get; set; }

        [Display(Name ="فایل")]
        public string FileUrl { get; set; }

        [Display(Name ="گالری")]
        public long GalleryId { get; set; }

        [Display(Name = "عنوان تصویر")]
        public string Title { get; set; }

        [Display(Name ="گالری")]
        public WebsiteGallery Gallery { get; set; }
    }
}
