﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace ClinicWebApp.Models
{
    public partial class Category
    {
        public Category()
        {
            WebsitePost = new HashSet<WebsitePost>();
        }

        public int Id { get; set; }

        [Display(Name ="عنوان دسته")]
        public string Title { get; set; }

        [Remote(action: "CheckExistingSlug", controller: "Categories", AdditionalFields = "Id,WebsiteId")]
        [Display(Name ="نامک دسته")]
        public string Slug { get; set; }

        public int WebsiteId { get; set; }

        [Display(Name ="تصویر دسته")]
        public string ImageUrl { get; set; }

        [Display(Name ="نمایش در صفحه اصلی")]
        public bool ShowInFrontPage { get; set; }

        public Website Website { get; set; }
        public ICollection<WebsitePost> WebsitePost { get; set; }
    }
}
