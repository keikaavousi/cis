﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using ClinicWebApp.Models.AdminPanelViewModel;
using ClinicWebApp.Models;

namespace ClinicWebApp.Models
{
    public partial class ClinicDBContext : DbContext
    {
        //public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        //public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        //public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        //public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        //public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        //public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        //public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<Clinic> Clinic { get; set; }
        public virtual DbSet<Comment> Comment { get; set; }
        public virtual DbSet<Message> Message { get; set; }
        public virtual DbSet<Notification> Notification { get; set; }
        public virtual DbSet<OnlineReserve> OnlineReserve { get; set; }
        public virtual DbSet<Patient> Patient { get; set; }
        public virtual DbSet<PatientRecord> PatientRecord { get; set; }
        public virtual DbSet<Payment> Payment { get; set; }
        public virtual DbSet<RecordAttachment> RecordAttachment { get; set; }
        public virtual DbSet<Reservation> Reservation { get; set; }
        public virtual DbSet<ReservationFreeDate> ReservationFreeDate { get; set; }
        public virtual DbSet<Speciality> Speciality { get; set; }
        public virtual DbSet<UserClinic> UserClinic { get; set; }
        public virtual DbSet<Website> Website { get; set; }
        public virtual DbSet<WebsiteContactForm> WebsiteContactForm { get; set; }
        public virtual DbSet<WebsiteDiscussion> WebsiteDiscussion { get; set; }
        public virtual DbSet<WebsiteGallery> WebsiteGallery { get; set; }
        public virtual DbSet<WebsiteGalleryImage> WebsiteGalleryImage { get; set; }
        public virtual DbSet<WebsitePage> WebsitePage { get; set; }
        public virtual DbSet<WebsitePost> WebsitePost { get; set; }
        public virtual DbSet<WebsiteSlider> WebsiteSlider { get; set; }
        public virtual DbSet<Extra> Extra { get; set; }
        public virtual DbSet<ExtraPayment> ExtraPayment { get; set; }
        public virtual DbSet<Phonebook> Phonebook { get; set; }
        public virtual DbSet<EntranceLog> EntranceLog { get; set; }
         public virtual DbSet<Note> Note { get; set; }

        public ClinicDBContext(DbContextOptions options) : base(options)
        {
        }

        public ClinicDBContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Server=185.4.30.126,2016;Database=medicat1_clinicdb;User Id=medicato_clinicwebapp;Password=3P^46kyt-347cbwq%*#2i;MultipleActiveResultSets=true");
            }
            optionsBuilder.EnableSensitiveDataLogging();
        }
       
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRoleClaims>(entity =>
            {
                entity.HasIndex(e => e.RoleId);

                entity.Property(e => e.RoleId).IsRequired();

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetRoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetRoles>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName)
                    .HasName("RoleNameIndex");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserClaims>(entity =>
            {
                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserClaims)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.HasIndex(e => e.RoleId);

                entity.HasIndex(e => e.UserId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.RoleId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.HasIndex(e => e.NormalizedEmail)
                    .HasName("EmailIndex");

                entity.HasIndex(e => e.NormalizedUserName)
                    .HasName("UserNameIndex")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserTokens>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.Slug)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Website)
                    .WithMany(p => p.Category)
                    .HasForeignKey(d => d.WebsiteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Category_Websites");
            });

            modelBuilder.Entity<Clinic>(entity =>
            {
                entity.Property(e => e.Address).HasMaxLength(200);

                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Mobile).HasMaxLength(20);

                entity.Property(e => e.Price).HasMaxLength(50);

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.Tel).HasMaxLength(20);

                entity.Property(e => e.Url).HasMaxLength(50);
                entity.Property(e => e.LogoFile);

                entity.HasOne(d => d.Speciality)
                    .WithMany(p => p.Clinic)
                    .HasForeignKey(d => d.SpecialityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Clinic_Speciality");
            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.Property(e => e.CommentText).IsRequired();

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Post)
                    .WithMany(p => p.CommentNavigation)
                    .HasForeignKey(d => d.PostId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Comment_WebsitePost");
            });

            modelBuilder.Entity<Message>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.HasOne(d => d.Sender)
                    .WithMany(p => p.Message)
                    .HasForeignKey(d => d.SenderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Message_Message");
            });

             modelBuilder.Entity<Phonebook>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedOnAdd();
                 entity.Property(e => e.Description).HasMaxLength(200);
                entity.HasOne(d => d.Clinic)
                    .WithMany(p => p.Phonebook)
                    .HasForeignKey(d => d.ClinicId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Phonebook_Phonebook");
            });

            modelBuilder.Entity<Note>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedOnAdd();
                 entity.Property(e => e.NoteText).HasMaxLength(200);
                entity.HasOne(d => d.Clinic)
                    .WithMany(p => p.Note)
                    .HasForeignKey(d => d.ClinicId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Note_Note");
            });

             modelBuilder.Entity<EntranceLog>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.HasOne(d => d.AspNetUsers)
                    .WithMany(p => p.EntranceLog)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EntranceLog_EntranceLog");
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Notification1)
                    .IsRequired()
                    .HasColumnName("Notification")
                    .HasMaxLength(200);

                entity.HasOne(d => d.Clinic)
                    .WithMany(p => p.Notification)
                    .HasForeignKey(d => d.ClinicId)
                    .HasConstraintName("FK_Notification_Clinics");
            });

            modelBuilder.Entity<OnlineReserve>(entity =>
            {
                entity.HasOne(d => d.Clinic)
                  .WithMany(p => p.OnlineReserve)
                  .HasForeignKey(d => d.ClinicId)
                  .OnDelete(DeleteBehavior.ClientSetNull)
                  .HasConstraintName("FK_OnlineReserve_Clinic");

                //entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Mobile).HasMaxLength(20);
            });

            modelBuilder.Entity<Patient>(entity =>
            {
            //    entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Address).HasMaxLength(200);

                entity.Property(e => e.DocNumber).HasMaxLength(50);

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.FatherName).HasMaxLength(50);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnName("firstName")
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(50);

                entity.Property(e => e.MainDisease).HasMaxLength(50);

                entity.Property(e => e.Mobile).HasMaxLength(20);

                entity.Property(e => e.Tel).HasMaxLength(20);

                entity.HasOne(d => d.Clinic)
                    .WithMany(p => p.Patient)
                    .HasForeignKey(d => d.ClinicId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Patient_Clinic");
            });

            modelBuilder.Entity<PatientRecord>(entity =>
            {
                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.PatientRecord)
                    .HasForeignKey(d => d.ReservationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PatientDocument_Reservation");
            });


            modelBuilder.Entity<Extra>(entity =>
            {
                entity.HasOne(d => d.Clinic)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.ClinicId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Extra_Clinic");
            });


             modelBuilder.Entity<ExtraPayment>(entity =>
            {
                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.ExtraPayment)
                    .HasForeignKey(d => d.ReservationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ExtraPayment_Reservation");


                     entity.HasOne(d => d.Extra)
                    .WithMany(p => p.ExtraPayment)
                    .HasForeignKey(d => d.ExtraId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Extra_Reservation");
            });

            modelBuilder.Entity<Payment>(entity =>
            {
                entity.Property(e => e.Amount).HasMaxLength(50);

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.UserId).HasMaxLength(450);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Payment)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_Payment_AspNetUsers");
            });

            modelBuilder.Entity<RecordAttachment>(entity =>
            {
                entity.Property(e => e.FileUrl).HasColumnType("nvarchar(max)");

                entity.HasOne(d => d.Patient)
                    .WithMany(p => p.RecordAttachment)
                    .HasForeignKey(d => d.PatientId)
                    .HasConstraintName("FK_RecordAttachment_PatientRecord");
            });

            modelBuilder.Entity<Reservation>(entity =>
            {
                entity.Property(e => e.Payment).HasMaxLength(50);

                entity.HasOne(d => d.Clinic)
                    .WithMany(p => p.Reservation)
                    .HasForeignKey(d => d.ClinicId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Reservation_Reservation");

                entity.HasOne(d => d.Patient)
                    .WithMany(p => p.Reservation)
                    .HasForeignKey(d => d.PatientId)
                    .HasConstraintName("FK_Reservation_Patient");
            });

            modelBuilder.Entity<ReservationFreeDate>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("date");

                entity.HasOne(d => d.Clinic)
                    .WithMany(p => p.ReservationFreeDate)
                    .HasForeignKey(d => d.ClinicId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ReservationFreeDate_Clinic");
            });

            modelBuilder.Entity<Speciality>(entity =>
            {
                entity.Property(e => e.ImageFile)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<UserClinic>(entity =>
            {
                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(450);

                entity.HasOne(d => d.Clinic)
                    .WithMany(p => p.UserClinic)
                    .HasForeignKey(d => d.ClinicId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserClinic_Clinic");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserClinic)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserClinic_AspNetUsers");
            });

            modelBuilder.Entity<Website>(entity =>
            {
                entity.Property(e => e.AboutUsPicture).HasMaxLength(200);

                entity.Property(e => e.Address).HasMaxLength(450);

                entity.Property(e => e.ColorTheme).HasMaxLength(50);

                //entity.Property(e => e.Lat).HasColumnType("decimal(9, 6)");

                //entity.Property(e => e.Long)
                //    .HasColumnName("long")
                //    .HasColumnType("decimal(9, 6)");

                entity.Property(e => e.SubTitle).HasMaxLength(200);


                entity.Property(e => e.Title).HasMaxLength(100);

                entity.HasOne(d => d.Clinic)
                    .WithMany(p => p.WebsiteNavigation)
                    .HasForeignKey(d => d.ClinicId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WebsitePrimaryContent_Clinic");
            });

            modelBuilder.Entity<WebsiteContactForm>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Message).IsRequired();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Tel).HasMaxLength(20);

                entity.HasOne(d => d.Website)
                    .WithMany(p => p.WebsiteContactForm)
                    .HasForeignKey(d => d.WebsiteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WebsiteContactForm_Websites");
            });

            modelBuilder.Entity<WebsiteDiscussion>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Message).IsRequired();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Website)
                    .WithMany(p => p.WebsiteDiscussion)
                    .HasForeignKey(d => d.WebsiteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WebsiteDiscussion_Websites");
            });

            modelBuilder.Entity<WebsiteGallery>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Website)
                    .WithMany(p => p.WebsiteGallery)
                    .HasForeignKey(d => d.WebsiteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WebsiteGallery_Websites");
            });

            modelBuilder.Entity<WebsiteGalleryImage>(entity =>
            {
                //entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.FileUrl).IsRequired();

                entity.Property(e => e.Title).HasMaxLength(200);

                entity.HasOne(d => d.Gallery)
                    .WithMany(p => p.WebsiteGalleryImage)
                    .HasForeignKey(d => d.GalleryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WebsiteGalleryImage_WebsiteGallery");
            });

            modelBuilder.Entity<WebsitePage>(entity =>
            {
                entity.Property(e => e.Slug)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Website)
                    .WithMany(p => p.WebsitePage)
                    .HasForeignKey(d => d.WebsiteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WebsitePage_Websites");
            });

            modelBuilder.Entity<WebsitePost>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.Slug)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Visible).HasColumnName("visible");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.WebsitePost)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_WebsitePost_Websites");

                entity.HasOne(d => d.Website)
                    .WithMany(p => p.WebsitePost)
                    .HasForeignKey(d => d.WebsiteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WebsitePost_Websites1");
            });

            modelBuilder.Entity<WebsiteSlider>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.FileUrl).IsRequired();

                entity.HasOne(d => d.Website)
                    .WithMany(p => p.WebsiteSlider)
                    .HasForeignKey(d => d.WebsiteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WebsiteSlider_Websites");
            });
        }
       
        public DbSet<ClinicWebApp.Models.AdminPanelViewModel.SpecialityViewModel> SpecialityViewModel { get; set; }
       
        public DbSet<ClinicWebApp.Models.AspNetRoles> AspNetRoles { get; set; }
       
        public DbSet<ClinicWebApp.Models.AspNetUsers> AspNetUsers { get; set; }
       
        public DbSet<ClinicWebApp.Models.AspNetUserRoles> AspNetUserRoles { get; set; }
    }
}
