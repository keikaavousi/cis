﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace ClinicWebApp.Models
{
    public partial class Notification
    {
        public int Id { get; set; }

        [Display(Name = "پیام")]
        public string Notification1 { get; set; }

        [Display(Name = "نام کلینیک")]
        public int? ClinicId { get; set; }

        [Display(Name = "تاریخ ارسال")]
        public DateTime Date { get; set; }

        [Display(Name = "نمایش به کاربر")]
        public bool Enabled { get; set; }

        [Display(Name = "کلینیک")]
        public Clinic Clinic { get; set; }
    }
}
