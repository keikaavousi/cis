﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
namespace ClinicWebApp.Models {
    public partial class Patient {
        public Patient () {
            Reservation = new HashSet<Reservation> ();
        }

        [Display (Name = "شناسه")]
        public long Id { get; set; }

        /*[Remote (action: "CheckExistingDocNumber", controller: "Patients", AdditionalFields = "Id,ClinicId")]*/
        [Display (Name = "شماره پرونده")]
        public string DocNumber { get; set; }

        [Required (ErrorMessage = "این فیلد ضروری است")]
        [Display (Name = "نام")]
        public string FirstName { get; set; }

        [Required (ErrorMessage = "این فیلد ضروری است")]
        [Display (Name = "نام خانوادگی")]
        public string LastName { get; set; }

        [Display (Name = "بیمار")]
        public string FullName {
            get {
                return FirstName + " " + LastName;
            }
        }

        [DataType (DataType.PhoneNumber)]
        [Display (Name = "تلفن")]
        public string Tel { get; set; }

        [DataType (DataType.PhoneNumber)]
        [Display (Name = "موبایل")]
        public string Mobile { get; set; }

        [DataType (DataType.EmailAddress, ErrorMessage = "فرمت ایمیل صحیح نیست")]
        [EmailAddress (ErrorMessage = "آدرس ایمیل معتبر نیست")]
        [Display (Name = "ایمیل")]
        public string Email { get; set; }

        [Display (Name = "آدرس")]
        public string Address { get; set; }

        [Display (Name = "بیماری اصلی")]
        public string MainDisease { get; set; }

        [Display (Name = "نام پدر")]
        public string FatherName { get; set; }

        [Display (Name = "شناسه مطب")]
        public int ClinicId { get; set; }

        [Display (Name = "داروهای مصرفی")]
        public string UsedMedication { get; set; }

        [Display (Name = "پزشک ارجاع دهنده")]
        public string Reagent { get; set; }

        [Display (Name = "کدملی")]
        public string NationalId { get; set; }

        [Display (Name = "شغل")]
        public string Job { get; set; }

        [Display (Name = "ویزیت رایگان")]
        public bool NoCharge { get; set; }

        [Display(Name="بیمه پایه")]
        public string Insurance{get;set;} 

        [Display(Name="بیمه تکمیلی")]
        public string Supplementary{get;set;} 

        [Display (Name = "کلینیک")]
        public Clinic Clinic { get; set; }

        public ICollection<Reservation> Reservation { get; set; }

        public ICollection<RecordAttachment> RecordAttachment { get; set; }
    }
}