﻿using System;
using System.Collections.Generic;

namespace ClinicWebApp.Models
{
    public partial class Extra
    {

        public Extra(){
            ExtraPayment=new HashSet<ExtraPayment>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Amount { get; set; }
        public int ClinicId { get; set; }

        public Clinic Clinic { get; set; }
        public ICollection<ExtraPayment> ExtraPayment { get; set; }
    }
}
