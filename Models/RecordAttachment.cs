﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClinicWebApp.Models
{
    public partial class RecordAttachment
    {
        public long Id { get; set; }

        [Display(Name ="بیمار")]
        public long PatientId { get; set; }

        [Display(Name = "فایل")]
        public string FileUrl { get; set; }

        [Display(Name = "تاریخ بارگزاری")]
        public DateTime UploadDateTime { get; set; }

         [Display(Name = "عنوان")]
        public string Title { get; set; }

        [Display(Name = "بیمار")]
        public Patient Patient { get; set; }
    }
}
