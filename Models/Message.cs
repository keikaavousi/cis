﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClinicWebApp.Models
{
    public partial class Message
    {
        public int Id { get; set; }

        [Display(Name ="فرستنده")]
        public int SenderId { get; set; }

        [Display(Name = "پیام")]
        public string MessageText { get; set; }

        [Display(Name = "تاریخ")]
        public DateTime Date { get; set; }

        [Display(Name = "پاسخ")]
        public string Reply { get; set; }

        [Display(Name = "فرستنده")]
        public Clinic Sender { get; set; }
    }
}
