﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace ClinicWebApp.Models
{
    public partial class AspNetUserRoles
    {
        public string UserId { get; set; }
        public string RoleId { get; set; }

        [Display(Name ="نقش")]
        public AspNetRoles Role { get; set; }
        [Display(Name = "کاربر")]
        public AspNetUsers User { get; set; }
    }
}
