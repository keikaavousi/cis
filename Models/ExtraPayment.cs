﻿using System;
using System.Collections.Generic;

namespace ClinicWebApp.Models
{
    public partial class ExtraPayment
    {
        public long Id { get; set; }
        public int ExtraId{get;set;}
        public long ReservationId { get; set; }
        public string Amount { get; set; }

        public Extra Extra { get; set; }
        public Reservation Reservation { get; set; }
    }
}
