﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClinicWebApp.Models
{
    public partial class PatientRecord
    {
        //public PatientRecord()
        //{
        //    RecordAttachment = new HashSet<RecordAttachment>();
        //}

        public long Id { get; set; }
        public long ReservationId { get; set; }

        [Display(Name ="شرح حال")]
        public string Description { get; set; }

        [Display(Name = "شرح حال")]
        public string DescriptionText { get; set; }

        [Display(Name = "داروهای تجویزی")]
        public string Medication { get; set; }

        [Display(Name = "کد رزرو")]
        public Reservation Reservation { get; set; }
        //public ICollection<RecordAttachment> RecordAttachment { get; set; }
    }
}
