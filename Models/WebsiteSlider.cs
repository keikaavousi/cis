﻿using System;
using System.Collections.Generic;

namespace ClinicWebApp.Models
{
    public partial class WebsiteSlider
    {
        public long Id { get; set; }
        public int WebsiteId { get; set; }
        public string FileUrl { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }

        public Website Website { get; set; }
    }
}
