﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace ClinicWebApp.Models
{
    public partial class EntranceLog
    {

        public int Id { get; set; }
        
        [Display(Name="ورود")]
        public DateTime Checkin { get; set; }

        [Display(Name="خروج")]
        public DateTime Checkout { get; set; }

        public string UserId{get;set;}

        [Display(Name="کاربر")]
        public AspNetUsers AspNetUsers { get; set; }

    }
}
