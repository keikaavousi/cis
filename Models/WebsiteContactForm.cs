﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace ClinicWebApp.Models
{
    public partial class WebsiteContactForm
    {
        public long Id { get; set; }
        public int WebsiteId { get; set; }

        [Display(Name="نام")]
        public string Name { get; set; }

        [Display(Name = "ایمیل")]
        public string Email { get; set; }

        [Display(Name = "تلفن")]
        public string Tel { get; set; }

        [Display(Name = "تاریخ")]
        public DateTime Date { get; set; }

        [Display(Name = "پیام")]
        public string Message { get; set; }

        [Display(Name = "فایل")]
        public string FileUrl { get; set; }

        [Display(Name = "تائید")]
        public bool Verify { get; set; }

        public Website Website { get; set; }
    }
}
