﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClinicWebApp.Models
{
    public partial class AspNetUsers
    {
        public AspNetUsers()
        {
            AspNetUserClaims = new HashSet<AspNetUserClaims>();
            AspNetUserLogins = new HashSet<AspNetUserLogins>();
            AspNetUserRoles = new HashSet<AspNetUserRoles>();
            Payment = new HashSet<Payment>();
            UserClinic = new HashSet<UserClinic>();
            EntranceLog = new HashSet<EntranceLog>();
        }
        [Display(Name = "شناسه کاربر")]
        public string Id { get; set; }

        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }

        [Display(Name = "تعداد دفعات ورود ناموفق")]
        public int AccessFailedCount { get; set; }

        [Display(Name = "ایمیل")]
        public string Email { get; set; }

        [Display(Name = "تائید ایمیل")]
        public bool EmailConfirmed { get; set; }

        [Display(Name = "اکانت آزاد")]
        public bool LockoutEnabled { get; set; }

        [Display(Name = "تاریخ قفل شدن اکانت")]
        public DateTimeOffset? LockoutEnd { get; set; }

        [Display(Name = "رمز عبور کدگذاری شده")]
        public string PasswordHash { get; set; }

        [Display(Name = "کد هم زمانی")]
        public string ConcurrencyStamp { get; set; }

        [Display(Name = "ایمیل نرمالیزه شده")]
        public string NormalizedEmail { get; set; }

        [Display(Name = "نام کاربری نرمالیزه شده")]
        public string NormalizedUserName { get; set; }

        [Display(Name = "شماره تماس")]
        public string PhoneNumber { get; set; }

        [Display(Name = "تائید شماره تماس")]
        public bool PhoneNumberConfirmed { get; set; }

        [Display(Name = "کد امنیتی")]
        public string SecurityStamp { get; set; }

        [Display(Name = "ورود دو مرحله ای")]
        public bool TwoFactorEnabled { get; set; }

        public ICollection<AspNetUserClaims> AspNetUserClaims { get; set; }
        public ICollection<AspNetUserLogins> AspNetUserLogins { get; set; }
        public ICollection<AspNetUserRoles> AspNetUserRoles { get; set; }
        public ICollection<Payment> Payment { get; set; }
        public ICollection<UserClinic> UserClinic { get; set; }
        public ICollection<EntranceLog> EntranceLog{get;set;}
    }
}
