﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace ClinicWebApp.Models
{
    public partial class WebsitePost
    {
        public WebsitePost()
        {
            CommentNavigation = new HashSet<Comment>();
        }

        public long Id { get; set; }
        public int WebsiteId { get; set; }

        [Display(Name ="عنوان")]
        [Required(ErrorMessage="این فیلد ضروری است")]
        public string Title { get; set; }

        [Remote(action: "CheckExistingSlug", controller: "WebsitePosts", AdditionalFields = "Id,WebsiteId")]
        [Display(Name = "لینک پست")]
        [Required(ErrorMessage="این فیلد ضروری است")]
        public string Slug { get; set; }

        [Display(Name = "تاریخ")]
        public DateTime? Date { get; set; }

        [Display(Name = "متن")]
        public string PostContent { get; set; }

        [Display(Name = "تصویر پست")]
        public string MainPicture { get; set; }

        [Display(Name = "دسته بندی")]
        public int? CategoryId { get; set; }

        [Display(Name = "برچسب ها")]
        public string Tag { get; set; }

        // [Display(Name = "این پست محتوا محور است؟")]
        // public bool TextBased { get; set; }

         [Display(Name = "کلمات کلیدی")]
        public string Keywords { get; set; }


         [Display(Name = "توضیحات مختصر")]
        public string Description { get; set; }


        [Display(Name = "فعال کردن نظرات برای این پست")]
        public string Comment { get; set; }

        [Display(Name = "قابلیت رویت در سایت")]
        public bool Visible { get; set; }
        
        [Display(Name = "دسته بندی")]
        public Category Category { get; set; }
        public Website Website { get; set; }
        public ICollection<Comment> CommentNavigation { get; set; }
    }
}
