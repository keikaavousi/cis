﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace ClinicWebApp.Models
{
    public partial class WebsiteGallery
    {
        public WebsiteGallery()
        {
            WebsiteGalleryImage = new HashSet<WebsiteGalleryImage>();
        }

        public long Id { get; set; }
        public int WebsiteId { get; set; }

        [Display(Name ="عنوان")]
        public string Title { get; set; }

        [Display(Name ="تاریخ ایجاد")]
        public DateTime Date { get; set; }

        [Display(Name = "نمایش در صفحه گالری")]
        public bool Show { get; set; }

        public Website Website { get; set; }
        public ICollection<WebsiteGalleryImage> WebsiteGalleryImage { get; set; }
    }
}
