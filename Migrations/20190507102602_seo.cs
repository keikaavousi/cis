﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ClinicWebApp.Migrations
{
    public partial class seo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TextBased",
                table: "WebsitePost");

            migrationBuilder.AlterColumn<string>(
                name: "Comment",
                table: "WebsitePost",
                nullable: true,
                oldClrType: typeof(bool));

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "WebsitePost",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Keywords",
                table: "WebsitePost",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "WebsitePost");

            migrationBuilder.DropColumn(
                name: "Keywords",
                table: "WebsitePost");

            migrationBuilder.AlterColumn<bool>(
                name: "Comment",
                table: "WebsitePost",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "TextBased",
                table: "WebsitePost",
                nullable: false,
                defaultValue: false);
        }
    }
}
