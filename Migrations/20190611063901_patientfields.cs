﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ClinicWebApp.Migrations
{
    public partial class patientfields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Job",
                table: "Patient",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "NoCharge",
                table: "Patient",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Job",
                table: "Patient");

            migrationBuilder.DropColumn(
                name: "NoCharge",
                table: "Patient");
        }
    }
}
