﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ClinicWebApp.Migrations
{
    public partial class OnlineReserveMig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ClinicId",
                table: "OnlineReserve",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_OnlineReserve_ClinicId",
                table: "OnlineReserve",
                column: "ClinicId");

            migrationBuilder.AddForeignKey(
                name: "FK_OnlineReserve_Clinic",
                table: "OnlineReserve",
                column: "ClinicId",
                principalTable: "Clinic",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OnlineReserve_Clinic",
                table: "OnlineReserve");

            migrationBuilder.DropIndex(
                name: "IX_OnlineReserve_ClinicId",
                table: "OnlineReserve");

            migrationBuilder.DropColumn(
                name: "ClinicId",
                table: "OnlineReserve");
        }
    }
}
