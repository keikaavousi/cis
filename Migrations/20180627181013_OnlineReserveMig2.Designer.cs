﻿// <auto-generated />
using ClinicWebApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;

namespace ClinicWebApp.Migrations
{
    [DbContext(typeof(ClinicDBContext))]
    [Migration("20180627181013_OnlineReserveMig2")]
    partial class OnlineReserveMig2
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.3-rtm-10026")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ClinicWebApp.Models.AdminPanelViewModel.SpecialityViewModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ImageFile");

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.ToTable("SpecialityViewModel");
                });

            modelBuilder.Entity("ClinicWebApp.Models.AspNetRoleClaims", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("ClinicWebApp.Models.AspNetRoles", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp");

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("ClinicWebApp.Models.AspNetUserClaims", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("ClinicWebApp.Models.AspNetUserLogins", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("ClinicWebApp.Models.AspNetUserRoles", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("ClinicWebApp.Models.AspNetUsers", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp");

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("ClinicWebApp.Models.AspNetUserTokens", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Category", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ImageUrl");

                    b.Property<bool?>("ShowInFrontPage");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("WebsiteId");

                    b.ToTable("Category");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Clinic", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<bool>("ClinicPanel");

                    b.Property<string>("Description")
                        .HasMaxLength(200);

                    b.Property<string>("Email")
                        .HasMaxLength(50);

                    b.Property<bool>("Enabled");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("LogoFile");

                    b.Property<string>("Mobile")
                        .IsRequired()
                        .HasMaxLength(20);

                    b.Property<string>("Price")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<bool>("ReservationPanel");

                    b.Property<int>("SpecialityId");

                    b.Property<DateTime>("StartDate")
                        .HasColumnType("date");

                    b.Property<byte?>("SupportPercent")
                        .IsRequired();

                    b.Property<string>("Tel")
                        .HasMaxLength(20);

                    b.Property<string>("Url")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<bool>("Website");

                    b.HasKey("Id");

                    b.HasIndex("SpecialityId");

                    b.ToTable("Clinic");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Comment", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CommentText")
                        .IsRequired();

                    b.Property<string>("Email")
                        .HasMaxLength(50);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<long>("PostId");

                    b.Property<string>("Reply");

                    b.Property<bool>("Verified");

                    b.HasKey("Id");

                    b.HasIndex("PostId");

                    b.ToTable("Comment");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Message", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date")
                        .HasColumnType("datetime");

                    b.Property<string>("MessageText");

                    b.Property<string>("Reply");

                    b.Property<int>("SenderId");

                    b.HasKey("Id");

                    b.HasIndex("SenderId");

                    b.ToTable("Message");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Notification", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date")
                        .HasColumnType("date");

                    b.Property<bool>("Enabled");

                    b.Property<string>("Notification1")
                        .IsRequired()
                        .HasColumnName("Notification")
                        .HasMaxLength(200);

                    b.Property<int?>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("WebsiteId");

                    b.ToTable("Notification");
                });

            modelBuilder.Entity("ClinicWebApp.Models.OnlineReserve", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ClinicId");

                    b.Property<DateTime>("Date")
                        .HasColumnType("date");

                    b.Property<string>("Email")
                        .HasMaxLength(50);

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Mobile")
                        .HasMaxLength(20);

                    b.Property<TimeSpan>("Time");

                    b.HasKey("Id");

                    b.HasIndex("ClinicId");

                    b.ToTable("OnlineReserve");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Patient", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .HasMaxLength(200);

                    b.Property<int>("ClinicId");

                    b.Property<string>("DocNumber")
                        .HasMaxLength(50);

                    b.Property<string>("Email")
                        .HasMaxLength(50);

                    b.Property<string>("FatherName")
                        .HasMaxLength(50);

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnName("firstName")
                        .HasMaxLength(50);

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasColumnName("lastName")
                        .HasMaxLength(50);

                    b.Property<string>("MainDisease")
                        .HasMaxLength(50);

                    b.Property<string>("Mobile")
                        .HasMaxLength(20);

                    b.Property<string>("Reagent");

                    b.Property<string>("Tel")
                        .HasMaxLength(20);

                    b.Property<string>("UsedMedication");

                    b.HasKey("Id");

                    b.HasIndex("ClinicId");

                    b.ToTable("Patient");
                });

            modelBuilder.Entity("ClinicWebApp.Models.PatientRecord", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<string>("Medication");

                    b.Property<long>("ReservationId");

                    b.HasKey("Id");

                    b.HasIndex("ReservationId");

                    b.ToTable("PatientRecord");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Payment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Amount")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("Date")
                        .HasColumnType("date");

                    b.Property<byte?>("PaymentType");

                    b.Property<string>("UserId")
                        .HasMaxLength(450);

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Payment");
                });

            modelBuilder.Entity("ClinicWebApp.Models.RecordAttachment", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FileUrl")
                        .HasMaxLength(200);

                    b.Property<long?>("PatientRecordId");

                    b.HasKey("Id");

                    b.HasIndex("PatientRecordId");

                    b.ToTable("RecordAttachment");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Reservation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ClinicId");

                    b.Property<long?>("PatientId");

                    b.Property<string>("Payment")
                        .HasMaxLength(50);

                    b.Property<byte?>("PaymentType");

                    b.Property<bool>("Visited");

                    b.HasKey("Id");

                    b.HasIndex("ClinicId");

                    b.HasIndex("PatientId");

                    b.ToTable("Reservation");
                });

            modelBuilder.Entity("ClinicWebApp.Models.ReservationFreeDate", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ClinicId");

                    b.Property<DateTime>("Date")
                        .HasColumnType("date");

                    b.Property<TimeSpan>("EndTime");

                    b.Property<int>("PatientPerDay");

                    b.Property<TimeSpan>("StartTime");

                    b.HasKey("Id");

                    b.HasIndex("ClinicId");

                    b.ToTable("ReservationFreeDate");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Speciality", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ImageFile")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Speciality");
                });

            modelBuilder.Entity("ClinicWebApp.Models.UserClinic", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ClinicId");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasMaxLength(450);

                    b.HasKey("Id");

                    b.HasIndex("ClinicId");

                    b.HasIndex("UserId");

                    b.ToTable("UserClinic");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Website", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AboutUs");

                    b.Property<string>("AboutUsPicture")
                        .HasMaxLength(200);

                    b.Property<string>("Address")
                        .HasMaxLength(450);

                    b.Property<int>("ClinicId");

                    b.Property<string>("ColorTheme")
                        .HasMaxLength(50);

                    b.Property<decimal?>("Lat")
                        .HasColumnType("decimal(9, 6)");

                    b.Property<string>("LogoFileUrl");

                    b.Property<decimal?>("Long")
                        .HasColumnName("long")
                        .HasColumnType("decimal(9, 6)");

                    b.Property<string>("SubTitle")
                        .HasMaxLength(100);

                    b.Property<string>("Tel")
                        .HasMaxLength(450);

                    b.Property<int?>("Theme");

                    b.Property<string>("Title")
                        .HasMaxLength(100);

                    b.HasKey("Id");

                    b.HasIndex("ClinicId");

                    b.ToTable("Website");
                });

            modelBuilder.Entity("ClinicWebApp.Models.WebsiteContactForm", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date")
                        .HasColumnType("date");

                    b.Property<string>("Email")
                        .HasMaxLength(50);

                    b.Property<string>("FileUrl");

                    b.Property<string>("Message")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Tel")
                        .HasMaxLength(20);

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("WebsiteId");

                    b.ToTable("WebsiteContactForm");
                });

            modelBuilder.Entity("ClinicWebApp.Models.WebsiteDiscussion", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date")
                        .HasColumnType("date");

                    b.Property<string>("Email")
                        .HasMaxLength(50);

                    b.Property<string>("Message")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Reply");

                    b.Property<bool>("Verified");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("WebsiteId");

                    b.ToTable("WebsiteDiscussion");
                });

            modelBuilder.Entity("ClinicWebApp.Models.WebsiteGallery", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date")
                        .HasColumnType("date");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("WebsiteId");

                    b.ToTable("WebsiteGallery");
                });

            modelBuilder.Entity("ClinicWebApp.Models.WebsiteGalleryImage", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FileUrl")
                        .IsRequired();

                    b.Property<long>("GalleryId");

                    b.Property<string>("Title")
                        .HasMaxLength(200);

                    b.HasKey("Id");

                    b.HasIndex("GalleryId");

                    b.ToTable("WebsiteGalleryImage");
                });

            modelBuilder.Entity("ClinicWebApp.Models.WebsitePage", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("PageContent");

                    b.Property<string>("PagePicture");

                    b.Property<long?>("ParentPage");

                    b.Property<bool?>("ShowInFrontPage");

                    b.Property<bool>("ShowInMenu");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("WebsiteId");

                    b.ToTable("WebsitePage");
                });

            modelBuilder.Entity("ClinicWebApp.Models.WebsitePost", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CategoryId");

                    b.Property<bool>("Comment");

                    b.Property<DateTime?>("Date")
                        .HasColumnType("datetime");

                    b.Property<string>("MainPicture");

                    b.Property<string>("PostContent");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("Tag");

                    b.Property<bool>("TextBased");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<bool>("Visible")
                        .HasColumnName("visible");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("WebsitePost");
                });

            modelBuilder.Entity("ClinicWebApp.Models.WebsiteSlider", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .HasMaxLength(200);

                    b.Property<string>("FileUrl")
                        .IsRequired();

                    b.Property<string>("Link");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("WebsiteId");

                    b.ToTable("WebsiteSlider");
                });

            modelBuilder.Entity("ClinicWebApp.Models.AspNetRoleClaims", b =>
                {
                    b.HasOne("ClinicWebApp.Models.AspNetRoles", "Role")
                        .WithMany("AspNetRoleClaims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ClinicWebApp.Models.AspNetUserClaims", b =>
                {
                    b.HasOne("ClinicWebApp.Models.AspNetUsers", "User")
                        .WithMany("AspNetUserClaims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ClinicWebApp.Models.AspNetUserLogins", b =>
                {
                    b.HasOne("ClinicWebApp.Models.AspNetUsers", "User")
                        .WithMany("AspNetUserLogins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ClinicWebApp.Models.AspNetUserRoles", b =>
                {
                    b.HasOne("ClinicWebApp.Models.AspNetRoles", "Role")
                        .WithMany("AspNetUserRoles")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ClinicWebApp.Models.AspNetUsers", "User")
                        .WithMany("AspNetUserRoles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ClinicWebApp.Models.Category", b =>
                {
                    b.HasOne("ClinicWebApp.Models.Website", "Website")
                        .WithMany("Category")
                        .HasForeignKey("WebsiteId")
                        .HasConstraintName("FK_Category_Websites");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Clinic", b =>
                {
                    b.HasOne("ClinicWebApp.Models.Speciality", "Speciality")
                        .WithMany("Clinic")
                        .HasForeignKey("SpecialityId")
                        .HasConstraintName("FK_Clinic_Speciality");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Comment", b =>
                {
                    b.HasOne("ClinicWebApp.Models.WebsitePost", "Post")
                        .WithMany("CommentNavigation")
                        .HasForeignKey("PostId")
                        .HasConstraintName("FK_Comment_WebsitePost");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Message", b =>
                {
                    b.HasOne("ClinicWebApp.Models.Clinic", "Sender")
                        .WithMany("Message")
                        .HasForeignKey("SenderId")
                        .HasConstraintName("FK_Message_Message");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Notification", b =>
                {
                    b.HasOne("ClinicWebApp.Models.Website", "Website")
                        .WithMany("Notification")
                        .HasForeignKey("WebsiteId")
                        .HasConstraintName("FK_Notification_Websites");
                });

            modelBuilder.Entity("ClinicWebApp.Models.OnlineReserve", b =>
                {
                    b.HasOne("ClinicWebApp.Models.Clinic", "Clinic")
                        .WithMany("OnlineReserve")
                        .HasForeignKey("ClinicId")
                        .HasConstraintName("FK_OnlineReserve_Clinic");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Patient", b =>
                {
                    b.HasOne("ClinicWebApp.Models.Clinic", "Clinic")
                        .WithMany("Patient")
                        .HasForeignKey("ClinicId")
                        .HasConstraintName("FK_Patient_Clinic");
                });

            modelBuilder.Entity("ClinicWebApp.Models.PatientRecord", b =>
                {
                    b.HasOne("ClinicWebApp.Models.Reservation", "Reservation")
                        .WithMany("PatientRecord")
                        .HasForeignKey("ReservationId")
                        .HasConstraintName("FK_PatientDocument_Reservation");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Payment", b =>
                {
                    b.HasOne("ClinicWebApp.Models.AspNetUsers", "User")
                        .WithMany("Payment")
                        .HasForeignKey("UserId")
                        .HasConstraintName("FK_Payment_AspNetUsers");
                });

            modelBuilder.Entity("ClinicWebApp.Models.RecordAttachment", b =>
                {
                    b.HasOne("ClinicWebApp.Models.PatientRecord", "PatientRecord")
                        .WithMany("RecordAttachment")
                        .HasForeignKey("PatientRecordId")
                        .HasConstraintName("FK_RecordAttachment_PatientRecord");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Reservation", b =>
                {
                    b.HasOne("ClinicWebApp.Models.Clinic", "Clinic")
                        .WithMany("Reservation")
                        .HasForeignKey("ClinicId")
                        .HasConstraintName("FK_Reservation_Reservation");

                    b.HasOne("ClinicWebApp.Models.Patient", "Patient")
                        .WithMany("Reservation")
                        .HasForeignKey("PatientId")
                        .HasConstraintName("FK_Reservation_Patient");
                });

            modelBuilder.Entity("ClinicWebApp.Models.ReservationFreeDate", b =>
                {
                    b.HasOne("ClinicWebApp.Models.Clinic", "Clinic")
                        .WithMany("ReservationFreeDate")
                        .HasForeignKey("ClinicId")
                        .HasConstraintName("FK_ReservationFreeDate_Clinic");
                });

            modelBuilder.Entity("ClinicWebApp.Models.UserClinic", b =>
                {
                    b.HasOne("ClinicWebApp.Models.Clinic", "Clinic")
                        .WithMany("UserClinic")
                        .HasForeignKey("ClinicId")
                        .HasConstraintName("FK_UserClinic_Clinic");

                    b.HasOne("ClinicWebApp.Models.AspNetUsers", "User")
                        .WithMany("UserClinic")
                        .HasForeignKey("UserId")
                        .HasConstraintName("FK_UserClinic_AspNetUsers");
                });

            modelBuilder.Entity("ClinicWebApp.Models.Website", b =>
                {
                    b.HasOne("ClinicWebApp.Models.Clinic", "Clinic")
                        .WithMany("WebsiteNavigation")
                        .HasForeignKey("ClinicId")
                        .HasConstraintName("FK_WebsitePrimaryContent_Clinic");
                });

            modelBuilder.Entity("ClinicWebApp.Models.WebsiteContactForm", b =>
                {
                    b.HasOne("ClinicWebApp.Models.Website", "Website")
                        .WithMany("WebsiteContactForm")
                        .HasForeignKey("WebsiteId")
                        .HasConstraintName("FK_WebsiteContactForm_Websites");
                });

            modelBuilder.Entity("ClinicWebApp.Models.WebsiteDiscussion", b =>
                {
                    b.HasOne("ClinicWebApp.Models.Website", "Website")
                        .WithMany("WebsiteDiscussion")
                        .HasForeignKey("WebsiteId")
                        .HasConstraintName("FK_WebsiteDiscussion_Websites");
                });

            modelBuilder.Entity("ClinicWebApp.Models.WebsiteGallery", b =>
                {
                    b.HasOne("ClinicWebApp.Models.Website", "Website")
                        .WithMany("WebsiteGallery")
                        .HasForeignKey("WebsiteId")
                        .HasConstraintName("FK_WebsiteGallery_Websites");
                });

            modelBuilder.Entity("ClinicWebApp.Models.WebsiteGalleryImage", b =>
                {
                    b.HasOne("ClinicWebApp.Models.WebsiteGallery", "Gallery")
                        .WithMany("WebsiteGalleryImage")
                        .HasForeignKey("GalleryId")
                        .HasConstraintName("FK_WebsiteGalleryImage_WebsiteGallery");
                });

            modelBuilder.Entity("ClinicWebApp.Models.WebsitePage", b =>
                {
                    b.HasOne("ClinicWebApp.Models.Website", "Website")
                        .WithMany("WebsitePage")
                        .HasForeignKey("WebsiteId")
                        .HasConstraintName("FK_WebsitePage_Websites");
                });

            modelBuilder.Entity("ClinicWebApp.Models.WebsitePost", b =>
                {
                    b.HasOne("ClinicWebApp.Models.Category", "Category")
                        .WithMany("WebsitePost")
                        .HasForeignKey("CategoryId")
                        .HasConstraintName("FK_WebsitePost_Websites");

                    b.HasOne("ClinicWebApp.Models.Website", "Website")
                        .WithMany("WebsitePost")
                        .HasForeignKey("WebsiteId")
                        .HasConstraintName("FK_WebsitePost_Websites1");
                });

            modelBuilder.Entity("ClinicWebApp.Models.WebsiteSlider", b =>
                {
                    b.HasOne("ClinicWebApp.Models.Website", "Website")
                        .WithMany("WebsiteSlider")
                        .HasForeignKey("WebsiteId")
                        .HasConstraintName("FK_WebsiteSlider_Websites");
                });
#pragma warning restore 612, 618
        }
    }
}
