﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ClinicWebApp.Migrations
{
    public partial class OnlineReserveMig5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RecordAttachment_PatientRecord",
                table: "RecordAttachment");

            migrationBuilder.AddColumn<long>(
                name: "PatientId",
                table: "RecordAttachment",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_RecordAttachment_PatientId",
                table: "RecordAttachment",
                column: "PatientId");

            migrationBuilder.AddForeignKey(
                name: "FK_RecordAttachment_PatientRecord",
                table: "RecordAttachment",
                column: "PatientId",
                principalTable: "Patient",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RecordAttachment_PatientRecord_PatientRecordId",
                table: "RecordAttachment",
                column: "PatientRecordId",
                principalTable: "PatientRecord",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RecordAttachment_PatientRecord",
                table: "RecordAttachment");

            migrationBuilder.DropForeignKey(
                name: "FK_RecordAttachment_PatientRecord_PatientRecordId",
                table: "RecordAttachment");

            migrationBuilder.DropIndex(
                name: "IX_RecordAttachment_PatientId",
                table: "RecordAttachment");

            migrationBuilder.DropColumn(
                name: "PatientId",
                table: "RecordAttachment");

            migrationBuilder.AddForeignKey(
                name: "FK_RecordAttachment_PatientRecord",
                table: "RecordAttachment",
                column: "PatientRecordId",
                principalTable: "PatientRecord",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
