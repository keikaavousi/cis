﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ClinicWebApp.Migrations
{
    public partial class OnlineReserveMig7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RecordAttachment_PatientRecord_PatientRecordId",
                table: "RecordAttachment");

            migrationBuilder.DropIndex(
                name: "IX_RecordAttachment_PatientRecordId",
                table: "RecordAttachment");

            migrationBuilder.DropColumn(
                name: "PatientRecordId",
                table: "RecordAttachment");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "PatientRecordId",
                table: "RecordAttachment",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RecordAttachment_PatientRecordId",
                table: "RecordAttachment",
                column: "PatientRecordId");

            migrationBuilder.AddForeignKey(
                name: "FK_RecordAttachment_PatientRecord_PatientRecordId",
                table: "RecordAttachment",
                column: "PatientRecordId",
                principalTable: "PatientRecord",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
