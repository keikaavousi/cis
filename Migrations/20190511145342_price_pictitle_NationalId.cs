﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ClinicWebApp.Migrations
{
    public partial class price_pictitle_NationalId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Title",
                table: "RecordAttachment",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "NationalId",
                table: "Patient",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "VisitPrice",
                table: "Clinic",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Title",
                table: "RecordAttachment");

            migrationBuilder.DropColumn(
                name: "NationalId",
                table: "Patient");

            migrationBuilder.DropColumn(
                name: "VisitPrice",
                table: "Clinic");
        }
    }
}
