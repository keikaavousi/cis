﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ClinicWebApp.Migrations
{
    public partial class patientfield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Insurance",
                table: "Patient",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Supplementary",
                table: "Patient",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Insurance",
                table: "Patient");

            migrationBuilder.DropColumn(
                name: "Supplementary",
                table: "Patient");
        }
    }
}
