﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ClinicWebApp.Migrations
{
    public partial class addmap : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Lat",
                table: "Website");

            migrationBuilder.DropColumn(
                name: "long",
                table: "Website");

            migrationBuilder.AddColumn<string>(
                name: "Map",
                table: "Website",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Map",
                table: "Website");

            migrationBuilder.AddColumn<decimal>(
                name: "Lat",
                table: "Website",
                type: "decimal(9, 6)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "long",
                table: "Website",
                type: "decimal(9, 6)",
                nullable: true);
        }
    }
}
