﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ClinicWebApp.Migrations
{
    public partial class tels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Tel",
                table: "Website");

            migrationBuilder.AddColumn<string>(
                name: "Tel1",
                table: "Website",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tel2",
                table: "Website",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tel3",
                table: "Website",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tel4",
                table: "Website",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tel5",
                table: "Website",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Tel1",
                table: "Website");

            migrationBuilder.DropColumn(
                name: "Tel2",
                table: "Website");

            migrationBuilder.DropColumn(
                name: "Tel3",
                table: "Website");

            migrationBuilder.DropColumn(
                name: "Tel4",
                table: "Website");

            migrationBuilder.DropColumn(
                name: "Tel5",
                table: "Website");

            migrationBuilder.AddColumn<string>(
                name: "Tel",
                table: "Website",
                maxLength: 450,
                nullable: true);
        }
    }
}
