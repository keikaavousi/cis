﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ClinicWebApp.Migrations
{
    public partial class OnlineReserveMig10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Notification_Websites",
                table: "Notification");

            migrationBuilder.AddColumn<int>(
                name: "ClinicId",
                table: "Notification",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Notification_ClinicId",
                table: "Notification",
                column: "ClinicId");

            migrationBuilder.AddForeignKey(
                name: "FK_Notification_Clinics",
                table: "Notification",
                column: "ClinicId",
                principalTable: "Clinic",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Notification_Website_WebsiteId",
                table: "Notification",
                column: "WebsiteId",
                principalTable: "Website",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Notification_Clinics",
                table: "Notification");

            migrationBuilder.DropForeignKey(
                name: "FK_Notification_Website_WebsiteId",
                table: "Notification");

            migrationBuilder.DropIndex(
                name: "IX_Notification_ClinicId",
                table: "Notification");

            migrationBuilder.DropColumn(
                name: "ClinicId",
                table: "Notification");

            migrationBuilder.AddForeignKey(
                name: "FK_Notification_Websites",
                table: "Notification",
                column: "WebsiteId",
                principalTable: "Website",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
