﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ClinicWebApp.Data;
using ClinicWebApp.Models;
using ClinicWebApp.Services;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.Cookies;
namespace ClinicWebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }





        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddDbContext<ClinicDBContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));


            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();


           /*  services.AddSession(options => { 
                options.CookieName = ".MyApplication";
            });*/



            services.AddSingleton<IFileProvider>(
               new PhysicalFileProvider(
                   Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")));


            services.Configure<IdentityOptions>(options =>
                {
                    // Password settings
                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 4;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireLowercase = false;
                });



            

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll", p =>
                {
                    p.AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod();
                });
            });

            services.AddMvc()
            .AddJsonOptions(
                options => options.SerializerSettings.ReferenceLoopHandling=Newtonsoft.Json.ReferenceLoopHandling.Ignore);
   
             services.AddScoped<GetUserInfoService>();

            services.AddDistributedMemoryCache(); // Adds a default in-memory implementation of IDistributedCache
            //services.AddSession();
            
            
             services.AddSession(options => {
                    options.IdleTimeout = TimeSpan.FromMinutes(86400);
            });

            services.AddAuthentication();
            services.AddProgressiveWebApp();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }



            //app.UseStaticFiles(new StaticFileOptions()
            //{

            //    FileProvider = new PhysicalFileProvider(
            //                Path.Combine(Directory.GetCurrentDirectory(), @"Records")),
            //    RequestPath = new PathString("/app-records")
            //});


            app.UseStaticFiles();


            app.Use(async (context, next) =>
               {
                   if (!context.User.Identity.IsAuthenticated
                       && context.Request.Path.StartsWithSegments("/images"))
                   {
                      context.Response.Redirect("/account/Login");
                        return;
                   }
                   await next.Invoke();
               });


            app.UseAuthentication();

            app.UseCors("AllowAll");
            app.UseSession();
            app.UseMvc(routes =>
            {

                routes.MapRoute(
               name: "default",
               template: "{controller=Home}/{action=Index}/{id?}");

        
               
                routes.MapRoute(name: "api", template: "api/{controller=Websites}");

             
                routes.MapRoute(name: "postapi", template: "api/{controller=WebsitePostInfo}/{action=GetWebsitePostDetails}/{pid:long}");

                routes.MapRoute(
           name: "SeoPost",
               template: "{controller=WebsitePosts}/{action=Details}/{id:long}/{*slug}"
           );

            });


        }
    }
}
