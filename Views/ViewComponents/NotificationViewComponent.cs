﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ClinicWebApp.Views.ViewComponents
{
    [ViewComponent(Name = "Notification")]
    public class NotificationViewComponent : ViewComponent
    {
        private readonly ClinicDBContext _context;

        public NotificationViewComponent(ClinicDBContext context)
        {
            _context = context;
        }
        private Task<List<Notification>> GetItemsAsync()
        {
            var clinicDBContext = _context.Notification.Include(n => n.Clinic)
            .Where(n=>n.ClinicId== UserInfo.ClinicId)
            .Where(n=>n.Enabled==true).ToListAsync();
            return clinicDBContext;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            string MyView = "Default";
            //// If asking for all completed tasks, render with the "PVC" view.
            //if (maxPriority > 3 && isDone == true)
            //{
            //    MyView = "PVC";
            //}
            var items = await GetItemsAsync();
            return View(MyView, items);
        }

               //public IActionResult QueueListViewComponent()
        //{
        //    return ViewComponent("QueueList");
        //}

    }
}

