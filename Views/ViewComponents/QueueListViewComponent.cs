﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ClinicWebApp.Views.ViewComponents
{
    
    [ViewComponent(Name = "QueueList")]
    public class QueueListViewComponent : ViewComponent
    {
        private readonly ClinicDBContext _context;

        public QueueListViewComponent(ClinicDBContext context)
        {
            _context = context;
        }

        private Task<List<Reservation>> GetItemsAsync(DateTime dt)
        {
            var clinicDBContext = _context.Reservation.Where(r => r.Date.ToShortDateString() == dt.ToShortDateString())
            .Include(r => r.Clinic).Include(r => r.Patient)
            .Where(r=>r.ClinicId== UserInfo.ClinicId)
            .OrderByDescending(r => r.Date).ThenBy(r => r.Time);

            @ViewData["visited"] = clinicDBContext.Where(r => r.Visited == true).Count();

            @ViewData["extra"]=_context.Extra.Where(x=>x.ClinicId == UserInfo.ClinicId);
           // @ViewData["NoCharge"] = clinicDBContext.Include(r=>r.Patient).Select(r=>r.Patient.NoCharge);

            @ViewData["accepted"] = clinicDBContext.Where(r => r.Acceptance == true).Count();


            @ViewData["unverify_online_reserved"]=  _context.OnlineReserve
                .Where(r => r.Date.ToShortDateString() == dt.ToShortDateString())
                .Where(r=>r.ClinicId== UserInfo.ClinicId)
                .Where(a=>a.PaymentStatus==true)
                .Where(a=>a.verify==false)
                .Count();
               





            var clinicList= clinicDBContext.ToListAsync();

            return clinicList;
        }
        public async Task<IViewComponentResult> InvokeAsync(DateTime dt)
        {
           
            string MyView = "Default";
            //// If asking for all completed tasks, render with the "PVC" view.
            //if (maxPriority > 3 && isDone == true)
            //{
            //    MyView = "PVC";
            //}
            var items = await GetItemsAsync(dt);
            return View(MyView, items);
        }
        

    }
    
}

