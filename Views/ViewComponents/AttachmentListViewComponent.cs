﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ClinicWebApp.Views.ViewComponents
{
    [ViewComponent(Name = "AttachmentList")]
    public class AttachmentListViewComponent : ViewComponent
    {
        private readonly ClinicDBContext _context;

        public AttachmentListViewComponent(ClinicDBContext context)
        {
            _context = context;
        }
        private Task<List<RecordAttachment>> GetItemsAsync()
        {

            var clinicDBContext = _context.RecordAttachment
            .Include(r => r.Patient)
            .Where(r => r.Patient.ClinicId == UserInfo.ClinicId)
            .OrderByDescending(r => r.UploadDateTime).ToListAsync();

            return clinicDBContext;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            string MyView = "Default";
            //// If asking for all completed tasks, render with the "PVC" view.
            //if (maxPriority > 3 && isDone == true)
            //{
            //    MyView = "PVC";
            //}
            var items = await GetItemsAsync();
            return View(MyView, items);
        }

               //public IActionResult QueueListViewComponent()
        //{
        //    return ViewComponent("QueueList");
        //}

    }
}

