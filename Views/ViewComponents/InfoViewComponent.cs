﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ClinicWebApp.Views.ViewComponents
{
    [ViewComponent(Name = "Info")]
    public class InfoViewComponent : ViewComponent
    {
        private readonly ClinicDBContext _context;
      
        public InfoViewComponent(ClinicDBContext context)
        {
            _context = context;
        }
        private Task<List<UserClinic>> GetItemsAsync()
        {
             var userId = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var uid = _context.UserClinic.Include(a => a.Clinic).Include(a => a.Clinic.Speciality).Where(a=>a.UserId== userId).ToListAsync();
            
            return uid;

        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            string MyView = "Default";
            //// If asking for all completed tasks, render with the "PVC" view.
            //if (maxPriority > 3 && isDone == true)
            //{
            //    MyView = "PVC";
            //}
            var items = await GetItemsAsync();
            return View(MyView, items);
        }

        //public IActionResult InfoViewComponent()
        //{
            

        //    return ViewComponent("Info");
        //}

    }
}

