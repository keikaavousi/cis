﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClinicWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ClinicWebApp.Views.ViewComponents
{
    [ViewComponent(Name = "RecordList")]
    public class RecordListViewComponent : ViewComponent
    {
        private readonly ClinicDBContext _context;

        public RecordListViewComponent(ClinicDBContext context)
        {
            _context = context;
        }
        private Task<List<PatientRecord>> GetItemsAsync()
        {
            var clinicDBContext = _context.PatientRecord
            .Include(r => r.Reservation)
            .Include(r => r.Reservation.Patient)
            .Where(r=>r.Reservation.ClinicId== UserInfo.ClinicId)
            .OrderByDescending(r => r.Reservation.Date).ToListAsync();

            return clinicDBContext;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            string MyView = "Default";
            //// If asking for all completed tasks, render with the "PVC" view.
            //if (maxPriority > 3 && isDone == true)
            //{
            //    MyView = "PVC";
            //}
            var items = await GetItemsAsync();
            return View(MyView, items);
        }

               //public IActionResult QueueListViewComponent()
        //{
        //    return ViewComponent("QueueList");
        //}

    }
}

